@include('Admin.Frame.head')
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <h4 class="mb-0">Tambah Siswa</h4>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
                <form id="create-siswa">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group row">
                                <label for="example-text-input" class="col-md-2 col-form-label">NIS</label>
                                <div class="col-md-10">
                                    <input class="form-control required" type="number" name="nis" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Nama Lengkap</label>
                                <div class="col-md-10">
                                    <input class="form-control required" type="text" name="nama" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Jenis Kelamin</label>
                                <div class="col-md-10">
                                    <select class="form-control select2 required" name="jenis_kelamin" id="jenis-kelamin" required>
                                        <option selected disabled value="default">Pilih jenis kelamin ...</option>
                                        <option value="Laki - Laki">Laki - Laki</option>
                                        <option value="Perempuan">Perempuan</option>
                                    </select>
                                    <p class="text-error pb-0 mb-0" id="jenis-kelamin-error"></p>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Email</label>
                                <div class="col-md-10">
                                    <input class="form-control required" type="email" name="email" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Password</label>
                                <div class="col-md-10">
                                    <input class="form-control required" type="text" name="password" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Rombel Saat Ini</label>
                                <div class="col-md-10">
                                    <select class="form-control select2 required" name="rombel" id="rombel" required>
                                        
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Tempat Lahir</label>
                                <div class="col-md-10">
                                    <input class="form-control required" type="text" name="tempat_lahir" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Tanggal Lahir</label>
                                <div class="col-md-10">
                                    <input class="form-control required" type="text" name="tanggal_lahir" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Nama Wali</label>
                                <div class="col-md-10">
                                    <input class="form-control required" type="text" name="nama_wali" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-md-2 col-form-label">NIK Wali</label>
                                <div class="col-md-10">
                                    <input class="form-control required" type="text" name="nik_wali" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Asal SD</label>
                                <div class="col-md-10">
                                    <input class="form-control required" type="text" name="asal_sd" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Asal SMP</label>
                                <div class="col-md-10">
                                    <input class="form-control required" type="text" name="asal_smp" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <button type="button" class="btn btn-success waves-effect waves-light btn-sm mr-2" onclick="generateKegiatan()"><i class="fas fa-plus mr-2"></i>Tambah Kegiatan</button>
                        </div>
                    </div>
                    <div class="row mt-3" id="generateKegiatan">
                        
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Foto Profile</label>
                                <div class="col-md-10">
                                    <div class="custom-file">
                                        <input type="file" required onchange="validateFileType(this)" accept="image/*" class="custom-file-input required" id="customFile" name="foto_profile" required>
                                        <label class="custom-file-label" for="customFile" id="file-name">Upload Foto Profile</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 text-center">
                            <img src="" alt="" id="imageFile"/>
                        </div>
                    </div>
                </form>
                <button type="button" class="btn btn-info waves-effect waves-light mr-2" onclick="tambahSiswa()"><i class="fas fa-plus mr-2"></i>Tambah Siswa</button>
            </div>
        </div>
    </div>
</div>
@include('Admin.Frame.footer')
<script src="{{url('assets/costum/js')}}/Admin/DataSiswa/tambah_siswa.js"></script>