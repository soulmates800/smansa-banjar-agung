@include('Admin.Frame.head')
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <h4 class="mb-0">Edit Siswa</h4>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
                <form id="create-siswa">
                    <input id="ed-id" type="hidden" name="id">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group row">
                                <label for="example-text-input" class="col-md-2 col-form-label">NIS</label>
                                <div class="col-md-10">
                                    <input class="form-control" id="ed-nis" type="text" name="nis">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Nama Lengkap</label>
                                <div class="col-md-10">
                                    <input class="form-control" id="ed-nama" type="text" name="nama">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Jenis Kelamin</label>
                                <div class="col-md-10">
                                    <select class="form-control select2" id="ed-jeniskelamin" name="jenis_kelamin">
                                        <option>Pilih jenis kelamin ...</option>
                                        <option value="Laki - Laki">Laki - Laki</option>
                                        <option value="Perempuan">Perempuan</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Email</label>
                                <div class="col-md-10">
                                    <input class="form-control" id="ed-email" type="text" name="email">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="no_ref" class="col-md-2 col-form-label">Password</label>
                                <div class="col-md-10">
                                    <div class="input-group">
                                        <input name="password" type="text" class="form-control" placeholder="Tuliskan Password Disini" id="ed-password" required>
                                        <div class="input-group-append ml-3">
                                            <div onclick="showPass()" id="panel" class="costum-center">
                                                <i class="ri-eye-line fa-lg"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Rombel Saat Ini</label>
                                <div class="col-md-10">
                                    <select class="form-control select2 required" name="rombel" id="ed-rombel" required>
                                        
                                    </select>
                                    {{-- <input class="form-control" id="ed-rombel" type="text" name="rombel"> --}}
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Tempat Lahir</label>
                                <div class="col-md-10">
                                    <input class="form-control" id="ed-tempatlahir" type="text" name="tempat_lahir">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Tanggal Lahir</label>
                                <div class="col-md-10">
                                    <input class="form-control" id="ed-tanggallahir" type="text" name="tanggal_lahir">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Nama Wali</label>
                                <div class="col-md-10">
                                    <input class="form-control" id="ed-namawali" type="text" name="nama_wali">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-md-2 col-form-label">NIK Wali</label>
                                <div class="col-md-10">
                                    <input class="form-control" id="ed-nikwali" type="text" name="nik_wali">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Asal SD</label>
                                <div class="col-md-10">
                                    <input class="form-control" id="ed-asalsd" type="text" name="asal_sd">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Asal SMP</label>
                                <div class="col-md-10">
                                    <input class="form-control" id="ed-asalsmp" type="text" name="asal_smp">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <button type="button" class="btn btn-success waves-effect waves-light btn-sm mr-2" onclick="generateKegiatan()"><i class="fas fa-plus mr-2"></i>Tambah Kegiatan</button>
                        </div>
                    </div>
                    <div class="row mt-3" id="generateKegiatan">
                        
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Foto Profile</label>
                                <div class="col-md-10">
                                    <div class="custom-file">
                                        <input type="file" required onchange="validateFileType(this)" accept="image/*" class="custom-file-input" id="customFile" name="foto_profile">
                                        <label class="custom-file-label" for="customFile" id="file-name">Upload Foto Profile</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 text-center" id="imageFile">
                            <img src="" alt="" width="150" height="150"/>
                        </div>
                    </div>
                </form>
                <button type="button" class="btn btn-info waves-effect waves-light mr-2" onclick="updateSiswa()"><i class="fas fa-edit mr-2"></i>Perbarui Data Siswa</button>
            </div>
        </div>
    </div>
</div>
@include('Admin.Frame.footer')
<script src="{{url('assets/costum/js')}}/Admin/DataSiswa/edit_siswa.js"></script>