@include('Admin.Frame.head')
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <h4 class="mb-0">Daftar Siswa</h4>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
                <div class="flex-daftarsiswa">
                    <a href="{{ url('admin/data-siswa/tambah_siswa') }}" class="btn btn-success waves-effect waves-light mr-2 mb-3"><i class="fas fa-plus mr-2"></i>Tambah Siswa</a>
                        <button onclick="cetakSemuaPerKelas()" class="btn btn-info waves-effect waves-light mr-2 mb-3"><i class="fas fa-print mr-2"></i>Cetak Semua</button>
                        <select id="selectFilter" class="pb-2 form-control select2 mb-3" style="width: 300px">
                            
                        </select>
                        <select onchange="handleKelasFilter(this)" id="kelasFilter" class="pb-2 form-control select2  mb-3" style="width: 300px">
                        
                        </select>
                </div>
                
                
                <div class="col-md-12 mt-3">
                    <div class="table-responsive">
                        <table id="datatable-buttons" class="table table-striped table-bordered nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead>
                                <tr>
                                    <th class="text-center" style="width: 5px;">No.</th>
                                    <th class="text-center">NIS</th>
                                    <th>Nama Siswa</th>
                                    <th class="text-center">Rombel</th>
                                    <th class="text-center">Jenis Kelamin</th>
                                    <th>Nama Wali</th>
                                    <th class="text-center">Tanggal Lahir</th>
                                    <th class="text-center">Tempat Lahir</th>
                                    <th class="text-center">Aksi</th>
                                </tr>
                            </thead>
                            <tbody id="generateData">
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('Admin.Frame.footer')
<script src="{{url('assets/costum/js')}}/Admin/DataSiswa/daftar_siswa.js"></script>