<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>SMANSA - SMA Negeri 1 Banjar Agung</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="SMANSA - SMA Negeri 1 Banjar Agung" name="Reky" />
    <meta content="smansa" name="Reky" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" href="{{url('assets/costum/images')}}/logo.png">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Mulish:wght@700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Poppins&display=swap" rel="stylesheet">
    <link href="{{url('assets/vendors/libs')}}/admin-resources/jquery.vectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />
    <link href="{{url('assets/vendors/libs')}}/datatables.net-bs4/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <link href="{{url('assets/vendors/libs')}}/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />  
    <link href="{{url('assets/vendors/css')}}/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="{{url('assets/vendors/css')}}/icons.min.css" rel="stylesheet" type="text/css" />
    <link href="{{url('assets/vendors/css')}}/app.min.css" rel="stylesheet" type="text/css" />
    <link href="{{url('assets/vendors/libs')}}/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="{{url('assets/vendors/libs')}}/toastr/build/toastr.min.css">
    <link href="{{url('assets/costum/css')}}/costum.css" rel="stylesheet" type="text/css" />
</head>
<body data-sidebar="dark">

    <div class="container-fluid mt-5">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <h3>Data Peserta Didik <br>SMA Negeri 1 Banjar Agung</h3>
                        <span>Kecamatan Banjar Agung, Kabupaten Kab. Tulang Bawang, Provinsi Prov. Lampung</span>
                    </div>
                    <div class="col-md-12 mt-3">
                        <table id="datatable-buttons" class="table table-striped table-bordered nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead style="font-size: 10px;">
                                <tr>
                                    <th class="text-center">No.</th>
                                    <th>Tipe</th>
                                    <th class="text-right">Keterangan</th>
                                </tr>
                            </thead>
                            <tbody class="roboto-font" style="font-size: 10px;">
                                <tr>
                                    <td class="text-center">1</td>
                                    <td>NIS</td>
                                    <td class="text-right" id="nis"></td>
                                </tr>
                                <tr>
                                    <td class="text-center">2</td>
                                    <td>Nama Siswa</td>
                                    <td class="text-right" id="nama"></td>
                                </tr>
                                <tr>
                                    <td class="text-center">3</td>
                                    <td>Rombel</td>
                                    <td class="text-right" id="rombel"></td>
                                </tr>
                                <tr>
                                    <td class="text-center">4</td>
                                    <td>Jenis Kelamin</td>
                                    <td class="text-right" id="jenis_kelamin"></td>
                                </tr>
                                <tr>
                                    <td class="text-center">5</td>
                                    <td>Tempat Lahir</td>
                                    <td class="text-right" id="tempat_lahir"></td>
                                </tr>
                                <tr>
                                    <td class="text-center">6</td>
                                    <td>Tanggal Lahir</td>
                                    <td class="text-right" id="tanggal_lahir"></td>
                                </tr>
                                <tr>
                                    <td class="text-center">7</td>
                                    <td>Wali Murid</td>
                                    <td class="text-right" id="nama_wali"></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-xl-12">
                        <div class="card">
                            <div class="card-body">
                                <span id="semester-aktif"></span>
                                <div class="row" id="generateTemplate">
                                    
                                </div>
                                <div class="col-md-12 text-right mt-3">
                                    <span id="totalPembayaran"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        
    </div>

@include('Admin.Frame.footer')
<script>
    var datas = {!! json_encode($data["params"]) !!}
</script>
<script src="{{url('assets/costum/js')}}/Admin/DataSiswa/cetak_per_siswa.js"></script>