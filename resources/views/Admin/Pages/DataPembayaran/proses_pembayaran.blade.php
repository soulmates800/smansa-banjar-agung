@include('Admin.Frame.head')

<style>
.select2-container--default .select2-selection--single .select2-selection__rendered {
    color: white !important;
    font-weight: bolder;
}

.select2-selection__rendered {
    opacity: 1 !important;
}
</style>

<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <h4 class="mb-0">Proses Validasi Pembayaran</h4>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
                <p id="semester"></p>
                <div class="row" id="generateTemplate">
                    
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-header border-bottom bg-main text-white roboto-font">
                Informasi Pembayaran
            </div>
            <div class="card-body">
                <form id="formPembayaran">
                    <div class="form-group row">
                        <label for="nis" class="col-md-2 col-form-label">NIS</label>
                        <div class="col-md-10">
                            <input class="form-control" type="text" id="nis" disabled readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="nama" class="col-md-2 col-form-label">NAMA</label>
                        <div class="col-md-10">
                            <input class="form-control bg-disabled" type="text" id="nama" disabled readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="tanggal_bayar" class="col-md-2 col-form-label">Tanggal Bayar</label>
                        <div class="col-md-10">
                            <input class="form-control bg-disabled" type="text" name="tanggal_bayar" id="tanggal_bayar" disabled readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="no_ref" class="col-md-2 col-form-label">No Ref</label>
                        <div class="col-md-10">
                            <input class="form-control" type="number" name="no_ref" id="no_ref" required disabled readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="nama_bank" class="col-md-2 col-form-label">Bayar Via</label>
                        <div class="col-md-10">
                            <input class="form-control" type="tel" name="nama_bank" id="nama_bank" required disabled readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="jumlah" class="col-md-2 col-form-label">Jumlah Pembayaran</label>
                        <div class="col-md-10">
                            <input class="form-control" oninput="getIdForRupiah(this)" type="text" id="jumlah_pembayaran" required disabled readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="bulan" class="col-md-2 col-form-label">Bulan Pembayaran</label>
                        <div class="col-md-10">
                            <input class="form-control" oninput="getIdForRupiah(this)" type="text" id="bulan_pembayaran" required disabled readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12 text-center">
                            <span>Foto Bukti Pembayaran</span>
                        </div>
                        <div class="col-md-12 text-center" id="bukti_pembayaran">
                            
                        </div>
                    </div>
                    <div class="row">
                        <label for="bulan" class="col-md-2 col-form-label">Status Pembayaran</label>
                        <div class="col-md-10">
                            <select class="form-control select2" style="width: 100%;" onchange="handleSelect()" id="ed-statuspembayaran">
                                
                            </select>
                        </div>
                    </div>
                    <div class="form-group row mt-3">
                        <div class="col-md-12">
                            <button type="button" onclick="updatePembayaran()" class="btn btn-info waves-effect waves-light mr-1">Update Status Pembayaran</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@include('Admin.Frame.footer')
<script src="{{url('assets/costum/js')}}/Admin/Pembayaran/proses_pembayaran.js"></script>