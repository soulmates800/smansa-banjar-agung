@include('Admin.Frame.head')



<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <h4 class="mb-0">Data Semester</h4>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12 mb-4">
                        <button type="button" class="btn btn-primary waves-effect waves-light mr-2" data-toggle="modal" data-target=".tambah-semester"><i class="fas fa-plus fa-sm mr-2"></i>Tambah Semester</button>
                    </div>
                    <div class="col-md-12 mb-4">
                        <span id="aktivated_semester"></span>
                    </div>
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table id="datatable-buttons" class="table table-striped table-bordered nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                <thead>
                                    <tr>
                                        <th class="text-center">No.</th>
                                        <th>Semester</th>
                                        <th>Dibuat Pada</th>
                                        <th class="text-center">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody id="generateData">
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade tambah-semester" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0">Tambah Semester</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="tambah-semester">
                    <div class="form-group">
                        <label>Semester</label>
                        <select class="form-control" type="text" onchange="txt_tipe(this)" id="tipe_ed" placeholder="Cth: Semester Ganjil TA. 2020/2021">
                            <option>Ganjil</option>
                            <option>Genap</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Tahun Ajaran</label>
                        <div class="row">
                            <div class="col-md-5">
                                <input class="form-control" oninput="txt_awal(this)" type="number" id="awal_ed" placeholder="Cth: 2020">
                            </div>
                            <div class="col-md-2">
                                <div class="costum-center" style="height: 100%">
                                    <div>/</div>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <input class="form-control" oninput="txt_akhir(this)" type="number" id="akhir_ed" name="semester" placeholder="Cth: 2021">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <span>Preview: Semester <span id="txt-tipe">Ganjil</span> Tahun Ajaran <span id="txt-awal">....</span>/<span id="txt-akhir">....</span></span>
                    </div>
                    <div class="form-group">
                        <span id="dataAda" style="color: red">Data sudah ada, silahkan masukkan ulang</span>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success waves-effect waves-light mr-2" onclick="checkDataSemester()"><i class="fas fa-plus fa-sm mr-2"></i>Tambah</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade update-semester" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0">Tambah Semester</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="tambah-semester">
                    <div class="form-group">
                        <label>Semester</label>
                        <select class="form-control" type="text" onchange="txt_tipe_updt(this)" id="update_tipe_ed" placeholder="Cth: Semester Ganjil TA. 2020/2021">
                            
                            <option>Genap</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Tahun Ajaran</label>
                        <div class="row">
                            <div class="col-md-5">
                                <input class="form-control" oninput="txt_awal_updt(this)" type="number" id="update_awal_ed" placeholder="Cth: 2020">
                            </div>
                            <div class="col-md-2">
                                <div class="costum-center" style="height: 100%">
                                    <div>/</div>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <input class="form-control" oninput="txt_akhir_updt(this)" type="number" id="update_akhir_ed" name="semester" placeholder="Cth: 2021">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <span>Preview: Semester <span id="txt-tipe_updt">Ganjil</span> Tahun Ajaran <span id="txt-awal_updt">....</span>/<span id="txt-akhir_updt">....</span></span>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success waves-effect waves-light mr-2" onclick="tambahSemester()"><i class="fas fa-plus fa-sm mr-2"></i>Update</button>
            </div>
        </div>
    </div>
</div>

@include('Admin.Frame.footer')
<script src="{{url('assets/costum/js')}}/Admin/MasterData/semester.js"></script>