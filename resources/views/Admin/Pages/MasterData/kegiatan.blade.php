@include('Admin.Frame.head')
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <h4 class="mb-0">Data Kegiatan</h4>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12 mb-5">
                        <button type="button" class="btn btn-primary waves-effect waves-light mr-2" data-toggle="modal" data-target=".tambah-kegiatan"><i class="fas fa-plus fa-sm mr-2"></i>Tambah Kegiatan</button>
                    </div>
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table id="datatable-buttons" class="table table-striped table-bordered nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                <thead>
                                    <tr>
                                        <th class="text-center">No.</th>
                                        <th>Kegiatan</th>
                                        <th>Dibuat Pada</th>
                                        <th class="text-center">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody id="generateData">
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade tambah-kegiatan" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0">Tambah Kegiatan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="tambah-kegiatan">
                    <div class="form-group">
                        <label>Kegiatan</label>
                        <input class="form-control" type="text" name="kegiatan" id="formkegiatan" placeholder="Cth: Paskibra">
                    </div>
                    <div class="form-group">
                        <span id="dataAda" style="color: red">Data sudah ada, silahkan masukkan ulang</span>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success waves-effect waves-light mr-2" onclick="checkDataSemester()"><i class="fas fa-plus fa-sm mr-2"></i>Tambah</button>
            </div>
        </div>
    </div>
</div>

@include('Admin.Frame.footer')
<script src="{{url('assets/costum/js')}}/Admin/MasterData/kegiatan.js"></script>