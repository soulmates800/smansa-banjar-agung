@include('Admin.Frame.head')
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <h4 class="mb-0">Laporan Belajar Siswa</h4>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-2 col-3 roboto-font pb-2">
                        <span>NIS</span>
                    </div>
                    <div class="col-md-10 col-9 pb-2">
                        <span id="dd-nis" class="roboto-font"></span>
                    </div>

                    <div class="col-md-2 col-3 roboto-font pb-2">
                        <span>Nama</span>
                    </div>
                    <div class="col-md-10 col-9 pb-2">
                        <span id="dd-nama" class="roboto-font"></span>
                    </div>

                    <div class="col-md-2 col-3 roboto-font pb-4">
                        <span>Kelas</span>
                    </div>
                    <div class="col-md-10 col-9 pb-4">
                        <span id="dd-kelas" class="roboto-font"></span>
                    </div>

                    <div class="col-md-12 mb-5">
                        <button type="button" class="btn btn-primary waves-effect waves-light mr-2" data-toggle="modal" data-target=".tambah-semester"><i class="fas fa-plus fa-sm mr-2"></i>Tambah Semester</button>
                    </div>
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table id="datatable-buttons" class="table table-striped table-bordered nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                <thead>
                                    <tr>
                                        <th class="text-center">No.</th>
                                        <th>Semester</th>
                                        <th class="text-center">Nama File</th>
                                        <th class="text-center">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody id="generateData">
                                    
                                </tbody>
                            </table>
                        </div>
                        
                    </div>
                </div>
                
                
            </div>
        </div>
    </div>
</div>

<div class="modal fade update-semester" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0">Update Semester</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="update-semester">
                    <input class="form-control" type="hidden" name="id" id="id-update">
                    <div class="form-group">
                        <label>Semester</label>
                        <select class="form-control select2 nama-semester" style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true" name="semester" id="nama-semester">
                                
                        </select>
                    </div>
                    <div class="form-group">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="update-file-laporan" name="file">
                            <label class="custom-file-label" for="update-file-laporan" id="update-label-file-laporan">Upload Laporan</label>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success waves-effect waves-light mr-2" onclick="updateSemester()"><i class="fas fa-plus fa-sm mr-2"></i>Perbarui</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade tambah-semester" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0">Tambah Semester</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="tambah-semester">
                    <div class="form-group">
                        <label>Semester</label>
                        <select class="form-control select2 nama-semester" style="width: 100%;" name="semester" data-select2-id="2" tabindex="-1" aria-hidden="true" id="tambah-semester">
                                
                        </select>
                    </div>
                    <div class="form-group">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="customFile" name="file">
                            <label class="custom-file-label" for="customFile">Upload Laporan</label>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success waves-effect waves-light mr-2" onclick="tambahSemester()"><i class="fas fa-plus fa-sm mr-2"></i>Tambah</button>
            </div>
        </div>
    </div>
</div>

@include('Admin.Frame.footer')
<script src="{{url('assets/costum/js')}}/Admin/BelajarSiswa/belajar_siswa.js"></script>