@include('Admin.Frame.head')
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <h4 class="mb-0">Daftar Siswa</h4>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-3 col-12 pb-2">
                        <select class="form-control select2 required" id="generateKelas" onchange="getSiswa()" required>
                            
                        </select>
                    </div>
                    <div class="col-md-3">
                        <select class="form-control select2 required" id="generateSemester" onchange="getSiswa()" required>

                        </select>
                    </div>
                    <div class="col-md-12 mt-3">
                        <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead>
                                <tr>
                                    <th class="text-center">No.</th>
                                    <th class="text-center">NIS</th>
                                    <th>Nama Siswa</th>
                                    <th class="text-center">Kelas</th>
                                    <th class="text-center">Aksi</th>
                                </tr>
                            </thead>
                            <tbody id="generateData">
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('Admin.Frame.footer')
<script src="{{url('assets/costum/js')}}/Admin/BelajarSiswa/daftar_siswa.js"></script>