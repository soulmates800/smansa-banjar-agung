@include('Admin.Frame.head')
<div class="row">
    <div class="col-6">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <h4 class="mb-0 dashboard-tittle">Dashboard | Selamat Datang Wali Murid</h4>
        </div>
    </div>
    <div class="col-6 text-right">
        <div class="page-title-box text-right">
            <h4 class="mb-0 dashboard-tittle" id="semester-dashboard"></h4>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xl-3">
        <div class="card">
            <div class="row no-gutters align-items-center">
                <div class="col-md-4 col-4">
                    <img class="card-img img-fluid" src="{{ url('assets/data/dash') }}/clock.png" style="height: 100px; width: 100px;" alt="Card image">
                </div>
                <div class="col-md-8 col-8">
                    <div class="card-body">
                        <h5 class="card-title" id="timeLive">12.00 WIB</h5>
                        <p class="card-text" id="currentDate">Senin, 01 Januari 2021</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xl-3">
        <div class="card">
            <div class="row no-gutters align-items-center">
                <div class="col-md-4 col-4">
                    <img class="card-img img-fluid" src="{{ url('assets/data/dash') }}/user.png" style="height: 100px; width: 100px;" alt="Card image">
                </div>
                <div class="col-md-8 col-8">
                    <div class="card-body">
                        <h5 class="card-title">Kepala Sekolah</h5>
                        <p class="card-text" style="font-size: 12px;">Hi. Danial Anwar, S.Pd., MM.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xl-3">
        <div class="card">
            <div class="row no-gutters align-items-center">
                <div class="col-md-4 col-4">
                    <img class="card-img img-fluid" src="{{ url('assets/data/dash') }}/users.png" style="height: 100px; width: 100px;" alt="Card image">
                </div>
                <div class="col-md-8 col-8">
                    <div class="card-body">
                        <h5 class="card-title">Jumlah Peserta Didik</h5>
                        <p class="card-text" id="jumlahPeserta"></p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xl-3">
        <div class="card">
            <div class="row no-gutters align-items-center">
                <div class="col-md-4 col-4">
                    <img class="card-img img-fluid" src="{{ url('assets/data/dash') }}/chart.png" style="height: 100px; width: 100px;" alt="Card image">
                </div>
                <div class="col-md-8 col-8">
                    <div class="card-body">
                        <h5 class="card-title">Total Pendapatan</h5>
                        <p class="card-text" id="totalPengeluaran"></p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xl-6">
        <div class="card">
            <div class="card-body">
                <textarea id="textpengumuman" required="" class="form-control" rows="5"></textarea>
                <button class="btn btn-info mt-2" onclick="kirimPengumuman()">Kirim</button>
            </div>
        </div>
    </div>

    <div class="col-xl-6">
        <div class="card">
            <div class="card-body" id="pengumuman">
                
            </div>
        </div>
    </div>

</div>
@include('Admin.Frame.footer')
<script src="{{url('assets/costum/js')}}/Admin/Home/dashboard.js"></script>