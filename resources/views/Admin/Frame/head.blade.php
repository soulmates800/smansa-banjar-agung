<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>SMANSA - SMA Negeri 1 Banjar Agung</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="SMANSA - SMA Negeri 1 Banjar Agung" name="Reky" />
    <meta content="smansa" name="Reky" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" href="{{url('assets/costum/images')}}/logo.png">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Mulish:wght@700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Poppins&display=swap" rel="stylesheet">
    <link href="{{url('assets/vendors/libs')}}/admin-resources/jquery.vectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />
    <link href="{{url('assets/vendors/libs')}}/datatables.net-bs4/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <link href="{{url('assets/vendors/libs')}}/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />  
    <link href="{{url('assets/vendors/css')}}/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="{{url('assets/vendors/css')}}/icons.min.css" rel="stylesheet" type="text/css" />
    <link href="{{url('assets/vendors/css')}}/app.min.css" rel="stylesheet" type="text/css" />
    <link href="{{url('assets/vendors/libs')}}/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="{{url('assets/vendors/libs')}}/toastr/build/toastr.min.css">
    <link href="{{url('assets/costum/css')}}/costum.css" rel="stylesheet" type="text/css" />
</head>

<body data-sidebar="dark">
    <div id="layout-wrapper">
        @include('Admin.Frame.header')