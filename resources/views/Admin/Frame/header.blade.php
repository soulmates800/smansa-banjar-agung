<header id="page-topbar">
    <div class="navbar-header">
        <div class="d-flex">
            <div class="navbar-brand-box">
                <div class="costum-center">
                    <h3 class="pb-0 mb-0 text-white roboto-font bold navbars-text">Admin</h3>
                </div>
            </div>
            <button type="button" class="btn btn-sm px-3 font-size-24 header-item waves-effect" id="vertical-menu-btn">
                <i class="ri-menu-2-line align-middle text-white"></i>
            </button>
        </div>
        <div class="d-flex">
            <div class="dropdown d-inline-block user-dropdown">
                <button type="button" class="btn header-item" id="page-header-user-dropdown"data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <img class="rounded-circle header-profile-user" src="{{ url('assets/data/images') .'/'. $data['user']->foto_profile }}" width="50" height="50">
                    <span class="d-none d-xl-inline-block ml-3 text-white roboto-font">{{$data['user']->nama}}</span>
                </button>
                <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" href="#"><i class="ri-user-line align-middle mr-1"></i> Profile</a>
                    <div class="dropdown-divider"></div>
                    <form id="logoutUser" method="POST" action="{{ route('logout') }}" >
                        @csrf
                        <a style="cursor: pointer;" onclick="event.preventDefault(); this.closest('form').submit();" class="dropdown-item text-danger"><i class="ri-shut-down-line align-middle mr-1 text-danger"></i> Logout</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</header>
@include('Admin.Frame.sidebar')