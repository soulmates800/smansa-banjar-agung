{{-- @include('Main.Layout.head') --}}

<!DOCTYPE html>
<html lang="en" style="height: 100% !important;">

    <head>
        <meta charset="utf-8" />
        <title>SMANSA - SMA Negeri 1 Banjar Agung</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="SMANSA - SMA Negeri 1 Banjar Agung" name="Reky" />
        <meta content="smansa" name="Reky" />
        <link href="{{ url('assets/costum') }}/css/login.css" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css" integrity="sha512-HK5fgLBL+xu6dm/Ii3z4xhlSUyZgTT9tuc/hSrtw6uzJOvgRr2a9jyxxT1ely+B+xFAmJKVSTbpM/CuL7qxO8w==" crossorigin="anonymous" />
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@600&display=swap" rel="stylesheet">
        <meta name="csrf-token" content="{{ csrf_token() }}">
    </head>

<body class="container" style="margin-top: 100px">
    <div class="mb-3">
        <label for="email" class="form-label">Email:</label>
        <input type="email" class="form-control" id="email" placeholder="Tuliskan email anda disini ...">
    </div>
    <div class="mb-3">
        <button type="button" class="btn btn-primary" onclick="redirectEmail()">Reset Password</button>
    </div>
</body>

<script src="{{ url('assets/costum/js') }}/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/gasparesganga-jquery-loading-overlay@2.1.7/dist/loadingoverlay.min.js" type="text/javascript"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script type="text/javascript">
  var BaseUrl = "{{ env('APP_SERVER_BASE') }}";
  var ServeUrl = "{{ env('APP_SERVER_API') }}";

  function redirectEmail() {
      window.location.href = BaseUrl + '/send-email/' + $('#email').val();
  }
</script>