<head>
    <link href="https://fonts.googleapis.com/css?family=PT+Sans:400,700|PT+Sans+Caption:400,700" rel="stylesheet" type="text/css">
    <!--[if (gte mso 9)|(IE)]><html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office" /><![endif]-->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style type="text/css">
      @import url(https://fonts.googleapis.com/css?family=PT+Sans:400,700|PT+Sans+Caption:400,700);
      /* Reset Styles */
      #outlook a {padding:0;}
      body {width: 100% !important;-webkit-text-size-adjust: none; -ms-text-size-adjust: none;
      margin: 0;padding: 0;}
      .ReadMsgBody {width: 100%;}
      .ExternalClass {width: 100%;}
      .backgroundTable {margin: 0 auto;padding: 0;width: 100% !important;}
      table td {border-collapse: collapse;}
      .ExternalClass * {line-height: 115%;}
      .inbox-overlay {background-color:rgba(255,255,255,.8) !important; background-color:#ffffff;}
      /* Link Fixes */
      a[x-apple-data-detectors] {color: inherit !important;
      text-decoration: none !important;font-size: inherit !important;
      font-family: inherit !important;font-weight: inherit !important;
      line-height: inherit !important;}
      
      /* All Your Font Jazz */
      /* Google Fonts */
      @media screen and (-webkit-min-device-pixel-ratio:0) {
      /* Webkit styles */
      .PTSans {font-family: 'PT Sans', sans-serif !important;}
      .PTSans strong {font-weight: 700 !important;}
      .PTSans.regular strong {font-weight: 400 !important;}
      }
      
      @-moz-document url-prefix() {
      /* Firefox styles */
      .PTSans {font-family: 'PT Sans', sans-serif !important;}
      .PTSans strong {font-weight: 700!important;}
      .PTSans.regular strong {font-weight: 400 !important;}
      }
      
      @media screen and (-ms-high-contrast: none), (-ms-high-contrast: active) {
      /* I.E. styles */
      .PTSans {font-family: 'PT Sans Caption', sans-serif !important;}
      .PTSans strong {font-weight: 700!important;}
      .PTSans.regular strong {font-weight: 400 !important;}
      }
      /* Caption */
      @media screen and (-webkit-min-device-pixel-ratio:0) {
      /* Webkit styles */
      .PTSansCaption {font-family: 'PT Sans Caption', sans-serif !important;}
      .PTSansCaption strong {font-weight: 700 !important;}
      .PTSansCaption.regular strong {font-weight: 400 !important;}
      }
      
      @-moz-document url-prefix() {
      /* Firefox styles */
      .PTSansCaption {font-family: 'PT Sans Caption', sans-serif !important;}
      .PTSansCaption strong {font-weight: 700!important;}
      .PTSansCaption.regular strong {font-weight: 400 !important;}
      }
      
      @media screen and (-ms-high-contrast: none), (-ms-high-contrast: active) {
      /* I.E. styles */
      .PTSansCaption {font-family: 'PT Sans Caption', sans-serif !important;}
      .PTSansCaption strong {font-weight: 700!important;}
      .PTSansCaption.regular strong {font-weight: 400 !important;}
      }
      /* Mobile Styles */
      @media only screen and (max-width : 480px) {
      /********** CUSTOM STYLES ***********/
      .body-width {width:250px !important;}
      .triangle-mobile {background-size:cover !important;background-repeat: no-repeat;background-position:50% 0 !important}
          .mobile-bg{height:764px !important;}
          .triangle-height{height:410px !important}
          .hero-bg {background-size:cover !important;background-position:-40px 10px !important}
      /****** Add Custom Styles Here ******/
      
      /* General Styles */
      .container {width: 100% !important;overflow: hidden;}
      .mobileHide {display: none !important;}
      .displayInlineBlock {display: inline-block;}
      .fullWidth {width: 100% !important;height: auto;}
      .halfWidth {width: 50% !important;height: auto;}
      .floatLeft {float: left;}
      .floatRight {float: right;}
      .heightAuto {height: auto;}
      th {width:100% !important;display:block !important;}
      .mobileStack th {width:100% !important; display:block !important;}
      
      /* Images */
      .fullWidthImg img {width: 100% !important;height: auto;}
      .fullImg {width: 100% !important;}
      
      /* Buttons */
      .fullWidthBtn {width: 100% !important;height: auto;border-left: 0 none !important;
          border-right: 0 none !important;padding-left: 0 !important;padding-right: 0 !important;}
      
      /* Text */
      .mobileAlignCenter {text-align: center !important;margin: auto;float: none;}
      .mobileAlignLeft {text-align: left !important;}
      .copy13 {font-size: 13px !important;line-height: 1.5 !important;}
      .copy14 {font-size: 14px !important;line-height: 1.5 !important;}
      .copy16 {font-size: 16px !important;line-height: 1.5 !important;}
      .copy18 {font-size: 18px !important;line-height: 1.5 !important;}
      .copy21 {font-size: 21px !important;line-height: 1.5 !important;}
      .copy24 {font-size: 24px !important;line-height: 1.5 !important;}
      .copy30 {font-size: 30px !important;line-height: 1.5 !important;}
      .copy52{font-size:52px !important;}
          .copy73 {font-size: 73px !important;}
      /* Borders */
      .borderTop {border-top:1px solid #e5e5e5;}
      .borderTopBold {border-top: 2px solid #559EBF;}
      .borderBottom {border-bottom: 1px solid #e5e5e5;}
      .borderBottomBold {border-bottom: 2px solid #559EBF;}
      .borderNone {border: 0 none !important;}
      .borderTopNone {border-top: 0 none !important;}
      .borderRightNone {border-right: 0 none !important;}
      .borderBottomNone {border-bottom: 0 none !important;}
      .borderLeftNone {border-left: 0 none !important;}
      
      /* Padding */
      .contentPadding {padding-left: 10px !important;padding-right: 10px !important;}
      .contentPadding20 {padding-left: 20px !important;padding-right: 20px !important;}
      .paddingNone {padding: 0 !important;}
      .paddingLeft0 {padding-left: 0 !important;}
      .paddingRight0 {padding-right: 0 !important;}
      .paddingTop0 {padding-top: 0 !important;}
      .paddingTop2 {padding-top: 2px !important;}
      .paddingTop5 {padding-top: 5px !important;}
      .paddingTop10 {padding-top: 10px !important;}
      .paddingTop15 {padding-top: 15px !important;}
      .paddingTop20 {padding-top: 20px !important;}
      .paddingTop25 {padding-top: 25px !important;}
      .paddingTop35 {padding-top: 35px !important;}
      .paddingBottom0 {padding-bottom: 0 !important;}
      .paddingBottom2 {padding-bottom: 2px !important;}
      .paddingBottom5 {padding-bottom: 5px !important;}
      .paddingBottom10 {padding-bottom: 10px !important;}
      .paddingBottom15 {padding-bottom: 15px !important;}
      .paddingBottom20 {padding-bottom: 20px !important;}
      .paddingBottom25 {padding-bottom: 25px !important;}
      .paddingBottom30 {padding-bottom: 30px !important;}
      
      /* Footer Nav */
      .footer-nav a {
          display: block !important;
          width: 100%;
          padding: 15px 0 !important;
          border-top: 1px solid #ecece8 !important;
          border-right: 0 !important;
          border-left: 0 !important;
          font-size: 13px !important;
          }
      .footer-nav, .footer-nav tbody, .footer-nav tr, .footer-nav td {width: 100% !important;max-height: inherit !important;
      overflow: visible !important;display: block !important;float: none !important;}
      .footer-nav table {display: table !important;width:100% !important;}
      .footer-nav tbody {display: table !important;}
      .footer-nav tr {display: table-row !important;}
      .footer-nav td {display: table-cell !important;}
      
      /* Mobile Show */
      .mobileShow, .mobileShow tbody, .mobileShow tr, .mobileShow td {width: 100% !important;
      max-height: inherit !important;overflow: visible !important;display: block !important;
      float: none !important;}
      .mobileShow table {display: table !important;width:100% !important;}
      .mobileShow tbody {display: table !important;}
      .mobileShow tr {display: table-row !important;}
      .mobileShow td {display: table-cell !important;}
      
      /* Reset Styles */
      body {margin:0 !important;}
      div[style*="margin: 16px 0"] {margin:0 !important;}
      }
    </style>
    <!--[if gte mso 9]>
  <xml>
      <o:OfficeDocumentSettings>
          <o:AllowPNG/>
          <o:PixelsPerInch>96</o:PixelsPerInch>
      o:OfficeDocumentSettings>
  </xml>
  <![endif]-->
  </head>
  
  <body>
    <table width="100%" cellpadding="0" cellspacing="0" border="0">
        <tbody>
          <tr>
            <td class="" align="center" valign="middle" height="300" style="height:300px">
              <!-- Place Your Nonsense Here -->
              <!-- Containing Table -->

              <!-- Containing Table END-->

              <!-- Headline -->
              <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0">
                <tbody>
                  <tr>
                    <td class="contentPadding20 copy52 PTSans paddingTop35" style="font-family:Arial, Helvetica, sans-serif; font-size:71px; color:#242424; letter-spacing:.1em; text-align:center; font-weight:bold; padding:80px 0 0;text-transform:uppercase;">smansa</td>
                  </tr>
                </tbody>
              </table>
              <!-- Headline END -->
              <!-- Headline -->
              <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0">
                <tbody>
                  <tr>
                    <td class="contentPadding20 copy16 PTSans" style="font-family:Arial, Helvetica, sans-serif; font-size:22px;letter-spacing:.45em; color:#363636; text-align:center; font-weight:normal; padding:0 0 20px;">Banjar Agung</td>
                  </tr>
                </tbody>
              </table>
              <!-- Headline END -->
              <!-- Copy -->
              <table class="body-width" width="316" cellpadding="0" cellspacing="0" border="0" style="width:316px;">
                <tbody>
                  <tr>
                    <td class="PTSans paddingTop10" style="font-family:Tahoma, Arial, Helvetica, sans-serif; font-size:13px; line-height:21px; color:#242424; text-align:center; padding:20px 0 20px;letter-spacing:.2px;">Permintaan reset password anda telah berhasil diproses. Berikut adalah password baru anda</td>
                  </tr>
                  <tr>
                    <td class="PTSans paddingTop10" style="font-family:Tahoma, Arial, Helvetica, sans-serif; font-size:13px; line-height:21px; color:#242424; text-align:center; padding:20px 0 20px;letter-spacing:.2px;"><h3><b>{{ $detail['password'] }}</b></h3></td>
                  </tr>
                  <tr>
                    <td class="PTSans paddingTop10" style="font-family:Tahoma, Arial, Helvetica, sans-serif; font-size:13px; line-height:21px; color:#242424; text-align:center; padding:20px 0 20px;letter-spacing:.2px;">Email ini adalah email otomatis. Diharap untuk tidak membalas pesan ini.</td>
                  </tr>
                </tbody>
              </table>
              <!-- Copy END -->
              <!-- Proceed to End Placing Nonsense -->
            </td>
          </tr>
        </tbody>
      </table>
  
  </body>