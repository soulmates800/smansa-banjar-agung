<div class="vertical-menu">
    <div data-simplebar class="h-100">
        <div class="row">
            <div class="col-md-12 mt-3 text-center">
                <img src="{{ url('assets/data/images') .'/'. $data['user']->foto_profile }}" width="50" height="50" class="sidebar-images">
                <div class="sidebars-text mt-3 mb-2">
                    <span class="roboto-font text-white sidebar-user-name">{{$data['user']->nama}}</span>
                </div>
            </div>
            <div class="col-md-12">
                <div id="sidebar-menu">
                    <ul class="metismenu list-unstyled" id="side-menu">
                        <li>
                            <a href="{{ url('kepala-sekolah/home') }}" class=" waves-effect">
                                <i class="ri-home-2-line"></i>
                                <span>Dashboard</span>
                            </a>
                        </li>
                        <li class="menu-title">Fungsi Utama</li>
                        <li>
                            <a href="javascript: void(0);" class="has-arrow waves-effect">
                                <i class="ri-computer-line"></i>
                                <span>Monitoring</span>
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li><a href="{{ url('kepala-sekolah/monitoring/kegiatan-siswa') }}"><i class="ri-chat-poll-line"></i>Kegiatan Siswa</a></li>
                                <li><a href="{{ url('kepala-sekolah/monitoring/belajar-siswa') }}"><i class="ri-folder-open-line"></i>Laporan Belajar</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="{{ url('kepala-sekolah/pembayaran/status_pembayaran') }}" class=" waves-effect">
                                <i class="ri-money-dollar-circle-line"></i>
                                <span>Status Pembayaran</span>
                            </a>
                        </li>
                        <li class="menu-title">Lainnya</li>
                        <li>
                            <a href="javascript: void(0);" class="has-arrow waves-effect">
                                <i class="ri-user-line"></i>
                                <span>Profile</span>
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li><a href="{{ url('kepala-sekolah/profile/edit_profile') }}" ><i class="ri-user-settings-line"></i>Edit Profile</a></li>
                            </ul>
                        </li>
                        <form id="logoutUser" method="POST" action="{{ route('logout') }}" >
                            @csrf
                        <li onclick="event.preventDefault(); this.closest('form').submit();">
                            <a class=" waves-effect">
                                <i class="ri-logout-circle-r-line"></i>
                                <span>Logout</span>
                            </a>
                        </li>
                    </form>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="main-content">
    <div class="page-content">
        <div class="container-fluid">