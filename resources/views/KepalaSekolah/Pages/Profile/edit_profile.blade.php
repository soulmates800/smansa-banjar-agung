@include('KepalaSekolah.Frame.head')
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <h4 class="mb-0">Edit Profile</h4>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-header border-bottom bg-main text-white roboto-font">
                Edit data diri
            </div>
            <div class="card-body">
                <form id="update-profile">
                    <div class="form-group row">
                        <label for="nis" class="col-md-2 col-form-label">NIP</label>
                        <div class="col-md-10">
                            <input class="form-control" type="text" id="nis" name="nis">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="nama" class="col-md-2 col-form-label">Nama Admin</label>
                        <div class="col-md-10">
                            <input class="form-control" type="text" id="nama" name="nama">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="tanggal_bayar" class="col-md-2 col-form-label">Jenis Kelamin</label>
                        <div class="col-md-10">
                            <input class="form-control" type="text" id="jenis_kelamin" name="jenis_kelamin">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="no_ref" class="col-md-2 col-form-label">Tanggal Lahir</label>
                        <div class="col-md-10">
                            <input class="form-control" type="text" id="tanggal_lahir" name="tanggal_lahir">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="no_ref" class="col-md-2 col-form-label">Email</label>
                        <div class="col-md-10">
                            <input class="form-control" type="email" id="email" name="email">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="no_ref" class="col-md-2 col-form-label">No. Telepon</label>
                        <div class="col-md-10">
                            <input class="form-control" type="text" id="no_telepon" name="no_telepon">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="no_ref" class="col-md-2 col-form-label">Password</label>
                        <div class="col-md-10">
                            <div class="input-group">
                                <input name="password" type="text" class="form-control" placeholder="Tuliskan Password Disini" id="password" required>
                                <div class="input-group-append ml-3">
                                    <div onclick="showPass()" id="panel" class="costum-center">
                                        <i class="ri-eye-line fa-lg"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Foto Profile</label>
                                <div class="col-md-10">
                                    <div class="custom-file">
                                        <input type="file" required onchange="validateFileType(this)" accept="image/*" class="custom-file-input" id="customFile" name="foto_profile">
                                        <label class="custom-file-label" for="customFile" id="file-name">Upload Foto Profile</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 text-center" id="imageFile">
                            <img src="" alt="" width="150" height="150"/>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12">
                            <button type="button" onclick="updateProfile()" class="btn btn-info waves-effect waves-light mr-1">Perbarui Profile</button>
                            <button type="button" class="btn btn-danger waves-effect waves-light">Batal</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@include('KepalaSekolah.Frame.footer')
<script src="{{url('assets/costum/js')}}/Admin/Profile/edit_profile.js"></script>