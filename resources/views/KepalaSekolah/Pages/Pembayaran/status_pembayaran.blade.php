@include('KepalaSekolah.Frame.head')
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <h4 class="mb-0">Data Status Pembayaran</h4>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
                <div class="col-md-3 pb-3">
                    <select onchange="handleSelectFilter(this)" id="selectFilter" class="pb-0 form-control select2">
                        
                    </select>
                    {{-- <select id="kelasFilter" class="pb-0 form-control select2" style="width: 300px">
                    
                    </select> --}}
                </div>
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table id="datatable-buttons" class="table table-striped table-bordered nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead>
                                <tr>
                                    <th class="text-center">No.</th>
                                    <th>Tanggal Bayar</th>
                                    <th class="text-center">NIS</th>
                                    <th class="text-center">Nama Siswa</th>
                                    <th class="text-center">No. Ref</th>
                                    <th class="text-center">Pemb. Via</th>
                                    <th class="text-center">Bulan Pemb.</th>
                                    <th class="text-center">Jumlah Pemb.</th>
                                    <th class="text-center">Status Pemb.</th>
                                </tr>
                            </thead>
                            <tbody id="generateData" class="roboto-font" style="font-size: 12px;">
                                
                            </tbody>
                        </table>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</div>

@include('KepalaSekolah.Frame.footer')
<script src="{{url('assets/costum/js')}}/KepalaSekolah/Pembayaran/status_pembayaran.js"></script>