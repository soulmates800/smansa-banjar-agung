</div>
    </div>
        </div>
        <script src="{{url('assets/vendors/libs')}}/jquery/jquery.min.js"></script>
        <script src="{{url('assets/vendors/libs')}}/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="{{url('assets/vendors/libs')}}/metismenu/metisMenu.min.js"></script>
        <script src="{{url('assets/vendors/libs')}}/simplebar/simplebar.min.js"></script>
        <script src="{{url('assets/vendors/libs')}}/node-waves/waves.min.js"></script>
        <script src="{{url('assets/vendors/libs')}}/admin-resources/jquery.vectormap/jquery-jvectormap-1.2.2.min.js"></script>
        <script src="{{url('assets/vendors/libs')}}/admin-resources/jquery.vectormap/maps/jquery-jvectormap-us-merc-en.js"></script>
        <script src="{{url('assets/vendors/libs')}}/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="{{url('assets/vendors/libs')}}/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
        <script src="{{url('assets/vendors/libs')}}/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
        <script src="{{url('assets/vendors/libs')}}/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>

        <script src="{{url('assets/vendors/libs')}}/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
        <script src="{{url('assets/vendors/libs')}}/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js"></script>
        <script src="{{url('assets/vendors/libs')}}/jszip/jszip.min.js"></script>
        <script src="{{url('assets/vendors/libs')}}/pdfmake/build/pdfmake.min.js"></script>
        <script src="{{url('assets/vendors/libs')}}/pdfmake/build/vfs_fonts.js"></script>
        <script src="{{url('assets/vendors/libs')}}/datatables.net-buttons/js/buttons.html5.min.js"></script>
        <script src="{{url('assets/vendors/libs')}}/datatables.net-buttons/js/buttons.print.min.js"></script>
        <script src="{{url('assets/vendors/libs')}}/datatables.net-buttons/js/buttons.colVis.min.js"></script>
        <script src="{{url('assets/vendors/libs')}}/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
        <script src="{{url('assets/vendors/libs')}}/datatables.net-select/js/dataTables.select.min.js"></script>
        <script src="{{url('assets/vendors/libs')}}/toastr/build/toastr.min.js"></script>
        <script src="{{url('assets/vendors/libs')}}/select2/js/select2.min.js"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.3/js/lightbox.min.js" integrity="sha512-k2GFCTbp9rQU412BStrcD/rlwv1PYec9SNrkbQlo6RZCf75l6KcC3UwDY8H5n5hl4v77IDtIPwOk9Dqjs/mMBQ==" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/gasparesganga-jquery-loading-overlay@2.1.7/dist/loadingoverlay.min.js" type="text/javascript"></script>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js" integrity="sha512-qTXRIMyZIFb8iQcfjXWCO8+M5Tbc38Qi5WzdPOYZHIlZpzBHG3L3by84BBBOiRGiEb7KKtAOAs5qYdUiZiQNNQ==" crossorigin="anonymous"></script>

        <script type="text/javascript">
            var BaseUrl = "{{ env('APP_SERVER_BASE') }}";
            var ServeUrl = "{{ env('APP_SERVER_API') }}";
        </script>

        <script src="{{url('assets/vendors/js')}}/app.js"></script>
        <script src="{{url('assets/costum/js')}}/global.js"></script>
        
    </body>
</html>