@include('WaliMurid.Frame.head')
<div class="row">
    <div class="col-6">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <h4 class="mb-0 dashboard-tittle">Dashboard | Selamat Datang Wali Murid</h4>
        </div>
    </div>
    <div class="col-6 text-right">
        <div class="page-title-box text-right">
            <h4 class="mb-0 dashboard-tittle" id="semester-dashboard"></h4>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xl-3">
        <div class="card">
            <div class="row no-gutters align-items-center">
                <div class="col-md-4 col-4">
                    <img class="card-img img-fluid" src="{{ url('assets/data/dash') }}/clock.png" style="height: 100px; width: 100px;" alt="Card image">
                </div>
                <div class="col-md-8 col-8">
                    <div class="card-body">
                        <h5 class="card-title" id="timeLive">12.00 WIB</h5>
                        <p class="card-text" id="currentDate">Senin, 01 Januari 2021</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xl-3">
        <div class="card">
            <div class="row no-gutters align-items-center">
                <div class="col-md-4 col-4">
                    <img class="card-img img-fluid" src="{{ url('assets/data/dash') }}/user.png" style="height: 100px; width: 100px;" alt="Card image">
                </div>
                <div class="col-md-8 col-8">
                    <div class="card-body">
                        <h5 class="card-title">Kepala Sekolah</h5>
                        <p class="card-text" style="font-size: 12px;">Hi. Danial Anwar, S.Pd., MM.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xl-3">
        <div class="card">
            <div class="row no-gutters align-items-center">
                <div class="col-md-4 col-4">
                    <img class="card-img img-fluid" src="{{ url('assets/data/dash') }}/users.png" style="height: 100px; width: 100px;" alt="Card image">
                </div>
                <div class="col-md-8 col-8">
                    <div class="card-body">
                        <h5 class="card-title">Jumlah Peserta Didik</h5>
                        <p class="card-text" id="jumlahPeserta"></p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xl-3">
        <div class="card">
            <div class="row no-gutters align-items-center">
                <div class="col-md-4 col-4">
                    <img class="card-img img-fluid" src="{{ url('assets/data/dash') }}/chart.png" style="height: 100px; width: 100px;" alt="Card image">
                </div>
                <div class="col-md-8 col-8">
                    <div class="card-body">
                        <h5 class="card-title">Total Pengeluaran</h5>
                        <p class="card-text" id="totalPengeluaran"></p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12 pb-2 border-bottom">
                        <h4>PENGUMUMAN</h4>
                    </div>
                    <div class="col-md-12 mt-2" id="pengumuman">
                        {{-- Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. --}}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12 pb-2 border-bottom">
                        <h4>Hubungi Kami Disini<i class="fab fa-whatsapp ml-2"></i></h4>
                    </div>
                    <div class="col-md-6 mt-4">
                        <h5>Pertanyaan Umum</h5>
                        <span>0896 8910 1066</span>
                    </div>
                    <div class="col-md-6 mt-4">
                        <h5>Social Media</h5>
                        <button class="btn btn-primary"><i class="fab fa-facebook mr-2"></i>Facebook</button>
                        <button class="btn btn-info"><i class="fab fa-twitter mr-2"></i>Twitter</button>
                        <button class="btn btn-danger"><i class="fab fa-instagram mr-2"></i>Instagram</button>
                        <h5 class="mt-3">Alamat</h5>
                        <span>Jl. Moris Jaya, Unit 3, Moris Jaya, Kec. Banjar Agung, Kab. Tulang Bawang Prov. Lampung</span>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
@include('WaliMurid.Frame.footer')
<script src="{{url('assets/costum/js')}}/WaliMurid/Home/dashboard.js"></script>