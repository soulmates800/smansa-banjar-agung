@include('WaliMurid.Frame.head')
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <h4 class="mb-0">Edit Profile</h4>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-header border-bottom bg-main text-white roboto-font">
                Edit data diri
            </div>
            <div class="card-body">
                <form id="profile-detail">
                    <div class="form-group row">
                        <label for="nis" class="col-md-2 col-form-label">NIS</label>
                        <div class="col-md-10">
                            <input class="form-control" type="text" id="nis" name="nis" required readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="nama" class="col-md-2 col-form-label">Nama Lengkap</label>
                        <div class="col-md-10">
                            <input class="form-control" type="text" id="nama" name="nama" required readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="tanggal_bayar" class="col-md-2 col-form-label">Jenis Kelamin</label>
                        <div class="col-md-10">
                            <input class="form-control" type="text" id="jenis_kelamin" name="jenis_kelamin" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="no_ref" class="col-md-2 col-form-label">Email</label>
                        <div class="col-md-10">
                            <input class="form-control" type="email" id="email" name="email" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="nama_bank" class="col-md-2 col-form-label">Rombel Saat Ini</label>
                        <div class="col-md-10">
                            <select id="rombel" class="pb-0 form-control select2" style="width: 300px">
                    
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="jumlah" class="col-md-2 col-form-label">Tempat Lahir</label>
                        <div class="col-md-10">
                            <input class="form-control" type="text" id="tempat_lahir" name="tempat_lahir" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="jumlah" class="col-md-2 col-form-label">Tanggal Lahir</label>
                        <div class="col-md-10">
                            <input class="form-control" type="text" id="tanggal_lahir" name="tanggal_lahir" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="jumlah" class="col-md-2 col-form-label">Nama Wali</label>
                        <div class="col-md-10">
                            <input class="form-control" type="text" id="nama_wali" name="nama_wali" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="jumlah" class="col-md-2 col-form-label">NIK Wali</label>
                        <div class="col-md-10">
                            <input class="form-control" type="text" id="nik_wali" name="nik_wali" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="jumlah" class="col-md-2 col-form-label">Asal SD</label>
                        <div class="col-md-10">
                            <input class="form-control" type="text" id="asal_sd" name="asal_sd" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="jumlah" class="col-md-2 col-form-label">Asal SMP</label>
                        <div class="col-md-10">
                            <input class="form-control" type="text" id="asal_smp" name="asal_smp" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="no_ref" class="col-md-2 col-form-label">Password</label>
                        <div class="col-md-10">
                            <div class="input-group">
                                <input name="password" type="text" class="form-control" placeholder="Tuliskan Password Disini" id="password" required>
                                <div class="input-group-append ml-3">
                                    <div onclick="showPass()" id="panel" class="costum-center">
                                        <i class="ri-eye-line fa-lg"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Foto Profile</label>
                                <div class="col-md-10">
                                    <div class="custom-file">
                                        <input type="file" required onchange="validateFileType(this)" accept="image/*" class="custom-file-input" id="customFile" name="foto_profile">
                                        <label class="custom-file-label" for="customFile" id="file-name">Upload Foto Profile</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 text-center" id="imageFile">
                            <img src="" alt="" width="150" height="150"/>
                        </div>
                    </div>
                </form>
                <div class="form-group row">
                    <div class="col-md-12">
                        <button type="button" class="btn btn-info waves-effect waves-light mr-1" onclick="updateProfile()">Update Profile</button>
                        {{-- <button type="button" class="btn btn-danger waves-effect waves-light">Batal</button> --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('WaliMurid.Frame.footer')
<script src="{{url('assets/costum/js')}}/WaliMurid/Profile/edit_profile.js"></script>