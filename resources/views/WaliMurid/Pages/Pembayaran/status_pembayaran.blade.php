@include('WaliMurid.Frame.head')
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <h4 class="mb-0">Data Status Pembayaran</h4>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12 text-right">
                        <span>Riwayat Pembayaran</span>
                        <select onchange="handleSelectFilter(this)" id="selectFilter" class="pb-0 select2 form-control" style="width: 300px">
                            <option></option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
                <span id="semester-aktif"></span>
                <div class="row" id="generateTemplate">
                    
                </div>
                <div class="col-md-12 text-right mt-3">
                    <span id="totalPengeluaran"></span>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table style="width: 100%;" id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <thead>
                            <tr>
                                <th class="text-center" style="width: 10%;">No.</th>
                                <th style="width: 70%;">Data Status Pembayaran</th>
                                <th class="text-center" style="width: 10%;">Status</th>
                                <th class="text-center" style="width: 10%;">Aksi</th>
                            </tr>
                        </thead>
                        <tbody id="generateData" class="roboto-font">
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@include('WaliMurid.Frame.footer')
<script src="{{url('assets/costum/js')}}/WaliMurid/Pembayaran/status_pembayaran.js"></script>