@include('WaliMurid.Frame.head')
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <h4 class="mb-0">Cara Pembayaran</h4>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-header border-bottom bg-main text-white roboto-font">
                Cara Pembayaran
            </div>
            <div class="card-body">
                <ol class="roboto-font">
                    <li class="p-2">Lakukan transfer ke rekening kami dan simpan foto slip pembayaran</li>
                    <li class="p-2">Buka situs resmi kami dan lakukan login menggunakan akun siswa</li>
                    <li class="p-2">Pilih menu "Input Pembayaran" yang ada pada menu "Pembayaran" di sebelah kiri</li>
                    <li class="p-2">Bila sudah, Pastikan anda melihat data pembayaran dengan teliti</li>
                    <li class="p-2">Isi semua kolom sesuai data pada slip pembayaran dengan benar</li>
                    <li class="p-2">Pada bagian kolom "Jumlah" masukkan nominal bayar sesuai dengan kesepakatan dengan sekolah</li>
                    <li class="p-2">Untuk kolom "Bulan Pembayaran" pilih nama bulan sesuai data yang belum anda lunasi dibagian atas</li>
                    <li class="p-2">Masukkan foto slip pembayaran sesuai dengan apa yang sudah anda isi. Pastikan foto terlihat dengan jelas</li>
                    <li class="p-2">Bila anda sudah mengisi semua kolom dan sudah yakin dengan apa yang anda isi silahkan tekan tombol kirim dibawah form</li>
                    <li class="p-2">Setelah selesai anda tinggal menunggu kami untuk meng-konfirmasi data yang sudah anda berikan</li>
                </ol>
            </div>
        </div>
    </div>
</div>

@include('WaliMurid.Frame.footer')