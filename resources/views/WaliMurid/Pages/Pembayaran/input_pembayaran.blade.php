@include('WaliMurid.Frame.head')
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <h4 class="mb-0">Input Pembayaran</h4>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
                <span id="semester-aktif"></span>
                <div class="row" id="generateTemplate">
                    
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-header border-bottom bg-main text-white roboto-font">
                Form Pembayaran
            </div>
            <div class="card-body">
                <form id="formPembayaran">
                    <div class="form-group row">
                        <label for="nis" class="col-md-2 col-form-label">NIS</label>
                        <div class="col-md-10">
                            <input class="form-control" type="text" id="nis" disabled readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="nama" class="col-md-2 col-form-label">NAMA</label>
                        <div class="col-md-10">
                            <input class="form-control bg-disabled" type="text" id="nama" disabled readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="tanggal_bayar" class="col-md-2 col-form-label">Tanggal Bayar</label>
                        <div class="col-md-10">
                            <input class="form-control bg-disabled" type="text" name="tanggal_bayar" id="tanggal_bayar" disabled readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="no_ref" class="col-md-2 col-form-label">No Ref</label>
                        <div class="col-md-10">
                            <input class="form-control" type="number" name="no_ref" id="no_ref" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="nama_bank" class="col-md-2 col-form-label">Bayar Via</label>
                        <div class="col-md-10">
                            <input class="form-control" type="tel" name="nama_bank" id="nama_bank" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="jumlah" class="col-md-2 col-form-label">Jumlah Pembayaran</label>
                        <div class="col-md-10">
                            <input class="form-control" oninput="getIdForRupiah(this)" type="text" id="jumlah_pembayaran" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="bulan" class="col-md-2 col-form-label">Bulan Pembayaran</label>
                        <div class="col-md-10">
                            <select class="form-control select2" name="bulan_pembayaran" id="generateSelectBulan" required>

                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="example-datetime-local-input" class="col-md-2 col-form-label">Upload Bukti Pembayaran</label>
                        <div class="col-md-10">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" name="bukti_pembayaran" id="customFile" onchange="validateFileType()" accept="image/*" required>
                                <label class="custom-file-label" id="fileName" for="customFile">Masukkan foto slip pembayaran</label>
                            </div>
                        </div>
                    </div>
                    <div class="alert alert-warning" role="alert">
                        <span class="bolder">WARNING!</span> Upload gambar bukti pembayaran dengan jelas untuk proses verifikasi
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12">
                            <button type="submit" id="btnkirimPembayaran" class="btn btn-info waves-effect waves-light mr-1">Kirim Pembayaran</button>
                            <button type="button" class="btn btn-danger waves-effect waves-light">Batal</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@include('WaliMurid.Frame.footer')
<script>
    var dataParamter = {!! json_encode($data) !!}
</script>
<script src="{{url('assets/costum/js')}}/WaliMurid/Pembayaran/input_pembayaran.js"></script>