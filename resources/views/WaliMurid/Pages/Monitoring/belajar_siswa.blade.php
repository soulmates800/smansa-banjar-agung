@include('WaliMurid.Frame.head')
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <h4 class="mb-0">Laporan Belajar Siswa</h4>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12 mb-3 text-right">
                        <select onchange="handleSelectFilter(this)" id="selectFilter" class="pb-0 select2 form-control" style="width: 300px;">
                            <option></option>
                        </select>
                    </div>
                    <div class="col-md-1">
                        <p>NIS</p>
                    </div>
                    <div class="col-md-11">
                        <p id="span_nis"></p>
                    </div>

                    <div class="col-md-1">
                        Nama
                    </div>
                    <div class="col-md-11">
                        <p id="span_nama"></p>
                    </div>
                    <div class="col-md-1">
                        Kelas
                    </div>
                    <div class="col-md-11">
                        <p id="span_kelas"></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <thead>
                            <tr>
                                <th class="text-center">No.</th>
                                <th>Semester</th>
                                <th class="text-center">Diperbarui Pada</th>
                                <th class="text-center">Aksi</th>
                            </tr>
                        </thead>
                        <tbody id="generateData">
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@include('WaliMurid.Frame.footer')
<script src="{{url('assets/costum/js')}}/WaliMurid/Monitoring/belajar_siswa.js"></script>