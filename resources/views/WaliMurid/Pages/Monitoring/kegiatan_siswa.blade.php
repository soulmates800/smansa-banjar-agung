@include('WaliMurid.Frame.head')
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <h4 class="mb-0">Aktifitas Kegiatan Siswa</h4>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
                <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                    <thead>
                        <tr>
                            <th class="text-center">No.</th>
                            <th>Kegiatan Ekstrakulikuler</th>
                            <th class="text-center">Prestasi</th>
                            <th class="text-center">Diraih Pada</th>
                            <th class="text-center">Status</th>
                            {{-- <th class="text-center">Diperbarui Pada</th> --}}
                        </tr>
                    </thead>
                    <tbody id="generateData">
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@include('WaliMurid.Frame.footer')
<script src="{{url('assets/costum/js')}}/WaliMurid/Monitoring/kegiatan_siswa.js"></script>