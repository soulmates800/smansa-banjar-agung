<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>SMANSA - SMA Negeri 1 Banjar Agung</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="SMANSA - SMA Negeri 1 Banjar Agung" name="Reky" />
    <meta content="smansa" name="Reky" />
    <link href="{{ url('assets/costum') }}/css/login.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css" integrity="sha512-HK5fgLBL+xu6dm/Ii3z4xhlSUyZgTT9tuc/hSrtw6uzJOvgRr2a9jyxxT1ely+B+xFAmJKVSTbpM/CuL7qxO8w==" crossorigin="anonymous" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@600&display=swap" rel="stylesheet">
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>

{{-- Ganti backgorund di body --class="bgtheme"-- --}}
<body>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="costum-center">
                <img class="bg" src="{{ url('assets/costum/images/login-background.png') }}" />
                <div class="row card-handle">
                    <div class="card_costum_login">
                        <div class="container_form_login">
                            <form id="authLogin">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <img src="{{ url('assets/costum/images/logo.png') }}" class="logo-login" />
                                    </div>
                                    <div class="col-md-12 text-center mt-2">
                                        <h4><b>SMA Negeri 1 Banjar Agung 2</b></h4>
                                        <span class="deskripsi-login">Selamat datang di website monitoring aktivitas siswa dan pembayaran uang sumbangan wali murid SMA Negeri 1 Banjar Agung</span>
                                    </div>
                                    <div class="col-md-12 mt-5 mb-3">
                                        <input placeholder="NIS" name="nis" type="text" class="form-control">
                                    </div>
                                    <div class="col-md-12">
                                        <input placeholder="Password" name="password" type="password" class="form-control">
                                    </div>
                                    <div class="col-md-12 mb-2" style="text-align: right;">
                                        <a href="{{ url('lupa-password') }}" class="deskripsi-login">Lupa Password ?</a>
                                    </div>
                                    <div class="col-md-12 mb-3">
                                        <button type="submit" class="btn_login_costum">Login</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>
</body>
<script src="{{ url('assets/costum/js') }}/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/gasparesganga-jquery-loading-overlay@2.1.7/dist/loadingoverlay.min.js" type="text/javascript"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>

<script type="text/javascript">
    var BaseUrl = "{{ env('APP_SERVER_BASE') }}";
    var ServeUrl = "{{ env('APP_SERVER_API') }}";
</script>

<script src="{{ url('assets/costum/js') }}/login.js"></script>