<?php

namespace App\Http\Controllers\Backend;

use App\Models\DataSemester;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class BackendController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public function uploadFile($value)
    {
        $file = $value;
        $nama_file = time()."_".$file->getClientOriginalName();
        $tujuan_upload = public_path('/assets/data/images');
        $file->move($tujuan_upload,$nama_file);
        return $nama_file;
    }

    public function get_aktivated_semester()
    {
        $query = DataSemester::where('status', 'Aktif')->first();
        if ($query) {
            return $query;
        }
    }
}
