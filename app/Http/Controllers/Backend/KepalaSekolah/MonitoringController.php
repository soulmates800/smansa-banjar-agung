<?php

namespace App\Http\Controllers\Backend\KepalaSekolah;
use App\Http\Controllers\Backend\BackendController as Controller;
use App\Models\AktifitasSiswa;
use App\Models\DataKegiatan;
use App\Models\DataKelas;
use App\Models\DataSemester;
use App\Models\LaporanBelajar;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
class MonitoringController extends Controller
{
    public function get_aktifitas(Request $request) {
        if ($request->kelas == 'semua' && $request->kegiatan == 'semua') {
            $query = AktifitasSiswa::get();

        } else if ($request->kelas == 'semua' && $request->kegiatan != 'semua') {
            $query = AktifitasSiswa::where('kegiatan', $request->kegiatan)->get();

        } else if ($request->kelas != 'semua' && $request->kegiatan == 'semua') {
            $query = AktifitasSiswa::where('rombel', $request->kelas)->get();

        } else if ($request->kelas != 'semua' && $request->kegiatan != 'semua') {
            $query = AktifitasSiswa::where([['kegiatan', $request->kegiatan],['rombel', $request->kelas]])->get();

        } else {
            $query = AktifitasSiswa::get();
        }
        
        return $query;
    }
    
    public function get_laporan(Request $request) {
        
        if ($request->kelas == 'semua' && $request->semester == 'semua') {
            $query = LaporanBelajar::get();

        } else if ($request->kelas == 'semua' && $request->semester != 'semua') {
            $query = LaporanBelajar::where('semester', $request->semester)->get();

        } else if ($request->kelas != 'semua' && $request->semester == 'semua') {
            $query = LaporanBelajar::where('rombel', $request->kelas)->get();
            // dd($query);

        } else if ($request->kelas != 'semua' && $request->semester != 'semua') {
            $query = LaporanBelajar::where([['semester', $request->semester],['rombel', $request->kelas]])->get();

        } else {
            $query = LaporanBelajar::get();
        }

        // $query = LaporanBelajar::get();
        return $query;
    }

    public function get_kegiatan()
    {
        $query = DataKegiatan::get();
        return $query;
    }

    public function get_kelas()
    {
        $query = DataKelas::get();
        return $query;
    }

    public function get_semester()
    {
        $query = DataSemester::get();
        return $query;
    }
}
