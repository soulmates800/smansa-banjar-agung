<?php

namespace App\Http\Controllers\Backend\KepalaSekolah;
use App\Http\Controllers\Backend\BackendController as Controller;
use App\Models\InputPembayaran;
use App\Models\StatusPembayaran;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PembayaranController extends Controller
{
    public function input_pembayaran(Request $request) {

        $bukti_pembayaran = $this->uploadFile($request->bukti_pembayaran);
        $query = InputPembayaran::create([
            'user_id' => Auth::user()->id,
            'nama_user' => Auth::user()->nama,
            'nis' => Auth::user()->nis,
            'tanggal_bayar' => $request->tanggal_bayar,
            'no_ref' => $request->no_ref,
            'nama_bank' => $request->nama_bank,
            'jumlah_pembayaran' => $request->jumlah_pembayaran,
            'bulan_pembayaran' => $request->bulan_pembayaran,
            'bukti_pembayaran' => $bukti_pembayaran,
            'status' => 'Diproses',
        ]);

        if ($query) {
            return $request;
        }
    }

    public function get_pembayaran($filter)
    {
        $query = InputPembayaran::where('user_id', Auth::user()->id)->orderBy('created_at', 'DESC')->get();
        if ($query) {
            return $query;
        }
    }

    public function bulan_status() {
        $query = StatusPembayaran::where('user_id', Auth::user()->id)->get();
        if ($query) {
            return $query;
        }
    }
}
