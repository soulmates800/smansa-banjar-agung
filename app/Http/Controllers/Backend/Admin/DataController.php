<?php

namespace App\Http\Controllers\Backend\Admin;
use App\Http\Controllers\Backend\BackendController as Controller;
use App\Models\DataKegiatan;
use App\Models\DataKelas;
use App\Models\DataSemester;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DataController extends Controller
{
    public function get_kelas()
    {
        $kelas = DataKelas::get();

        for ($i=0; $i < count($kelas); $i++) { 
            $query['jumlah_siswa'][$i] = count(User::where('rombel', $kelas[$i]['kelas'])->get());
            $query['kelas'][$i] = $kelas[$i];
        }

        if ($query) {
            return $query;
        }
    }

    public function hapus_kelas($id)
    {
        $query = DataKelas::where('id', $id)->delete();
        if ($query) {
            return $query;
        }
    }

    public function tambah_kelas(Request $request)
    {
        $query = DataKelas::create([
            'kelas' => $request->kelas
        ]);
        if ($query) {
            return $query;
        }
    }

    public function get_kegiatan()
    {
        $query = DataKegiatan::get();
        if ($query) {
            return $query;
        }
    }

    public function hapus_kegiatan($id)
    {
        $query = DataKegiatan::where('id', $id)->delete();
        if ($query) {
            return $query;
        }
    }

    public function tambah_kegiatan(Request $request)
    {
        $query = DataKegiatan::create([
            'kegiatan' => $request->kegiatan
        ]);
        if ($query) {
            return $query;
        }
    }

    public function get_semester()
    {
        $query['aktif_semester'] = $this->get_aktivated_semester();
        $query['semester'] = DataSemester::get();
        if ($query) {
            return $query;
        }
    }

    public function hapus_semester($id)
    {
        $query = DataSemester::where('id', $id)->delete();
        if ($query) {
            return $query;
        }
    }

    public function tambah_semester(Request $request)
    {
        $check = DataSemester::where('semester', $request->semester)->first();
        
        if ($check == null) {
            $query = DataSemester::create([
                'semester' => $request->semester,
                'tipe' => $request->tipe
            ]);
        }
        
        if ($query) {
            return $query;
        }
    }

    public function aktifkan_semester($id)
    {

        DataSemester::where('id_helper', 1)->update([
            'status' => NULL
        ]);
        
        $query = DataSemester::where('id', $id)->update([
            'status' => 'Aktif'
        ]);

        if ($query) {
            return $query;
        }
    }
}
