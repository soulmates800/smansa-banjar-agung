<?php

namespace App\Http\Controllers\Backend\Admin;
use App\Http\Controllers\Backend\BackendController as Controller;
use App\Models\LaporanBelajar;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BelajarSiswaController extends Controller
{
    public function get_siswa(Request $request) {
        
        $query = '';

        if ($request->semester == 'all' && $request->kelas == 'all') {
            $query = User::where('roles', 'wali_murid')->get();
        } else if ($request->semester == 'all' && $request->kelas != 'all') {
            $query = User::where([['rombel' , $request->kelas],['roles', 'wali_murid']])->get();
        } else if ($request->semester != 'all' && $request->kelas == 'all') {
            $laporan = LaporanBelajar::where('semester' , $request->semester)->get();
            for ($i=0; $i < count($laporan); $i++) { 
                $query = User::where([['id', $laporan[$i]->user_id],['roles', 'wali_murid']])->get();
            }
            
        } else if ($request->semester != 'all' && $request->kelas != 'all') {

            $laporan = LaporanBelajar::where('semester' , $request->semester)->get();
            for ($i=0; $i < count($laporan); $i++) { 
                $query = User::where([['id', $laporan[$i]->user_id],['rombel', $request->kelas],['roles', 'wali_murid']])->get();
            }
        } else {
            $query = User::where('roles', 'wali_murid')->get();
        }

        if ($query) {
            return $query;
        }
    }

    public function detail_belajar($user_id) {
        $query['laporan'] = LaporanBelajar::where('user_id', $user_id)->get();
        $query['user'] = User::where('id', $user_id)->first();
        if ($query) {
            return $query;
        }
    }

    public function tambah_semester(Request $request) {

        $user = User::where('id', $request->user_id)->first();

        $file_laporan = $this->uploadFile($request->file);
        $query = LaporanBelajar::create([
            'user_id' => $user->id,
            'nis' => $user->nis,
            'rombel' => $user->rombel,
            'nama_user' => $user->nama,
            'semester' => $request->semester,
            'file' => $file_laporan,
        ]);

        if ($query) {
            return $request;
        }
    }

    public function update_semester(Request $request) {

        if($request->hasFile('file')){
            $file_laporan = $this->uploadFile($request->file);
            $query = LaporanBelajar::where('id', $request->id)->update([
                'semester' => $request->semester,
                'file' => $file_laporan,
            ]);
        } else {
            $query = LaporanBelajar::where('id', $request->id)->update([
                'semester' => $request->semester,
            ]);
        }

        if ($query) {
            return $request;
        }
    }

    public function hapus_semester($id)
    {
        $query = LaporanBelajar::where('id', $id)->delete();
        if ($query) {
            return $query;
        }
    }
}
