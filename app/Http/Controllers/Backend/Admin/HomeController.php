<?php


namespace App\Http\Controllers\Backend\Admin;
use App\Http\Controllers\Backend\BackendController as Controller;
use App\Models\InputPembayaran;
use App\Models\Pengumuman;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function get_pengumuman()
    {
        $query['aktif_semester'] = $this->get_aktivated_semester();
        $query['pengumuman'] = Pengumuman::first();
        $query['total_user'] = count(User::where('roles', 'wali_murid')->get());
        $pembayaran = InputPembayaran::where('semester', $query['aktif_semester']['semester'])->whereYear('created_at', date('Y'))->get('jumlah_pembayaran');
        $query['total_pembayaran'] = 0;

        for ($i=0; $i < count($pembayaran); $i++) { 
            $query['total_pembayaran'] = $query['total_pembayaran'] + $pembayaran[$i]->jumlah_pembayaran;
        }

        if ($query) {
            return $query;
        }
    }

    public function kirim_pengumuman(Request $request)
    {
        $query = Pengumuman::where('id', 1)->update([
            'pengumuman' => $request->pengumuman
        ]);
        if ($query) {
            return $query;
        }
    }
}
