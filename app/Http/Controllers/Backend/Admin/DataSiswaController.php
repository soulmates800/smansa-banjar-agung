<?php

namespace App\Http\Controllers\Backend\Admin;
use App\Http\Controllers\Backend\BackendController as Controller;
use App\Models\AktifitasSiswa;
use App\Models\DataKelas;
use App\Models\DataSemester;
use App\Models\InputPembayaran;
use App\Models\Kegiatan;
use App\Models\LaporanBelajar;
use App\Models\StatusPembayaran;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class DataSiswaController extends Controller
{
    public function get_siswa($kelas) {
        $query['aktif_semester'] = $this->get_aktivated_semester();
        if ($kelas != 'semua') {
            $check = DataKelas::where('id', $kelas)->first();
            $query['siswa'] = User::where([['roles', 'wali_murid'],['rombel', $check['kelas']]])->get();
        } else {
            $query['siswa'] = User::where('roles', 'wali_murid')->get();
        }
       
        if ($query) {
            return $query;
        }
    }

    public function tambah_siswa(Request $request) {

        $foto_profile = '';
        if ($request->hasFile('foto_profile')) {
            $foto_profile = $this->uploadFile($request->foto_profile);
        } else {
            $foto_profile = 'default.png';
        }

        $checkNis = User::where('nis', $request->nis)->first();
        if ($checkNis != null) {
            return response('NIS telah digunakan', 422);
        }

        $checkNis = User::where('email', $request->email)->first();
        if ($checkNis != null) {
            return response('Email telah digunakan', 422);
        }

        $user_id = User::create([
            'nis' => $request->nis,
            'nama' => $request->nama,
            'jenis_kelamin' => $request->jenis_kelamin,
            'email' => $request->email,
            'password' => $request->password,
            'rombel' => $request->rombel,
            'tempat_lahir' => $request->tempat_lahir,
            'tanggal_lahir' => $request->tanggal_lahir,
            'nama_wali' => $request->nama_wali,
            'nik_wali' => $request->nik_wali,
            'asal_sd' => $request->asal_sd,
            'asal_smp' => $request->asal_smp,
            'roles' => "wali_murid",
            'foto_profile' => $foto_profile,
            'remember_token' => Carbon::now()
        ])->id;

        if ($request->kegiatan != null) {
            for ($i=0; $i < count($request->kegiatan); $i++) { 
                $query = AktifitasSiswa::create([
                    'user_id' => $user_id,
                    'nama_user' => $request->nama,
                    'rombel' => $request->rombel,
                    'kegiatan' => $request->kegiatan[$i],
                    'diraih_pada' => $request->diraih_pada[$i],
                    'prestasi' => $request->prestasi[$i],
                    'status' => $request->status[$i],
                ]);
            }
        }

        return $user_id;
    }

    public function get_kegiatan() {
        $query = Kegiatan::get();
        if ($query) {
            return $query;
        }
    }

    public function detail_siswa($user_id) {
        $query['user'] = User::where('id', $user_id)->first();
        $query['kegiatan'] = AktifitasSiswa::where('user_id', $user_id)->get();
        if ($query) {
            return $query;
        }
    }

    public function update_siswa(Request $request) {

        $foto_profile = '';
        if ($request->hasFile('foto_profile')) {
            $foto_profile = $this->uploadFile($request->foto_profile);

            $query = User::where('id', $request->id)->update([
                'nis' => $request->nis,
                'nama' => $request->nama,
                'jenis_kelamin' => $request->jenis_kelamin,
                'email' => $request->email,
                'password' => $request->password,
                'rombel' => $request->rombel,
                'tempat_lahir' => $request->tempat_lahir,
                'tanggal_lahir' => $request->tanggal_lahir,
                'nama_wali' => $request->nama_wali,
                'nik_wali' => $request->nik_wali,
                'asal_sd' => $request->asal_sd,
                'asal_smp' => $request->asal_smp,
                'foto_profile' => $foto_profile,
            ]);

        } else {

            $query = User::where('id', $request->id)->update([
                'nis' => $request->nis,
                'nama' => $request->nama,
                'jenis_kelamin' => $request->jenis_kelamin,
                'email' => $request->email,
                'password' => $request->password,
                'rombel' => $request->rombel,
                'tempat_lahir' => $request->tempat_lahir,
                'tanggal_lahir' => $request->tanggal_lahir,
                'nama_wali' => $request->nama_wali,
                'nik_wali' => $request->nik_wali,
                'asal_sd' => $request->asal_sd,
                'asal_smp' => $request->asal_smp,
            ]);

        }

        if ($request->kegiatan != null) {

            AktifitasSiswa::where('user_id', $request->id)->delete();
            for ($i=0; $i < count($request->kegiatan); $i++) {
                $query = AktifitasSiswa::create([
                    'user_id' => $request->id,
                    'nama_user' => $request->nama,
                    'rombel' => $request->rombel,
                    'kegiatan' => $request->kegiatan[$i],
                    'prestasi' => $request->prestasi[$i],
                    'diraih_pada' => $request->diraih_pada[$i],
                    'status' => $request->status[$i],
                ]);
            }
        }

        if ($query) {
            return $query;
        }
    }

    public function cetak_siswa($kelas) {

        if ($kelas != 'semua') {
            $check = DataKelas::where('id', $kelas)->first();
            $query['siswa'] = User::where([['roles', 'wali_murid'],['rombel', $check['kelas']]])->get();
        } else {
            $query['siswa'] = User::where('roles', 'wali_murid')->get();
        }
        if ($query) {
            return $query;
        }
    }

    public function cetak_per_siswa($user_id) {
        $query = User::where([['roles', 'wali_murid'],['id', $user_id]])->first();
        if ($query) {
            return $query;
        }
    }

    public function hapus_siswa($user_id) {
        $query = User::where('id', $user_id)->delete();

        if ($query) {
            return $query;
        }
    }

    public function status_pembayaran_siswa(Request $request)
    {
        $query['semester'] = DataSemester::where('id', $request->semester_id)->first();

        // dd($query['semester']);
        $query['pembayaran'] = InputPembayaran::where([['user_id', $request->user_id],['semester', $query['semester']['semester']]])->orderBy('created_at', 'DESC')->get();
        $query['status_pembayaran'] = StatusPembayaran::where('user_id', $request->user_id)->get();
        if ($query) {
            return $query;
        }
    }
}