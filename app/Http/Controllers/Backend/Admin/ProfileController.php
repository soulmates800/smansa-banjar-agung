<?php

namespace App\Http\Controllers\Backend\Admin;
use App\Http\Controllers\Backend\BackendController as Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    public function get_profile()
    {
        $query = User::where('id', Auth::user()->id)->first();
        if ($query) {
            return $query;
        }
    }

    public function update_profile(Request $request) {

        $foto_profile = '';
        if ($request->hasFile('foto_profile')) {
            $foto_profile = $this->uploadFile($request->foto_profile);
            $query = User::where('id', Auth::user()->id)->update([
                'nis' => $request->nis,
                'nama' => $request->nama,
                'jenis_kelamin' => $request->jenis_kelamin,
                'tanggal_lahir' => $request->tanggal_lahir,
                'email' => $request->email,
                'no_telepon' => $request->no_telepon,
                'password' => $request->password,
                'foto_profile' => $foto_profile,
            ]);
        } else {
            $query = User::where('id', Auth::user()->id)->update([
                'nis' => $request->nis,
                'nama' => $request->nama,
                'jenis_kelamin' => $request->jenis_kelamin,
                'tanggal_lahir' => $request->tanggal_lahir,
                'email' => $request->email,
                'no_telepon' => $request->no_telepon,
                'password' => $request->password,
            ]);
        }

        if ($query) {
            return $query;
        }

    }
}
