<?php

namespace App\Http\Controllers\Backend\Admin;
use App\Http\Controllers\Backend\BackendController as Controller;
use App\Models\DataKelas;
use App\Models\DataSemester;
use App\Models\InputPembayaran;
use App\Models\LaporanBelajar;
use App\Models\StatusPembayaran;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PembayaranController extends Controller
{
    public function get_pembayaran($filter) {
        $query['aktif_semester'] = $this->get_aktivated_semester();
        if ($filter == 'semua') {
            $query['pembayaran'] = InputPembayaran::orderBy('created_at', 'DESC')->get();
        } else {
            $getSemester = DataSemester::where('id', $filter)->first();
            $query['pembayaran'] = InputPembayaran::where('semester', $getSemester['semester'])->orderBy('created_at', 'DESC')->get();
        }
        
        if ($query) {
            return $query;
        }
    }

    public function detail_pembayaran($id) {
        $query['pembayaran'] = InputPembayaran::where('id', $id)->first();
        if ($query) {
            return $query;
        }
    }

    public function update_pembayaran(Request $request) {

        $semester = $this->get_aktivated_semester();

        $query['pembayaran'] = InputPembayaran::where('id', $request->id)->update([
            'status' => $request->status
        ]);

        $info_pembayaran = InputPembayaran::where('id', $request->id)->first();

        $status = StatusPembayaran::where([['user_id', $info_pembayaran->user_id],['bulan', $info_pembayaran->bulan_pembayaran],['semester', $semester['semester']]])->get();

        $stats = '';
        if ($request->status == 'Diverifikasi') {
            $stats = 'Lunas';
        } else {
            $stats = 'Belum Lunas';
        }

        if (count($status) != 0) {
            StatusPembayaran::where([['user_id', $info_pembayaran->user_id],['bulan', $info_pembayaran->bulan_pembayaran]])->update([
                'status' => $stats
            ]);
        } else {
            StatusPembayaran::create([
                'user_id' => $info_pembayaran->user_id,
                'nama_user' => $info_pembayaran->nama_user,
                'bulan' => $info_pembayaran->bulan_pembayaran,
                'jumlah' => $info_pembayaran->jumlah_pembayaran,
                'status' => $stats,
                'semester' => $semester['semester'],
            ]);
        }
        
        if ($query) {
            return $query;
        }
    }

    public function bulan_status($user_id) {
        $query['semester'] = $this->get_aktivated_semester();
        // $query['pembayaran'] = StatusPembayaran::where('user_id', $user_id)->get();

        
        $query['pembayaran'] = InputPembayaran::where([['user_id', $user_id],['semester', $query['semester']['semester']]])->orderBy('created_at', 'DESC')->get();
        $query['status_pembayaran'] = StatusPembayaran::where('user_id', $user_id)->get();

        if ($query) {
            return $query;
        }
    }

    public function cetak_pembayaran(Request $request) {
        
        if ($request->kelas != 'semua') {
            $check = DataKelas::where('id', $request->kelas)->first();
            $query['user'] = User::where([['roles', 'wali_murid'],['rombel', $check['kelas']]])->get();
            $query['kelas'] = $check['kelas'];
        } else {
            $query['user'] = User::where('roles', 'wali_murid')->get();
            $query['kelas'] = 'Seluruh Kelas';
        }

        $query['semester'] = DataSemester::where('id', $request->semester)->first();
        if ($query['semester']['tipe'] == 'Ganjil') {
            $bulan = [
                "Januari",
                "Februari",
                "Maret",
                "April",
                "Mei",
                "Juni",
            ];
        } else {
            $bulan = [
                "Juli",
                "Agustus",
                "September",
                "Oktober",
                "November",
                "Desember",
            ];
        }

        for ($i=0; $i < count($query['user']); $i++) {
            for ($j=0; $j < count($bulan); $j++) {
                $status = StatusPembayaran::where([['bulan', $bulan[$j]], ['user_id', $query['user'][$i]->id], ['semester', $query['semester']['semester']]])->first();
                if ($status != null) {
                    $query['user'][$i][$bulan[$j]] = $status->jumlah;
                } else {
                    $query['user'][$i][$bulan[$j]] = 0;
                }
            }
        }

        if ($query) {
            return $query;
        }

    }
}
