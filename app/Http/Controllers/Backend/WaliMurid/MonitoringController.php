<?php

namespace App\Http\Controllers\Backend\WaliMurid;
use App\Http\Controllers\Backend\BackendController as Controller;
use App\Models\AktifitasSiswa;
use App\Models\DataSemester;
use App\Models\LaporanBelajar;
use Illuminate\Support\Facades\Auth;

class MonitoringController extends Controller
{
    public function get_aktifitas() {
        $query = AktifitasSiswa::where('user_id', Auth::user()->id)->get();
        return $query;
    }
    
    public function get_laporan($filter_semester) {
        $query['aktif_semester'] = $this->get_aktivated_semester();

        if ($filter_semester == 'null') {
            $aktifSemester['semester'] = $query['aktif_semester']['semester'];
        } else {
            $aktifSemester = DataSemester::where('id', $filter_semester)->first('semester');
        }
        $query['laporan'] = LaporanBelajar::where([['user_id', Auth::user()->id],['semester', $aktifSemester['semester']]])->get();
        $query['user'] = Auth::user();

        return $query;
    }

}
