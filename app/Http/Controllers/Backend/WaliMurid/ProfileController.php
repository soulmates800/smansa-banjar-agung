<?php

namespace App\Http\Controllers\Backend\WaliMurid;
use App\Http\Controllers\Backend\BackendController as Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    public function get_profile()
    {
        $query = User::where('id', Auth::user()->id)->first();
        if ($query) {
            return $query;
        }
    }

    public function update_profile(Request $request) {

        $foto_profile = '';
        if ($request->hasFile('foto_profile')) {
            $foto_profile = $this->uploadFile($request->foto_profile);
            $query = User::where('id', Auth::user()->id)->update([
                'nis' => $request->nis,
                'nama' => $request->nama,
                'jenis_kelamin' => $request->jenis_kelamin,
                'email' => $request->email,
                'rombel' => $request->rombel,
                'password' => $request->password,
                'tempat_lahir' => $request->tempat_lahir,
                'tanggal_lahir' => $request->tanggal_lahir,
                'nama_wali' => $request->nama_wali,
                'nik_wali' => $request->nik_wali,
                'asal_sd' => $request->asal_sd,
                'asal_smp' => $request->asal_smp,
                'foto_profile' => $foto_profile,
            ]);
        } else {
            $query = User::where('id', Auth::user()->id)->update([
                'nis' => $request->nis,
                'nama' => $request->nama,
                'jenis_kelamin' => $request->jenis_kelamin,
                'email' => $request->email,
                'password' => $request->password,
                'rombel' => $request->rombel,
                'tempat_lahir' => $request->tempat_lahir,
                'tanggal_lahir' => $request->tanggal_lahir,
                'nama_wali' => $request->nama_wali,
                'nik_wali' => $request->nik_wali,
                'asal_sd' => $request->asal_sd,
                'asal_smp' => $request->asal_smp,
            ]);
        }

        if ($query) {
            return $query;
        }

    }
}
