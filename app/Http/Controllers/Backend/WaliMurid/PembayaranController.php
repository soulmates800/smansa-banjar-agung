<?php

namespace App\Http\Controllers\Backend\WaliMurid;
use App\Http\Controllers\Backend\BackendController as Controller;
use App\Models\DataSemester;
use App\Models\InputPembayaran;
use App\Models\StatusPembayaran;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PembayaranController extends Controller
{
    public function input_pembayaran(Request $request) {

        $semester = $this->get_aktivated_semester();
        $bukti_pembayaran = $this->uploadFile($request->bukti_pembayaran);
        $query = InputPembayaran::create([
            'user_id' => Auth::user()->id,
            'nama_user' => Auth::user()->nama,
            'semester' => $semester['semester'],
            'nis' => Auth::user()->nis,
            'tanggal_bayar' => $request->tanggal_bayar,
            'no_ref' => $request->no_ref,
            'nama_bank' => $request->nama_bank,
            'jumlah_pembayaran' => $request->jumlah_pembayaran,
            'bulan_pembayaran' => $request->bulan_pembayaran,
            'bukti_pembayaran' => $bukti_pembayaran,
            'status' => 'Diproses',
        ]);

        // $check = StatusPembayaran::where([['user_id', Auth::user()->id], ['bulan', $request->bulan_pembayaran], ['semester', $semester['semester']]])->first();
        // if ($check == null) {
        //     $query['pembayaran'] = StatusPembayaran::create([
        //         'user_id' => Auth::user()->id,
        //         'semester' => $semester['semester'],
        //         'nama_user' => Auth::user()->nama,
        //         'bulan' => $request->bulan_pembayaran,
        //         'jumlah' => $request->jumlah_pembayaran,
        //         'status' => 'Diproses',
        //     ]);
        // } else {
        //     $totalJumlah = $check->jumlah + $request->jumlah_pembayaran;
        //     $query['pembayaran'] = StatusPembayaran::where('id', $check->id)->update([
        //         'jumlah' => $totalJumlah,
        //         'status' => 'Diproses',
        //     ]);
        // }

        if ($query) {
            return $request;
        }
    }

    public function get_pembayaran($filter_semester)
    {
        $query['semester'] = $this->get_aktivated_semester();
        
        if ($filter_semester == 'null') {
            $aktifSemester['semester'] = $query['semester']['semester'];
        } else {
            $aktifSemester = DataSemester::where('id', $filter_semester)->first('semester');
            $query['semester'] = DataSemester::where('id', $filter_semester)->first();
        }
        $query['pembayaran'] = InputPembayaran::where([['user_id', Auth::user()->id],['semester', $aktifSemester['semester']],['hapus_user_status', 0]])->orderBy('created_at', 'DESC')->get();
        $query['status_pembayaran'] = StatusPembayaran::where([['user_id', Auth::user()->id],['semester', $query['semester']['semester']]])->get();
        if ($query) {
            return $query;
        }
    }

    public function get_semua_semester()
    {
        $query['semua_semester'] = DataSemester::get();
        if ($query) {
            return $query;
        }
    }

    public function hapus_pembayaran($id)
    {
        // $check = InputPembayaran::where('id', $id)->first();
        // $pembayaran = StatusPembayaran::where([['user_id', Auth::user()->id],['semester', $check['semester']],['bulan', $check['bulan_pembayaran']]])->first();
        // $totalPembayaran = $pembayaran['jumlah'] - $check['jumlah_pembayaran'];
        // dd($check['jumlah_pembayaran']);
        // if ($totalPembayaran) {
        //     return $totalPembayaran;
        // }

        // $check = InputPembayaran::where('id', $id)->first();
        // $pembayaran = StatusPembayaran::where([['user_id', Auth::user()->id],['semester', $check['semester']],['bulan', $check['bulan_pembayaran']]])->delete();
        $query = InputPembayaran::where('id', $id)->update([
            'hapus_user_status' => 1
        ]);
        
        if ($query) {
            return $query;
        }
    }

    public function bulan_status() {
        $query['semester'] = $this->get_aktivated_semester();
        $query['pembayaran'] = StatusPembayaran::where('user_id', Auth::user()->id)->get();
        if ($query) {
            return $query;
        }
    }
}
