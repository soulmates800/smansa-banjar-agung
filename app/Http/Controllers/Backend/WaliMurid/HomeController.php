<?php


namespace App\Http\Controllers\Backend\WaliMurid;
use App\Http\Controllers\Backend\BackendController as Controller;
use App\Models\InputPembayaran;
use App\Models\Pengumuman;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function get_dashboard()
    {
        $query['aktif_semester'] = $this->get_aktivated_semester();
        $query['pengumuman'] = Pengumuman::first();
        $query['total_user'] = count(User::where('roles', 'wali_murid')->get());
        $pembayaran = InputPembayaran::where([['user_id', Auth::user()->id],['semester', $query['aktif_semester']['semester']]])->whereYear('created_at', date('Y'))->get('jumlah_pembayaran');
        $query['total_pembayaran'] = 0;
        // dd($pembayaran);
 
        for ($i=0; $i < count($pembayaran); $i++) { 
            $query['total_pembayaran'] = $query['total_pembayaran'] + $pembayaran[$i]->jumlah_pembayaran;
        }

        if ($query) {
            return $query;
        }
    }
}
