<?php

namespace App\Http\Controllers\Auth;
use App\Http\Controllers\Auth\MainAuthController as Controller;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

class LoginControllers extends Controller
{
    public function AuthLogin(Request $request)
		{
		  try {
			
			$user = User::where('nis', $request->nis)->first();

			if ($request->nis != $user->nis) {
				return response()->json([
					'_status' => 422,
					'message' => 'User tidak ditemukan',
				  ]);
			}
			
			if ($request->password != $user->password) {
				return response()->json([
					'_status' => 422,
					'message' => 'Password salah',
				  ]);
			}

            $tokenResult = $user->createToken('authToken')->plainTextToken;
			
			Auth::login($user);
			
			return response()->json([
			  '_status' => 200,
			  '_token' => $tokenResult,
              'token_type' => 'Bearer',
              'roles' => $user->roles,
              'message' => 'Selamat datang kembali ' . $user->name,
			]);
		  } catch (Exception $error) {
			return response()->json([
			  '_status' => 500,
              'message' => $error->getMessage(),
			  'error' => $error,
			]);
		  }
		}

		public function AuthRegister(Request $request)
		{
			$query = User::create([
				'name' => $request->name,
				'nis' => $request->nis,
				'telepon' => null,
				'nis_verified_at' => Carbon::now(),
				'password' => Hash::make($request->password),
				'roles' => 'user',
				'picture' => 'default.png',
			]);
			if ($query) {
				return response()->json([
					'_status' => 200,
					'_message' => 'Akun berhasil dibuat'
				  ]);
			}
		}

	public function AuthLogout(Request $request)
    {
	// Auth::guard('web')->logout();

	$request->session()->invalidate();

	$request->session()->regenerateToken();

	Auth::logout();

	return redirect('/');
    }
}