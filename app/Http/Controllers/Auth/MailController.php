<?php


namespace App\Http\Controllers\Auth;
use App\Http\Controllers\Auth\MainAuthController as Controller;
use App\Mail\PasswordMail;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class MailController extends Controller
{
    public function sendEmail($email)
    {

        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < 15; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        User::where('email', $email)->update([
            'password' => $randomString
        ]);
        
        $detail = [
            'password' => $randomString,
            'email' => $email
        ];

        $data["email"] = $email;

        
        Mail::to($email)->send(new PasswordMail($detail));
        return view('LupaPassword.email_terkirim',compact('data'));
    }
}
