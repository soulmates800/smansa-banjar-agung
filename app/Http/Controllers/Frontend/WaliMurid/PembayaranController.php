<?php

namespace App\Http\Controllers\Frontend\WaliMurid;
use App\Http\Controllers\Frontend\FrontendController as Controller;
use Illuminate\Support\Facades\Auth;

class PembayaranController extends Controller
{
    public function status_pembayaran() {
        $data["user"] = Auth::user();
        return view('WaliMurid.Pages.Pembayaran.status_pembayaran',compact('data'));
    }

    public function cara_pembayaran() {
        $data["user"] = Auth::user();
        return view('WaliMurid.Pages.Pembayaran.cara_pembayaran',compact('data'));
    }

    public function input_pembayaran() {
        $data["user"] = Auth::user();
        return view('WaliMurid.Pages.Pembayaran.input_pembayaran',compact('data'));
    }
}
