<?php

namespace App\Http\Controllers\Frontend\Admin;
use App\Http\Controllers\Frontend\FrontendController as Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
class DataSiswaController extends Controller
{
    public function daftar_siswa() {
        $data["user"] = Auth::user();
        return view('Admin.Pages.DataSiswa.daftar_siswa',compact('data'));
    }

    public function tambah_siswa() {
        $data["user"] = Auth::user();
        return view('Admin.Pages.DataSiswa.tambah_siswa',compact('data'));
    }

    public function edit_siswa() {
        $data["user"] = Auth::user();
        return view('Admin.Pages.DataSiswa.edit_siswa',compact('data'));
    }

    public function cetak_siswa() {
        $data["user"] = Auth::user();
        return view('Admin.Pages.DataSiswa.cetak_siswa',compact('data'));
    }

    public function cetak_per_siswa(Request $request) {
        $data["params"] = $request->all();
        $data["user"] = Auth::user();
        return view('Admin.Pages.DataSiswa.cetak_per_siswa',compact('data'));
    }
}
