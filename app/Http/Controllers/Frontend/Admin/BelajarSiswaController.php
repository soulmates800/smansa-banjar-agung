<?php

namespace App\Http\Controllers\Frontend\Admin;
use App\Http\Controllers\Frontend\FrontendController as Controller;
use Illuminate\Support\Facades\Auth;

class BelajarSiswaController extends Controller
{
    public function belajar_siswa() {
        $data["user"] = Auth::user();
        return view('Admin.Pages.BelajarSiswa.daftar_siswa',compact('data'));
    }

    public function belajar_detail() {
        $data["user"] = Auth::user();
        return view('Admin.Pages.BelajarSiswa.belajar_siswa',compact('data'));
    }
}