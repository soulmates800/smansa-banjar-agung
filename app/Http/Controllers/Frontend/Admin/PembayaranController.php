<?php

namespace App\Http\Controllers\Frontend\Admin;
use App\Http\Controllers\Frontend\FrontendController as Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
class PembayaranController extends Controller
{
    public function data_pembayaran() {
        $data["user"] = Auth::user();
        return view('Admin.Pages.DataPembayaran.data_pembayaran',compact('data'));
    }

    public function proses_pembayaran() {
        $data["user"] = Auth::user();
        return view('Admin.Pages.DataPembayaran.proses_pembayaran',compact('data'));
    }

    public function cetak_pembayaran(Request $request) {
        $data["params"] = $request->all();
        $data["user"] = Auth::user();
        return view('Admin.Pages.DataPembayaran.cetak_pembayaran',compact('data'));
    }
}
