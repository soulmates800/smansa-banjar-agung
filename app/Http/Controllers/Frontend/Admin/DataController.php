<?php

namespace App\Http\Controllers\Frontend\Admin;
use App\Http\Controllers\Frontend\FrontendController as Controller;
use Illuminate\Support\Facades\Auth;

class DataController extends Controller
{
    public function kelas() {
        $data["user"] = Auth::user();
        return view('Admin.Pages.MasterData.kelas',compact('data'));
    }

    public function kegiatan() {
        $data["user"] = Auth::user();
        return view('Admin.Pages.MasterData.kegiatan',compact('data'));
    }

    public function semester() {
        $data["user"] = Auth::user();
        return view('Admin.Pages.MasterData.semester',compact('data'));
    }
}