<?php


namespace App\Http\Controllers\Frontend\KepalaSekolah;
use App\Http\Controllers\Frontend\FrontendController as Controller;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function dashboard() {
        $data["user"] = Auth::user();
        return view('KepalaSekolah.Pages.Home.dashboard',compact('data'));
    }
}
