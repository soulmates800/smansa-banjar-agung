<?php

namespace App\Http\Controllers\Frontend\KepalaSekolah;
use App\Http\Controllers\Frontend\FrontendController as Controller;
use Illuminate\Support\Facades\Auth;

class PembayaranController extends Controller
{
    public function status_pembayaran() {
        $data["user"] = Auth::user();
        return view('KepalaSekolah.Pages.Pembayaran.status_pembayaran',compact('data'));
    }
}
