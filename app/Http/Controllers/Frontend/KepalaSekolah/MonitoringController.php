<?php

namespace App\Http\Controllers\Frontend\KepalaSekolah;
use App\Http\Controllers\Frontend\FrontendController as Controller;
use Illuminate\Support\Facades\Auth;

class MonitoringController extends Controller
{
    public function belajar_siswa() {
        $data["user"] = Auth::user();
        return view('KepalaSekolah.Pages.Monitoring.belajar_siswa',compact('data'));
    }

    public function kegiatan_siswa() {
        $data["user"] = Auth::user();
        return view('KepalaSekolah.Pages.Monitoring.kegiatan_siswa',compact('data'));
    }
}
