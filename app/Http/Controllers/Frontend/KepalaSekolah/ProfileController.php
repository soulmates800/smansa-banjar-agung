<?php

namespace App\Http\Controllers\Frontend\KepalaSekolah;
use App\Http\Controllers\Frontend\FrontendController as Controller;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    public function edit_profile() {
        $data["user"] = Auth::user();
        return view('KepalaSekolah.Pages.Profile.edit_profile',compact('data'));
    }
}