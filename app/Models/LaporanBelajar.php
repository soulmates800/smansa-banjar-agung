<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LaporanBelajar extends Model
{
    protected $table = 'laporan_belajar';

    protected $primaryKey = 'id';
	
	protected $guarded = [];
}
