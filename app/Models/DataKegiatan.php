<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DataKegiatan extends Model
{
    protected $table = 'data_kegiatan';

    protected $primaryKey = 'id';
	
	protected $guarded = [];
}
