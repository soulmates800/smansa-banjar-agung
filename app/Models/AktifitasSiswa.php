<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class AktifitasSiswa extends Model
{
    protected $table = 'aktifitas_siswa';

    protected $primaryKey = 'id';
	
	protected $guarded = [];
}
