<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InputPembayaran extends Model
{
    protected $table = 'input_pembayaran';

    protected $primaryKey = 'id';
	
	protected $guarded = [];
}
