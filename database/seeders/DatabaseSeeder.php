<?php

namespace Database\Seeders;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'roles' => 'kepala_sekolah',
            'nama' => 'Reky Pratama AR',
            'nis' => '11111111',
            'jenis_kelamin' => '-',
            'email' => 'kepsek@gmail.com',
            'password' => Hash::make('admin'),
            'rombel' => '-',
            'tempat_lahir' => '-',
            'tanggal_lahir' => '-',
            'nama_wali' => '-',
            'nik_wali' => '-',
            'asal_sd' => '-',
            'asal_smp' => '-',
            'foto_profile' => 'default.png',
            'remember_token' => Str::random(100),
        ]);
    }
}
