<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\Auth as AuthPath;
use \App\Http\Controllers\Backend\WaliMurid as WaliMuridPath;
use \App\Http\Controllers\Backend\Admin as AdminPath;
use \App\Http\Controllers\Backend\KepalaSekolah as KepalaSekolahPath;
use \App\Http\Controllers\Backend as BackendPath;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/login', [AuthPath\LoginControllers::class, 'AuthLogin']);
Route::post('/logout', [AuthPath\LoginControllers::class, 'AuthLogout'])->middleware('auth')->name('logout');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('wali_murid')->group(function () {

    Route::prefix('/dashboard')->group(function () {
        Route::get('/get_pengumuman', [AdminPath\HomeController::class, 'get_pengumuman']);
        Route::get('/get_dashboard', [WaliMuridPath\HomeController::class, 'get_dashboard']);
    });

    Route::prefix('/monitoring')->group(function () {
        Route::get('/get_aktifitas', [WaliMuridPath\MonitoringController::class, 'get_aktifitas']);
        Route::get('/get_laporan/{filter_semester}', [WaliMuridPath\MonitoringController::class, 'get_laporan']);
    });
    
    Route::prefix('/pembayaran')->group(function () {
        Route::get('/get_pembayaran/{filter_semester}', [WaliMuridPath\PembayaranController::class, 'get_pembayaran']);
        Route::post('/input_pembayaran', [WaliMuridPath\PembayaranController::class, 'input_pembayaran']);
        Route::get('/bulan_status', [WaliMuridPath\PembayaranController::class, 'bulan_status']);
        Route::get('/get_semua_semester', [WaliMuridPath\PembayaranController::class, 'get_semua_semester']);
        Route::get('/hapus_pembayaran/{id}', [WaliMuridPath\PembayaranController::class, 'hapus_pembayaran']);
    });

    Route::prefix('/profile')->group(function () {
        Route::get('/get_profile', [WaliMuridPath\ProfileController::class, 'get_profile']);
        Route::post('/update_profile', [WaliMuridPath\ProfileController::class, 'update_profile']);
    });
    
});

Route::prefix('admin')->group(function () {

    Route::prefix('/dashboard')->group(function () {
        Route::get('/get_pengumuman', [AdminPath\HomeController::class, 'get_pengumuman']);
        Route::post('/kirim_pengumuman', [AdminPath\HomeController::class, 'kirim_pengumuman']);
    });

    Route::prefix('/belajar_siswa')->group(function () {
        Route::get('/get_siswa', [AdminPath\BelajarSiswaController::class, 'get_siswa']);
        Route::get('/detail_belajar/{user_id}', [AdminPath\BelajarSiswaController::class, 'detail_belajar']);
        Route::post('/tambah_semester', [AdminPath\BelajarSiswaController::class, 'tambah_semester']);
        Route::post('/update_semester', [AdminPath\BelajarSiswaController::class, 'update_semester']);
        Route::get('/hapus_semester/{id}', [AdminPath\BelajarSiswaController::class, 'hapus_semester']);
    });

    Route::prefix('/data_siswa')->group(function () {
        Route::get('/get_siswa/{kelas}', [AdminPath\DataSiswaController::class, 'get_siswa']);
        Route::post('/tambah_siswa', [AdminPath\DataSiswaController::class, 'tambah_siswa']);
        Route::get('/get_kegiatan', [AdminPath\DataSiswaController::class, 'get_kegiatan']);
        Route::get('/detail_siswa/{user_id}', [AdminPath\DataSiswaController::class, 'detail_siswa']);
        Route::post('/update_siswa', [AdminPath\DataSiswaController::class, 'update_siswa']);
        Route::get('/cetak_siswa/{kelas}', [AdminPath\DataSiswaController::class, 'cetak_siswa']);
        Route::get('/cetak_per_siswa/{user_id}', [AdminPath\DataSiswaController::class, 'cetak_per_siswa']);
        Route::get('/hapus_siswa/{user_id}', [AdminPath\DataSiswaController::class, 'hapus_siswa']);
        Route::post('/status_pembayaran_siswa', [AdminPath\DataSiswaController::class, 'status_pembayaran_siswa']);
    });

    Route::prefix('/pembayaran')->group(function () {
        Route::get('/get_pembayaran/{filter}', [AdminPath\PembayaranController::class, 'get_pembayaran']);
        Route::get('/detail_pembayaran/{id}', [AdminPath\PembayaranController::class, 'detail_pembayaran']);
        Route::post('/update_pembayaran', [AdminPath\PembayaranController::class, 'update_pembayaran']);
        Route::get('/bulan_status/{user_id}', [AdminPath\PembayaranController::class, 'bulan_status']);
        Route::post('/cetak_pembayaran', [AdminPath\PembayaranController::class, 'cetak_pembayaran']);
    });

    Route::prefix('/profile')->group(function () {
        Route::get('/get_profile', [AdminPath\ProfileController::class, 'get_profile']);
        Route::post('/update_profile', [AdminPath\ProfileController::class, 'update_profile']);
    });

    Route::prefix('/master_data')->group(function () {
        Route::get('/get_kelas', [AdminPath\DataController::class, 'get_kelas']);
        Route::get('/hapus_kelas/{id}', [AdminPath\DataController::class, 'hapus_kelas']);
        Route::post('/tambah_kelas', [AdminPath\DataController::class, 'tambah_kelas']);

        Route::get('/get_kegiatan', [AdminPath\DataController::class, 'get_kegiatan']);
        Route::get('/hapus_kegiatan/{id}', [AdminPath\DataController::class, 'hapus_kegiatan']);
        Route::post('/tambah_kegiatan', [AdminPath\DataController::class, 'tambah_kegiatan']);

        Route::get('/get_semester', [AdminPath\DataController::class, 'get_semester']);
        Route::get('/hapus_semester/{id}', [AdminPath\DataController::class, 'hapus_semester']);
        Route::post('/tambah_semester', [AdminPath\DataController::class, 'tambah_semester']);
        Route::get('/aktifkan_semester/{id}', [AdminPath\DataController::class, 'aktifkan_semester']);
    });

});

Route::prefix('kepala_sekolah')->group(function () {

    Route::prefix('/monitoring')->group(function () {
        Route::post('/get_aktifitas', [KepalaSekolahPath\MonitoringController::class, 'get_aktifitas']);
        Route::post('/get_laporan', [KepalaSekolahPath\MonitoringController::class, 'get_laporan']);
        Route::get('/get_kegiatan', [KepalaSekolahPath\MonitoringController::class, 'get_kegiatan']);
        Route::get('/get_kelas', [KepalaSekolahPath\MonitoringController::class, 'get_kelas']);
        Route::get('/get_semester', [KepalaSekolahPath\MonitoringController::class, 'get_semester']);
    });
    
    Route::prefix('/pembayaran')->group(function () {
        Route::get('/get_pembayaran', [KepalaSekolahPath\PembayaranController::class, 'get_pembayaran']);
        Route::post('/input_pembayaran', [KepalaSekolahPath\PembayaranController::class, 'input_pembayaran']);
        Route::get('/bulan_status', [KepalaSekolahPath\PembayaranController::class, 'bulan_status']);
    });

});

Route::get('/get_aktivated_semester', [BackendPath\BackendController::class, 'get_aktivated_semester']);