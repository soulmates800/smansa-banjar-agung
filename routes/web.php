<?php

use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\Frontend\WaliMurid as WaliMuridPath;
use \App\Http\Controllers\Frontend\Admin as AdminPath;
use \App\Http\Controllers\Frontend\KepalaSekolah as KepalaSekolahPath;
use \App\Http\Controllers\Auth as AuthPath;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login');
});

Route::get('/lupa-password', function () {
    return view('LupaPassword.form_password');
});

Route::get('/send-email/{email}', [AuthPath\MailController::class, 'sendEmail']);

Route::prefix('wali-murid')->group(function () {
    Route::prefix('home')->group(function () {
        Route::get('/', [WaliMuridPath\HomeController::class, 'dashboard']);
    });

    Route::prefix('monitoring')->group(function () {
        Route::get('/belajar-siswa', [WaliMuridPath\MonitoringController::class, 'belajar_siswa']);
        Route::get('/kegiatan-siswa', [WaliMuridPath\MonitoringController::class, 'kegiatan_siswa']);
    });

    Route::prefix('pembayaran')->group(function () {
        Route::get('/status-pembayaran', [WaliMuridPath\PembayaranController::class, 'status_pembayaran']);
        Route::get('/cara-pembayaran', [WaliMuridPath\PembayaranController::class, 'cara_pembayaran']);
        Route::get('/input-pembayaran', [WaliMuridPath\PembayaranController::class, 'input_pembayaran']);
    });

    Route::prefix('profile')->group(function () {
        Route::get('/edit-profile', [WaliMuridPath\ProfileController::class, 'edit_profile']);
    });
    
});

Route::prefix('admin')->group(function () {
    Route::prefix('home')->group(function () {
        Route::get('/', [AdminPath\HomeController::class, 'dashboard']);
    });

    Route::prefix('belajar-siswa')->group(function () {
        Route::get('/', [AdminPath\BelajarSiswaController::class, 'belajar_siswa']);
        Route::get('/detail/{user_id}', [AdminPath\BelajarSiswaController::class, 'belajar_detail']);
    });

    Route::prefix('data-siswa')->group(function () {
        Route::get('/', [AdminPath\DataSiswaController::class, 'daftar_siswa']);
        Route::get('/tambah_siswa', [AdminPath\DataSiswaController::class, 'tambah_siswa']);
        Route::get('/edit_siswa/{user_id}', [AdminPath\DataSiswaController::class, 'edit_siswa']);
        Route::get('/cetak_siswa/{kelas}', [AdminPath\DataSiswaController::class, 'cetak_siswa']);
        Route::get('/cetak_per_siswa', [AdminPath\DataSiswaController::class, 'cetak_per_siswa']);
    });

    Route::prefix('data-pembayaran')->group(function () {
        Route::get('/', [AdminPath\PembayaranController::class, 'data_pembayaran']);
        Route::get('/proses_pembayaran/{id_pembayaran}', [AdminPath\PembayaranController::class, 'proses_pembayaran']);
        Route::get('/cetak_pembayaran', [AdminPath\PembayaranController::class, 'cetak_pembayaran']);
    });

    Route::prefix('profile')->group(function () {
        Route::get('/edit_profile', [AdminPath\ProfileController::class, 'edit_profile']);
    });

    Route::prefix('master-data')->group(function () {
        Route::get('/kelas', [AdminPath\DataController::class, 'kelas']);
        Route::get('/kegiatan', [AdminPath\DataController::class, 'kegiatan']);
        Route::get('/semester', [AdminPath\DataController::class, 'semester']);
    });

});

Route::prefix('kepala-sekolah')->group(function () {
    Route::prefix('home')->group(function () {
        Route::get('/', [KepalaSekolahPath\HomeController::class, 'dashboard']);
    });

    Route::prefix('monitoring')->group(function () {
        Route::get('/belajar-siswa', [KepalaSekolahPath\MonitoringController::class, 'belajar_siswa']);
        Route::get('/kegiatan-siswa', [KepalaSekolahPath\MonitoringController::class, 'kegiatan_siswa']);
    });

    Route::prefix('pembayaran')->group(function () {
        Route::get('/status_pembayaran', [KepalaSekolahPath\PembayaranController::class, 'status_pembayaran']);
    });

    Route::prefix('profile')->group(function () {
        Route::get('/edit_profile', [KepalaSekolahPath\ProfileController::class, 'edit_profile']);
    });

});