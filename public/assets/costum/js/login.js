// var ServeUrl = "http://smansa.spacearts.id/api";
// var BaseUrl = "http://smansa.spacearts.id";

// var ServeUrl = "http://127.0.0.1:8000/api";
// var BaseUrl = "http://127.0.0.1:8000";

var csrf_token = $('meta[name="csrf-token"]').attr('content');
$("#authLogin").submit(function(event) {
    event.preventDefault();
    
    var form = $('#authLogin')[0];
    var data = new FormData(form);
    data.append("_token", csrf_token );
    $.ajax({
        data: data,
        url: ServeUrl + "/login",
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        type: 'POST',
        dataType: 'json',
        beforeSend: function() {
            startLoading();
        },
        complete: function(response) {
            stopLoading();
        },
        success: function(response) {

            console.log(`response`, response)

            if (response._status == 200) {
                Swal.fire({
                    title: '<div style="color: white;">Berhasil!</div>',
                    html: '<p class="card-text"><small class="text-muted">Berhasil melakukan login. <br> Anda akan diarahkan kehalaman dashboard.</small></p>',
                    type: 'success',
                    icon: 'success',
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'Pergi ke Dashboard!',
                    background: '#14274E',
                    buttonsStyling: true,
                    onClose: function() {
                        switch (response.roles) {
                            case 'wali_murid':
                                window.location.href = BaseUrl + '/wali-murid/home/'
                                break;
                            case 'admin':
                                window.location.href = BaseUrl + '/admin/home/'
                                break;
                            case 'kepala_sekolah':
                                window.location.href = BaseUrl + '/kepala-sekolah/home/'
                                break;
                            default:
                                break;
                        }
                    }
                })
            } else {
                Swal.fire({
                    title: '<div style="color: white;">'+response._status+'</div>',
                    html: '<p class="card-text"><small class="text-white">'+response.message+'</small></p>',
                    type: 'error',
                    icon: 'error',
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'Coba Lagi!',
                    background: '#D8691D',
                    buttonsStyling: true,
                    onClose: function() {
                        
                    }
                })
            }
            

        },error: function(response) {
            stopLoading();
        },
    })
});

function startLoading() {
    $.LoadingOverlay("show", {
        image : BaseUrl + '/assets/costum/images/loading.gif',
        zIndex : 1000000,
        imageAnimation: "0ms",
        fade: [0, 0],
        size: 200,
        maxSize: 200,
        background: "#d6eaff52"
    });
}

function stopLoading() {
    $.LoadingOverlay("hide");
}

$(document).ajaxStop(function() {
    stopLoading()
});