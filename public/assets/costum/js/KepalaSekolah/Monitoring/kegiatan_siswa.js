
function reinitDatatable() {
    var a = $("#datatable-buttons").DataTable({
        lengthChange: !1,
        language: {
            paginate: {
                previous: "<i class='mdi mdi-chevron-left'>",
                next: "<i class='mdi mdi-chevron-right'>",
            },
        },
        drawCallback: function () {
            $(".dataTables_paginate > .pagination").addClass(
                "pagination-rounded"
            );
        },
        // buttons: ["copy", "excel", "pdf"],
    });
    a
    .buttons()
    .container()
    .appendTo("#datatable-buttons_wrapper .col-md-6:eq(0)"),
    $("#selection-datatable").DataTable({
        select: { style: "multi" },
        language: {
            paginate: {
                previous: "<i class='mdi mdi-chevron-left'>",
                next: "<i class='mdi mdi-chevron-right'>",
            },
        },
        drawCallback: function () {
            $(".dataTables_paginate > .pagination").addClass(
                "pagination-rounded"
            );
        },
    })
}

function getKegiatan(kelas, kegiatan) {
    $('#datatable-buttons').DataTable().clear();
    $('#datatable-buttons').DataTable().destroy();
    var request = new FormData();

    request.append("kelas", kelas );
    request.append("kegiatan", kegiatan );

    request.append("_token", csrf_token );

    $.ajax({
        data: request,
        url: ServeUrl + '/kepala_sekolah/monitoring/get_aktifitas',
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        type: 'POST',
        dataType: 'json',
        beforeSend: function() {
            startLoading();
        },
        complete: function() {
            stopLoading();
        },
        success: function(response) {
            var data = response;
            var html = '';
            var number = 1;
            for (let i = 0; i < data.length; i++) {
                html += '<tr>';
                html += '<td class="text-center">'+number+'</td>';
                html += '<td>'+data[i].nama_user+'</td>';
                html += '<td class="text-center">'+data[i].rombel+'</td>';
                html += '<td>'+data[i].kegiatan+'</td>';
                html += '<td class="text-center">'+data[i].prestasi+'</td>';
                html += '<td class="text-center">'+data[i].diraih_pada+'</td>';
                html += '<td class="text-center">'+data[i].status+'</td>';
                html += '<td class="text-center">'+formatMomentTime(data[i].updated_at)+'</td>';
                html += '</tr>';
                number++;
            }
            $('#generateData').html(html);
            
            reinitDatatable()
        },error: function(xhr) {
            stopLoading();
            toastr["error"](xhr.responseJSON.message)
        },
    })
}

getKegiatan('semua', 'semua')

function formatMomentTime(value) {
    return moment(value).format('DD MMMM YYYY - h:mm:ss a');
}

function getSemuaKegiatan() {
    $.ajax({
        url: ServeUrl + '/kepala_sekolah/monitoring/get_kegiatan',
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        type: 'GET',
        dataType: 'json',
        beforeSend: function() {
            startLoading();
        },
        complete: function() {
            stopLoading();
        },
        success: function(response) {
            var data = response
            var selectFilter = '';
            selectFilter += '<option selected value="semua">Seluruh Kegiatan</option>'
            for (let i = 0; i < data.length; i++) {
                selectFilter += '<option value="'+data[i].kegiatan+'">'+data[i].kegiatan+'</option>'
            }
            $('#kegiatanFilter').html(selectFilter);
        },error: function(xhr) {
    
        },
    })
}
getSemuaKegiatan()

function getKelas() {
    $.ajax({
        url: ServeUrl + '/kepala_sekolah/monitoring/get_kelas',
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        type: 'GET',
        dataType: 'json',
        beforeSend: function() {
            startLoading();
        },
        complete: function() {
            stopLoading();
        },
        success: function(response) {
            var data = response
            var selectFilter = '';
            selectFilter += '<option selected value="semua">Seluruh Kelas</option>'
            for (let i = 0; i < data.length; i++) {
                selectFilter += '<option value="'+data[i].kelas+'">'+data[i].kelas+'</option>'
            }
            $('#kelasFilter').html(selectFilter);
        },error: function(xhr) {
    
        },
    })
}
getKelas()

function handleChange() {
    var kelas = $('#kelasFilter').val();
    var semester = $('#kegiatanFilter').val();
    getKegiatan(kelas, semester)
}

$(".select2").select2();