
function reinitDatatable() {
    var a = $("#datatable-buttons").DataTable({
        lengthChange: !1,
        language: {
            paginate: {
                previous: "<i class='mdi mdi-chevron-left'>",
                next: "<i class='mdi mdi-chevron-right'>",
            },
        },
        drawCallback: function () {
            $(".dataTables_paginate > .pagination").addClass(
                "pagination-rounded"
            );
        },
        // buttons: ["copy", "excel", "pdf"],
    });
    a
    .buttons()
    .container()
    .appendTo("#datatable-buttons_wrapper .col-md-6:eq(0)"),
    $("#selection-datatable").DataTable({
        select: { style: "multi" },
        language: {
            paginate: {
                previous: "<i class='mdi mdi-chevron-left'>",
                next: "<i class='mdi mdi-chevron-right'>",
            },
        },
        drawCallback: function () {
            $(".dataTables_paginate > .pagination").addClass(
                "pagination-rounded"
            );
        },
    })
}

function getLaporan(kelas, semester) {

    $('#datatable-buttons').DataTable().clear();
    $('#datatable-buttons').DataTable().destroy();
    var request = new FormData();

    request.append("kelas", kelas );
    request.append("semester", semester );

    request.append("_token", csrf_token );

    $.ajax({
        data: request,
        url: ServeUrl + '/kepala_sekolah/monitoring/get_laporan',
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        type: 'POST',
        dataType: 'json',
        beforeSend: function() {
            startLoading();
        },
        complete: function() {
            stopLoading();
        },
        success: function(response) {
            var data = response;
            var html = '';
            var number = 1;
            for (let i = 0; i < data.length; i++) {
                html += '<tr>';
                html += '<td class="text-center align-middle">'+number+'</td>';
                html += '<td class="align-middle text-center">'+data[i].nis+'</td>';
                html += '<td class="align-middle">'+data[i].nama_user+'</td>';
                html += '<td class="align-middle text-center">'+data[i].rombel+'</td>';
                html += '<td class="text-center"><a href="'+ BaseUrl + '/assets/data/images/' + data[i].file +'" class="btn btn-sm btn-info waves-effect waves-light">Download</a></td>';
                html += '</tr>';
                number++;
            }
            $('#generateData').html(html);
            
            reinitDatatable()
        },error: function(xhr) {
            stopLoading();
            toastr["error"](xhr.responseJSON.message)
        },
    })
}
getLaporan('semua', 'semua')

function handleChange() {
    var kelas = $('#kelasFilter').val();
    var semester = $('#semesterFilter').val();
    getLaporan(kelas, semester)
}

function formatMomentTime(value) {
    return moment(value).format('DD MMMM YYYY - h:mm:ss a');
}

function getSemuaSemester() {
    $.ajax({
        url: ServeUrl + '/kepala_sekolah/monitoring/get_semester',
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        type: 'GET',
        dataType: 'json',
        beforeSend: function() {
            startLoading();
        },
        complete: function() {
            stopLoading();
        },
        success: function(response) {
            var data = response
            var selectFilter = '';
            selectFilter += '<option selected value="semua">Seluruh Semester</option>'
            for (let i = 0; i < data.length; i++) {
                selectFilter += '<option value="'+data[i].semester+'">'+data[i].semester+'</option>'
            }
            $('#semesterFilter').html(selectFilter);
        },error: function(xhr) {
    
        },
    })
}
getSemuaSemester()

function getKelas() {
    $.ajax({
        url: ServeUrl + '/kepala_sekolah/monitoring/get_kelas',
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        type: 'GET',
        dataType: 'json',
        beforeSend: function() {
            startLoading();
        },
        complete: function() {
            stopLoading();
        },
        success: function(response) {
            var data = response
            var selectFilter = '';
            selectFilter += '<option selected value="semua">Seluruh Kelas</option>'
            for (let i = 0; i < data.length; i++) {
                selectFilter += '<option value="'+data[i].kelas+'">'+data[i].kelas+'</option>'
            }
            $('#kelasFilter').html(selectFilter);
        },error: function(xhr) {
    
        },
    })
}
getKelas()

$(".select2").select2();