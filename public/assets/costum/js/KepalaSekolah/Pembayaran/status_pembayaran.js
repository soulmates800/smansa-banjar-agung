var semesterAktif;
var selectFilterHelperBug = 0;
function getDataPembayaran(filter) {

    $('#datatable-buttons').DataTable().clear();
    $('#datatable-buttons').DataTable().destroy();

    $.ajax({
        url: ServeUrl + '/admin/pembayaran/get_pembayaran/' + filter,
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        type: 'GET',
        dataType: 'json',
        beforeSend: function() {
            startLoading();
        },
        complete: function() {
            stopLoading();
        },
        success: function(response) {
            var data = response.pembayaran;;
            var html = '';
            var numberIncrement = 1;

            for (let i = 0; i < data.length; i++) {
                html += '<tr>';
                html += '<td class="align-middle text-center">'+numberIncrement+'</td>';
                html += '<td class="align-middle text-center">'+data[i].tanggal_bayar+'</td>';
                html += '<td class="align-middle text-center">'+data[i].nis+'</td>';
                html += '<td class="align-middle">'+data[i].nama_user+'</td>';
                html += '<td class="align-middle text-center">'+data[i].no_ref+'</td>';
                html += '<td class="align-middle text-center">'+data[i].nama_bank+'</td>';
                html += '<td class="align-middle text-center">'+data[i].bulan_pembayaran+'</td>';
                html += '<td class="align-middle text-center">'+formatRupiah(String(data[i].jumlah_pembayaran), 'Rp.')+'</td>';

                if (data[i].status == 'Diverifikasi') {
                    html += '<td class="align-middle text-center"><button type="button" class="btn btn-success btn-sm verifikasi-info"><i class="fas fa-check-circle fa-sm mr-2"></i>Diverifikasi</button></td>';
                } else if (data[i].status == 'Ditolak') {
                    html += '<td class="align-middle text-center"><button type="button" class="btn btn-danger btn-sm tolak-info"><i class="fas fa-window-close fa-sm mr-2"></i>Ditolak</button></td>';
                } else {
                    html += '<td class="align-middle text-center"><button type="button" class="btn btn-info btn-sm proses-info"><i class="far fa-clock fa-sm mr-2"></i>Diproses</button></td>';
                }

                html += '</tr>';
                numberIncrement++;
            }

            $('#generateData').html(html);
            reinitDatatable()
        },error: function(xhr) {
            stopLoading();
            toastr["error"](xhr.responseJSON.message)
        },
    })
}
getDataPembayaran('semua');

function reinitDatatable() {
    var a = $("#datatable-buttons").DataTable({
        lengthChange: !1,
        language: {
            paginate: {
                previous: "<i class='mdi mdi-chevron-left'>",
                next: "<i class='mdi mdi-chevron-right'>",
            },
        },
        drawCallback: function () {
            $(".dataTables_paginate > .pagination").addClass(
                "pagination-rounded"
            );
        },
        // buttons: ["copy", "excel", "pdf"],
    });
    a
    .buttons()
    .container()
    .appendTo("#datatable-buttons_wrapper .col-md-6:eq(0)"),
    $("#selection-datatable").DataTable({
        select: { style: "multi" },
        language: {
            paginate: {
                previous: "<i class='mdi mdi-chevron-left'>",
                next: "<i class='mdi mdi-chevron-right'>",
            },
        },
        drawCallback: function () {
            $(".dataTables_paginate > .pagination").addClass(
                "pagination-rounded"
            );
        },
    })
}

function getSemuaSemester() {
    $.ajax({
        url: ServeUrl + '/wali_murid/pembayaran/get_semua_semester',
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        type: 'GET',
        dataType: 'json',
        beforeSend: function() {
            startLoading();
        },
        complete: function() {
            stopLoading();
        },
        success: function(response) {
            var dataSelectFilter = response.semua_semester
            var selectFilter = '';
            selectFilter += '<option selected value="semua">Semua Semester</option>'
            for (let i = 0; i < dataSelectFilter.length; i++) {
                
                if (dataSelectFilter[i].semester == semesterAktif && selectFilterHelperBug == 0) {
                    selectFilterHelperBug = 1;
                    selectFilter += '<option selected value="'+dataSelectFilter[i].id+'">'+dataSelectFilter[i].semester+'</option>'
                } else {
                    selectFilter += '<option value="'+dataSelectFilter[i].id+'">'+dataSelectFilter[i].semester+'</option>'
                }
            }
            $('#selectFilter').html(selectFilter);
        },error: function(xhr) {
    
        },
    })
}
getSemuaSemester()



// function getKelas() {
//     $.ajax({
//         url: ServeUrl + '/admin/master_data/get_kelas',
//         processData: false,
//         contentType: false,
//         cache: false,
//         timeout: 600000,
//         type: 'GET',
//         dataType: 'json',
//         beforeSend: function() {
//             startLoading();
//         },
//         complete: function() {
//             stopLoading();
//         },
//         success: function(response) {
//             var data = response.kelas
//             var selectFilter = '';
//             selectFilter += '<option selected value="semua">Seluruh Kelas</option>'
//             for (let i = 0; i < data.length; i++) {
//                 selectFilter += '<option value="'+data[i].id+'">'+data[i].kelas+'</option>'
//             }
//             $('#kelasFilter').html(selectFilter);
//         },error: function(xhr) {
    
//         },
//     })
// }
// getKelas()

$(".select2").select2();

function handleSelectFilter(attr) {
    getDataPembayaran(attr.value);
}