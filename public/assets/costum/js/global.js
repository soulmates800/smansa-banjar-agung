// var ServeUrl = "http://smansa.spacearts.id/api";
// var BaseUrl = "http://smansa.spacearts.id";

// var ServeUrl = "http://127.0.0.1:8000/api";
// var BaseUrl = "http://127.0.0.1:8000";

var csrf_token = $('meta[name="csrf-token"]').attr('content');

function startLoading() {
    $.LoadingOverlay("show", {
        image : BaseUrl + '/assets/costum/images/loading.gif',
        zIndex : 1000000,
        imageAnimation: "0ms",
        fade: [0, 0],
        size: 200,
        maxSize: 200,
        background: "#d6eaff52"
    });
}

function CapitalEachWord(value) {
    var splitValue = value.toLowerCase().split(' ');
    for (var i = 0; i < splitValue.length; i++) {
        splitValue[i] = splitValue[i].charAt(0).toUpperCase() + splitValue[i].substring(1);
    }
    return splitValue.join(' ');
}


function stopLoading() {
    $.LoadingOverlay("hide");
}

$(document).ajaxStop(function() {
    stopLoading()
});

function toastRoption() {
    toastr.options = {
        "closeButton": true,
        "debug": true,
        "newestOnTop": true,
        "progressBar": true,
        "positionClass": "toast-top-right",
        "preventDuplicates": true,
        "showDuration": 300,
        "hideDuration": 1000,
        "timeOut": 5000,
        "extendedTimeOut": 1000,
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
      }
}
toastRoption();

function clearFormatRupiah(value) {
    return value.replaceAll(/([\/\,\!\\\^\$\{\}\[\]\(\)\Rp.\\ \.\*\+\?\|\<\>\-\&])/g, "")
}

function getIdForRupiah(attr) {
    var currentValue = $('#' + attr.id).val();
    $('#' + attr.id).val(formatRupiah(currentValue, 'Rp. '));
}

function formatRupiah(angka, prefix){
    var number_string = angka.replace(/[^,\d]/g, '').toString(),
    split = number_string.split(','),
    sisa = split[0].length % 3,
    rupiah = split[0].substr(0, sisa),
    ribuan = split[0].substr(sisa).match(/\d{3}/gi);
    if (ribuan) {
        separator = sisa ? '.' : '';
        rupiah += separator + ribuan.join('.');
    }
    rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
    return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
}

var bulan = [
    "Januari",
    "Februari",
    "Maret",
    "April",
    "Mei",
    "Juni",
    "Juli",
    "Agustus",
    "September",
    "Oktober",
    "November",
    "Desember",
];

function getAktivatedSemester() {
    $.ajax({
        url: ServeUrl + '/get_aktivated_semester',
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        type: 'GET',
        dataType: 'json',
        beforeSend: function() {
            startLoading();
        },
        complete: function() {
            stopLoading();
        },
        success: function(response) {
            console.log(`response`, response)
    
        },error: function(xhr) {
    
        },
    })
}
getAktivatedSemester();