
var currentPassword = '';
function getProfile() {
    $.ajax({
        url: ServeUrl + '/wali_murid/profile/get_profile',
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        type: 'GET',
        dataType: 'json',
        beforeSend: function() {
            startLoading();
        },
        complete: function() {
            stopLoading();
        },
        success: function(response) {
            
            var data = response;
            $('#nis').val(data.nis)
            $('#nama').val(data.nama)
            $('#jenis_kelamin').val(data.jenis_kelamin)
            $('#tanggal_lahir').val(data.tanggal_lahir)
            $('#email').val(data.email)
            $('#rombel').val(data.rombel)
            $('#tempat_lahir').val(data.tempat_lahir)

            $('#nama_wali').val(data.nama_wali)
            $('#nik_wali').val(data.nik_wali)
            $('#asal_sd').val(data.asal_sd)
            $('#asal_smp').val(data.asal_smp)
            $('#password').val(data.password)

            var html = ''
            html += '<a href="'+ BaseUrl + '/assets/data/images/' + data.foto_profile +'" data-lightbox="image-1" data-title="'+data.foto_profile+'">';
            html += '<img width="150" height="150" src="'+BaseUrl + '/assets/data/images/' + data.foto_profile +'"/>';
            html += '</a>';

            $('#imageFile').html(html)
            $('#file-name').html(data.foto_profile);

            lightbox.option({
                'resizeDuration': 200,
                'wrapAround': true,
                'alwaysShowNavOnTouchDevices': true,
                'fadeDuration':300,
                'imageFadeDuration': 300
            })

        },error: function(xhr) {
            toastr["error"](xhr.responseJSON.message)
        },
    })
}
getProfile()

function validateFileType(input){
    readURL(input)
    var fileName = $('#customFile').val();
    var idxDot = fileName.lastIndexOf(".") + 1;
    var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();
    if (extFile=="jpg" || extFile=="jpeg" || extFile=="png"){
        $('#file-name').html(fileName);
    }else{
        alert("Hanya gambar berformat .jpg .jpeg .png yang diizinkan");
    }
}

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {

                var html = ''
                html += '<a href="'+ e.target.result +'" data-lightbox="image-1">';
                html += '<img width="150" height="150" src="'+ e.target.result +'"/>';
                html += '</a>';

                $('#imageFile').html(html)
        };
        reader.readAsDataURL(input.files[0]);
    }
}

function updateProfile() {
    var form = $('#profile-detail')[0];
    var request = new FormData(form);
    request.append("_token", csrf_token );

    $.ajax({
        data: request,
        url: ServeUrl + '/wali_murid/profile/update_profile',
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        type: 'POST',
        dataType: 'json',
        beforeSend: function() {
            startLoading();
        },
        complete: function() {
            stopLoading();
        },
        success: function(response) {
            Swal.fire({
                title: '<div style="color: white;">Berhasil!</div>',
                html: '<p class="card-text"><small class="text-muted">Profile berhasil diperbarui.</small></p>',
                type: 'success',
                icon: 'success',
                confirmButtonColor: '#3085d6',
                confirmButtonText: '<i class="fas fa-life-ring fa-sm rotating mr-5"></i>Ya, Baiklah!<i class="fas fa-sm fa-life-ring rotating ml-5"></i>',
                confirmButtonText: 'Pergi ke Status Pembayaran!',
                background: '#14274E',
                confirmButtonClass: 'btn btn-success',
                buttonsStyling: false,
                onClose: function() {
                    getProfile()
                }
            })
        },error: function(xhr) {
            stopLoading();
            toastr["error"](xhr.responseJSON.message)
        },
    })
}

function showPass() {
    var checkTipe = $('#ed-password').get(0).type;
    if (checkTipe == 'password') {
        $('#ed-password').get(0).type = 'text';
        $("#panel").html('<i class="ri-eye-line fa-lg"></i>');
    } else {
        $('#ed-password').get(0).type = 'password';
        $("#panel").html('<i class="ri-eye-off-line fa-lg"></i>');
    }
}

function getKelas() {
    $.ajax({
        url: ServeUrl + '/admin/master_data/get_kelas',
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        type: 'GET',
        dataType: 'json',
        beforeSend: function() {
            startLoading();
        },
        complete: function() {
            stopLoading();
        },
        success: function(response) {
            var data = response.kelas
            var selectFilter = '';
            for (let i = 0; i < data.length; i++) {
                selectFilter += '<option value="'+data[i].id+'">'+data[i].kelas+'</option>'
            }
            $('#rombel').html(selectFilter);
        },error: function(xhr) {
    
        },
    })
}
getKelas()

$(".select2").select2();