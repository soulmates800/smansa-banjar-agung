var semesterAktif;
var bulanLunas = new Array(12);
var selectFilterHelperBug = 0;
function getPembayaran(filter_semester) {
    $.ajax({
        url: ServeUrl + '/wali_murid/pembayaran/get_pembayaran/' + filter_semester,
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        type: 'GET',
        dataType: 'json',
        beforeSend: function() {
            startLoading();
        },
        complete: function() {
            stopLoading();
        },
        success: function(response) {
            var data = response.pembayaran;
            var html = '';
            var tempBulan = '';
            var statusPembayaran = response.status_pembayaran
            semesterAktif = response.semester.semester;
            console.log(`response.semester.tipe`, response.semester.tipe)
            if (response.semester.tipe == 'Ganjil') {
                bulan = [
                    "Januari",
                    "Februari",
                    "Maret",
                    "April",
                    "Mei",
                    "Juni",
                ];
            } else {
                bulan = [
                    "Juli",
                    "Agustus",
                    "September",
                    "Oktober",
                    "November",
                    "Desember",
                ];
            }
            templateBulan()

            var totalPengeluaran = 0;
            for (let i = 0; i < bulan.length; i++) {
                for (let j = 0; j < statusPembayaran.length; j++) {
                    if (statusPembayaran[j].bulan == bulan[i] && statusPembayaran[j].status == 'Lunas') {
                        bulanLunas[i] = bulan[i]
                        tempBulan += '<div class="row">';
                        tempBulan += '<div class="mb-2 col-md-12">';
                        tempBulan += '<span class="roboto-font">'+formatRupiah(String(statusPembayaran[j].jumlah), 'Rp. ')+'</span>';
                        tempBulan += '</div>';
                        tempBulan += '<div class="col-md-12">';
                        tempBulan += '<span class="badge bg-success text-white roboto-font bulan-status-info"><i class="fas fa-check-circle fa-sm mr-2"></i>Lunas</span>';
                        tempBulan += '</div>';
                        tempBulan += '</div>';
                        $('#' + bulan[i]).html(tempBulan)
                        tempBulan = '';
                        totalPengeluaran = totalPengeluaran + statusPembayaran[j].jumlah
                    }
                }
            }

            $('#totalPengeluaran').html('<b>Total Pengeluaran : </b>' + formatRupiah(String(totalPengeluaran), 'Rp. '))

            var numberIncrement = 1;
            for (let i = 0; i < data.length; i++) {
                html += '<tr>';
                html += '<td class="align-middle text-center">'+numberIncrement+'</td>';
                html += '<td>';
                html += '<div class="row">';
                html += '<div class="col-md-3">';

                html += '<a href="'+ BaseUrl + '/assets/data/images/' + data[i].bukti_pembayaran +'" data-lightbox="image-1" data-title="'+data[i].bukti_pembayaran+'">';
                html += '<img class="foto-slip" src="'+BaseUrl + '/assets/data/images/' + data[i].bukti_pembayaran +'"/>';
                html += '</a>';

                // html += '<img class="foto-slip" src="'+BaseUrl + '/assets/data/images/' + data[i].bukti_pembayaran +'" />';
                html += '</div>';
                html += '<div class="col-md-9">';
                html += '<div class="info-pengirim">';
                html += '<span class="nama-pengirim">'+data[i].nama_user+'</span>';
                html += '<span class="nis-pengirim">'+data[i].nis+'</span>';
                html += '</div>';
                html += '<div class="info-tanggal">';
                html += '<span class="nis-pengirim">Tanggal : '+data[i].tanggal_bayar+' | Pembayaran via : '+data[i].nama_bank+' | Jumlah : '+formatRupiah(String(data[i].jumlah_pembayaran), 'Rp. ')+'</span>';
                html += '</div>';
                html += '</div>';
                html += '</div>';
                html += '</td>';
                if (data[i].status == 'Diverifikasi') {
                    html += '<td class="align-middle text-center"><button type="button" class="btn btn-success btn-sm verifikasi-info"><i class="fas fa-check-circle fa-sm mr-2"></i>Diverifikasi</button></td>';
                } else if (data[i].status == 'Ditolak') {
                    html += '<td class="align-middle text-center"><button type="button" class="btn btn-danger btn-sm tolak-info"><i class="fas fa-window-close fa-sm mr-2"></i>Ditolak</button></td>';
                } else {
                    html += '<td class="align-middle text-center"><button type="button" class="btn btn-info btn-sm proses-info"><i class="far fa-clock fa-sm mr-2"></i>Diproses</button></td>';
                }
                html += '<td class="align-middle text-center"><button type="button" onclick="hapusPembayaran('+data[i].id+')" class="btn btn-danger btn-sm"><i class="fa fa-trash fa-sm mr-2"></i>Hapus</button></td>';
                // html += '<td class="align-middle text-center"><button type="button" class="btn btn-danger waves-effect waves-light btn-sm">Danger</button></td>';
                html += '</tr>';
                numberIncrement++;
            }
            $('#generateData').html(html);
            $('#semester-aktif').html(semesterAktif)
            lightbox.option({
                'resizeDuration': 200,
                'wrapAround': true,
                'alwaysShowNavOnTouchDevices': true,
                'fadeDuration':300,
                'imageFadeDuration': 300
            })

        },error: function(xhr) {
            stopLoading();
            toastr["error"](xhr.responseJSON.message)
        },
    })
}
getPembayaran('null')

function handleSelectFilter(attr) {
    getPembayaran(attr.value)
}

function getDetailBulanPembayaran() {
    $.ajax({
        url: ServeUrl + '/wali_murid/pembayaran/bulan_status',
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        type: 'GET',
        dataType: 'json',
        beforeSend: function() {
            startLoading();
        },
        complete: function() {
            stopLoading();
        },
        success: function(response) {
            var data = response;
            var html = '';
            for (let i = 0; i < bulan.length; i++) {
                for (let j = 0; j < data.length; j++) {
                    if (data[j].bulan == bulan[i] && data[j].status == 'Lunas') {
                        $('#' + bulan[i]).html('<span class="badge bg-success text-white roboto-font bulan-status-info"><i class="fas fa-check-circle fa-sm mr-2"></i>Lunas</span>')
                    }
                }
            }
            $('#generateBulanStatus').html(html);
            
        },error: function(xhr) {
            toastr["error"](xhr.responseJSON.message)
        },
    })
}
getDetailBulanPembayaran();

function templateBulan() {
    var html = '';
    for (let i = 0; i < bulan.length; i++) {
        html += '<div class="col-md-2 text-center border p-2">';
        html += '<div class="bulan-name">';
        html += '<span class="roboto-font bold">'+bulan[i]+'</span>';
        html += '</div>';
        html += '<div class="bulan-status mt-2" id="'+bulan[i]+'">';

        html += '<div class="row">';
        html += '<div class="mb-2 col-md-12">';
        html += '<span class="roboto-font">'+formatRupiah('0', 'Rp. ')+'</span>';
        html += '</div>';
        html += '<div class="col-md-12">';
        html += '<span class="badge bg-danger text-white roboto-font bulan-status-info"><i class="far fa-window-close fa-sm mr-2"></i>Belum Lunas</span>'
        html += '</div>';
        html += '</div>';

        
        html += '</div>';
        html += '</div>';
    }
    $('#generateTemplate').html(html);
}
templateBulan()

function getAllSemester() {
    $.ajax({
        url: ServeUrl + '/wali_murid/pembayaran/get_semua_semester',
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        type: 'GET',
        dataType: 'json',
        beforeSend: function() {
            startLoading();
        },
        complete: function() {
            stopLoading();
        },
        success: function(response) {
            var dataSelectFilter = response.semua_semester
            var selectFilter = '';
            for (let i = 0; i < dataSelectFilter.length; i++) {
                if (dataSelectFilter[i].semester == semesterAktif && selectFilterHelperBug == 0) {
                    selectFilterHelperBug = 1;
                    selectFilter += '<option selected value="'+dataSelectFilter[i].id+'">'+dataSelectFilter[i].semester+'</option>'
                } else {
                    selectFilter += '<option value="'+dataSelectFilter[i].id+'">'+dataSelectFilter[i].semester+'</option>'
                }
            }
            $('#selectFilter').html(selectFilter);
        },error: function(xhr) {
    
        },
    })
}
setTimeout(() => {
    getAllSemester()  
}, 500);

function hapusPembayaran(id) {
    Swal.fire({
        title: '<div>Wait !</div>',
        html: '<p class="card-text"><small class="text-muted">Apa anda benar benar sudah yakin ?</small></p>',
        type: 'warning',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya, Lakukan!',
        cancelButtonText: 'Tidak, Batalkan!',
        confirmButtonClass: 'btn btn-primary',
        background: '#20232A',
        cancelButtonClass: 'btn btn-danger ml-1',
    }).then(function (result) {
        if (result.value) {
            $.ajax({
                url: ServeUrl + '/wali_murid/pembayaran/hapus_pembayaran/' + id,
                processData: false,
                contentType: false,
                cache: false,
                timeout: 600000,
                type: 'GET',
                dataType: 'json',
                beforeSend: function() {
                    startLoading();
                },
                complete: function() {
                    stopLoading();
                },
                success: function(response) {
                    Swal.fire({
                        title: '<div style="color: white;">Berhasil!</div>',
                        html: '<p class="card-text"><small class="text-muted">Riwayat pembayaran berhasil dihapus. <br> Status pembayaran masih tetap tersimpan.</small></p>',
                        type: 'success',
                        icon: 'success',
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: '<i class="fas fa-life-ring fa-sm rotating mr-5"></i>Ya, Baiklah!<i class="fas fa-sm fa-life-ring rotating ml-5"></i>',
                        confirmButtonText: 'Ya, Baiklah!',
                        background: '#14274E',
                        confirmButtonClass: 'btn btn-success',
                        buttonsStyling: false,
                        onClose: function() {
                            window.location.reload()
                        }
                    })
                },error: function(xhr) {
            
                },
            })
        }
    })
}

$(".select2").select2();