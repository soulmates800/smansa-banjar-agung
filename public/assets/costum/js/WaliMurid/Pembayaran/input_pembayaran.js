var semesterAktif;
function initLayout() {
    $('#nis').val(dataParamter['user'].nis);
    $('#nama').val(dataParamter['user'].nama);
    
}
initLayout();

setInterval(() => {
    liveTime();
}, 1000);
function liveTime() {
    $('#tanggal_bayar').val(moment().format('DD MMMM YYYY - h:mm:ss a'));
}
liveTime();

function validateFileType(){
    var fileName = $('#customFile').val();
    var idxDot = fileName.lastIndexOf(".") + 1;
    var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();
    if (extFile=="jpg" || extFile=="jpeg" || extFile=="png"){
        $('#fileName').html(fileName);
    }else{
        alert("Hanya gambar berformat .jpg .jpeg .png yang diizinkan");
    }
}

$("#formPembayaran").on("submit", function(){
    event.preventDefault();
    kirimPembayaran();
})

function kirimPembayaran() {
    event.preventDefault();
    var form = $('#formPembayaran')[0];
    var request = new FormData(form);
    request.append("jumlah_pembayaran", clearFormatRupiah($('#jumlah_pembayaran').val()) );
    request.append("tanggal_bayar", moment().format('YYYY-MM-DD') );
    request.append("_token", csrf_token );
    
    $.ajax({
        data: request,
        url: ServeUrl + '/wali_murid/pembayaran/input_pembayaran',
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        type: 'POST',
        dataType: 'json',
        beforeSend: function() {
            startLoading();
        },
        complete: function() {
            stopLoading();
        },
        success: function() {
            Swal.fire({
                title: '<div style="color: white;">Berhasil!</div>',
                html: '<p class="card-text"><small class="text-muted">Pembayaran berhasil dikirm. <br> Admin akan segera malakukan verifikasi untuk ini.</small></p>',
                type: 'success',
                icon: 'success',
                confirmButtonColor: '#3085d6',
                confirmButtonText: '<i class="fas fa-life-ring fa-sm rotating mr-5"></i>Ya, Baiklah!<i class="fas fa-sm fa-life-ring rotating ml-5"></i>',
                confirmButtonText: 'Pergi ke Status Pembayaran!',
                background: '#14274E',
                confirmButtonClass: 'btn btn-success',
                buttonsStyling: false,
                onClose: function() {
                    window.location.href = BaseUrl + '/wali-murid/pembayaran/status-pembayaran';
                }
            })
        },error: function(xhr) {
            stopLoading();
            toastr["error"](xhr.responseJSON.message)
        },
    })
}

var bulanLunas = new Array(12);
function getDetailBulanPembayaran() {
    $.ajax({
        url: ServeUrl + '/wali_murid/pembayaran/bulan_status',
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        type: 'GET',
        dataType: 'json',
        beforeSend: function() {
            startLoading();
        },
        complete: function() {
            stopLoading();
        },
        success: function(response) {
            var data = response.pembayaran;
            var html = '';
            var tempBulan = '';
            semesterAktif = response.semester.semester;
            if (response.semester.tipe == 'Ganjil') {
                bulan = [
                    "Januari",
                    "Februari",
                    "Maret",
                    "April",
                    "Mei",
                    "Juni",
                ];
            } else {
                bulan = [
                    "Juli",
                    "Agustus",
                    "September",
                    "Oktober",
                    "November",
                    "Desember",
                ];
            }
            templateBulan()
            for (let i = 0; i < bulan.length; i++) {
                for (let j = 0; j < data.length; j++) {
                    if (data[j].bulan == bulan[i] && data[j].status == 'Lunas') {
                        bulanLunas[i] = bulan[i]
                        tempBulan += '<div class="row">';
                        tempBulan += '<div class="mb-2 col-md-12">';
                        tempBulan += '<span class="roboto-font">'+formatRupiah(String(data[j].jumlah), 'Rp. ')+'</span>';
                        tempBulan += '</div>';
                        tempBulan += '<div class="col-md-12">';
                        tempBulan += '<span class="badge bg-success text-white roboto-font bulan-status-info"><i class="fas fa-check-circle fa-sm mr-2"></i>Lunas</span>';
                        tempBulan += '</div>';
                        tempBulan += '</div>';
                        $('#' + bulan[i]).html(tempBulan)
                        tempBulan = '';
                    }
                }
            }
            $('#generateBulanStatus').html(html);
            $('#semester-aktif').html(semesterAktif)
            $(".select2").select2();
            setSelectedInput()
        },error: function(xhr) {
            toastr["error"](xhr.responseJSON.message)
        },
    })
}
getDetailBulanPembayaran();

function setSelectedInput() {
    var selectBulan = '';
    selectBulan += '<option selected disabled>Pilih bulan pembayaran</option>';
    
    for (let i = 0; i < bulan.length; i++) {
        if (bulan[i] != bulanLunas[i]) {
            selectBulan += '<option value="'+bulan[i]+'">'+bulan[i]+'</option>';
        } else {
            selectBulan += '<option disabled>'+bulan[i]+'</option>';
        }
    }
    $('#generateSelectBulan').html(selectBulan);
}

function templateBulan() {
    var html = '';
    for (let i = 0; i < bulan.length; i++) {
        html += '<div class="col-md-2 text-center border p-2">';
        html += '<div class="bulan-name">';
        html += '<span class="roboto-font bold">'+bulan[i]+'</span>';
        html += '</div>';
        html += '<div class="bulan-status mt-2" id="'+bulan[i]+'">';

        html += '<div class="row">';
        html += '<div class="mb-2 col-md-12">';
        html += '<span class="roboto-font">'+formatRupiah('0', 'Rp. ')+'</span>';
        html += '</div>';
        html += '<div class="col-md-12">';
        html += '<span class="badge bg-danger text-white roboto-font bulan-status-info"><i class="far fa-window-close fa-sm mr-2"></i>Belum Lunas</span>'
        html += '</div>';
        html += '</div>';

        
        html += '</div>';
        html += '</div>';
    }
    $('#generateTemplate').html(html);
}
