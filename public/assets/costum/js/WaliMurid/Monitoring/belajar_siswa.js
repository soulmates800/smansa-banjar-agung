
var semesterAktif;
var selectFilterHelperBug = 0;

function reinitDatatable() {
    var a = $("#datatable-buttons").DataTable({
        lengthChange: !1,
        language: {
            paginate: {
                previous: "<i class='mdi mdi-chevron-left'>",
                next: "<i class='mdi mdi-chevron-right'>",
            },
        },
        drawCallback: function () {
            $(".dataTables_paginate > .pagination").addClass(
                "pagination-rounded"
            );
        },
        // buttons: ["copy", "excel", "pdf"],
    });
    a
    .buttons()
    .container()
    .appendTo("#datatable-buttons_wrapper .col-md-6:eq(0)"),
    $("#selection-datatable").DataTable({
        select: { style: "multi" },
        language: {
            paginate: {
                previous: "<i class='mdi mdi-chevron-left'>",
                next: "<i class='mdi mdi-chevron-right'>",
            },
        },
        drawCallback: function () {
            $(".dataTables_paginate > .pagination").addClass(
                "pagination-rounded"
            );
        },
    })
}

function getLaporan(filter_select) {
    $.ajax({
        url: ServeUrl + '/wali_murid/monitoring/get_laporan/' + filter_select,
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        type: 'GET',
        dataType: 'json',
        beforeSend: function() {
            startLoading();
        },
        complete: function() {
            stopLoading();
        },
        success: function(response) {
            var data = response.laporan;
            semesterAktif = response.aktif_semester.semester;
            var html = '';
            var number = 1;
            for (let i = 0; i < data.length; i++) {
                html += '<tr>';
                html += '<td class="text-center align-middle">'+number+'</td>';
                html += '<td class="align-middle">'+data[i].semester+'</td>';
                html += '<td class="text-center align-middle">'+formatMomentTime(data[i].updated_at)+'</td>';
                html += '<td class="text-center"><a href="'+ BaseUrl + '/assets/data/images/' + data[i].file +'" target="_blank" class="btn btn-sm btn-info waves-effect waves-light">Download</a></td>';
                html += '</tr>';
                number++;
            }
            $('#generateData').html(html);
            
            var dataUser = response.user
            $('#span_nis').html(dataUser.nis)
            $('#span_nama').html(dataUser.nama)
            $('#span_kelas').html(dataUser.rombel)

            // reinitDatatable()
        },error: function(xhr) {
            stopLoading();
            toastr["error"](xhr.responseJSON.message)
        },
    })
}
getLaporan('null')

function formatMomentTime(value) {
    return moment(value).format('DD MMMM YYYY - h:mm:ss a');
}

function getSemuaSemester() {
    $.ajax({
        url: ServeUrl + '/wali_murid/pembayaran/get_semua_semester',
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        type: 'GET',
        dataType: 'json',
        beforeSend: function() {
            startLoading();
        },
        complete: function() {
            stopLoading();
        },
        success: function(response) {
            var dataSelectFilter = response.semua_semester
            var selectFilter = '';
            for (let i = 0; i < dataSelectFilter.length; i++) {
                if (dataSelectFilter[i].semester == semesterAktif && selectFilterHelperBug == 0) {
                    selectFilterHelperBug = 1;
                    selectFilter += '<option selected value="'+dataSelectFilter[i].id+'">'+dataSelectFilter[i].semester+'</option>'
                } else {
                    selectFilter += '<option value="'+dataSelectFilter[i].id+'">'+dataSelectFilter[i].semester+'</option>'
                }
            }
            $('#selectFilter').html(selectFilter);
        },error: function(xhr) {
    
        },
    })
}
getSemuaSemester()

function handleSelectFilter(attr) {
    getLaporan(attr.value)
}

$(".select2").select2();