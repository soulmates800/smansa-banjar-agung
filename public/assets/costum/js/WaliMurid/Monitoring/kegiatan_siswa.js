
function reinitDatatable() {
    var a = $("#datatable-buttons").DataTable({
        lengthChange: !1,
        language: {
            paginate: {
                previous: "<i class='mdi mdi-chevron-left'>",
                next: "<i class='mdi mdi-chevron-right'>",
            },
        },
        drawCallback: function () {
            $(".dataTables_paginate > .pagination").addClass(
                "pagination-rounded"
            );
        },
        buttons: ["copy", "excel", "pdf"],
    });
    a
    .buttons()
    .container()
    .appendTo("#datatable-buttons_wrapper .col-md-6:eq(0)"),
    $("#selection-datatable").DataTable({
        select: { style: "multi" },
        language: {
            paginate: {
                previous: "<i class='mdi mdi-chevron-left'>",
                next: "<i class='mdi mdi-chevron-right'>",
            },
        },
        drawCallback: function () {
            $(".dataTables_paginate > .pagination").addClass(
                "pagination-rounded"
            );
        },
    })
}

function getKegiatan() {
    $.ajax({
        url: ServeUrl + '/wali_murid/monitoring/get_aktifitas',
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        type: 'GET',
        dataType: 'json',
        beforeSend: function() {
            startLoading();
        },
        complete: function() {
            stopLoading();
        },
        success: function(response) {
            var data = response;
            var html = '';
            var number = 1;
            for (let i = 0; i < data.length; i++) {
                html += '<tr>';
                html += '<td class="text-center">'+number+'</td>';
                html += '<td>'+data[i].kegiatan+'</td>';
                html += '<td class="text-center">'+data[i].prestasi+'</td>';
                html += '<td class="text-center">'+data[i].diraih_pada+'</td>';
                html += '<td class="text-center">'+data[i].status+'</td>';
                // html += '<td class="text-center">'+formatMomentTime(data[i].updated_at)+'</td>';
                html += '</tr>';
                number++;
            }
            $('#generateData').html(html);
            
            reinitDatatable()
        },error: function(xhr) {
            stopLoading();
            toastr["error"](xhr.responseJSON.message)
        },
    })
}

getKegiatan()

function formatMomentTime(value) {
    return moment(value).format('DD MMMM YYYY - h:mm:ss a');
}