
function getPengumuman() {
    $.ajax({
        url: ServeUrl + '/wali_murid/dashboard/get_dashboard',
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        type: 'GET',
        dataType: 'json',
        beforeSend: function() {
            startLoading();
        },
        complete: function() {
            stopLoading();
        },
        success: function(response) {
            var data = response;
            $('#pengumuman').html(data.pengumuman.pengumuman);
            $('#jumlahPeserta').html(data.total_user);
            $('#totalPengeluaran').html(formatRupiah(String(data.total_pembayaran), 'Rp. '));
            $('#semester-dashboard').html(data.aktif_semester.semester)
        },error: function(xhr) {
            stopLoading();
            toastr["error"](xhr.responseJSON.message)
        },
    })
}
getPengumuman();

setInterval(() => {
    liveTime();
}, 1000);
function liveTime() {
    $('#timeLive').html(moment().format('h:mm:ss a'));
}
liveTime();

$('#currentDate').html(moment().format('dddd, DD MMMM YYYY'));