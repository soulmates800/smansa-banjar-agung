var id_pembayaran = window.location.pathname.split('/').pop();
var semesterAktif;
var bulanLunas = new Array(12);
var selectFilterHelperBug = 0;
function getDetailPembayaran() {
    $.ajax({
        url: ServeUrl + '/admin/pembayaran/detail_pembayaran/' + id_pembayaran,
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        type: 'GET',
        dataType: 'json',
        beforeSend: function() {
            startLoading();
        },
        complete: function() {
            stopLoading();
        },
        success: function(response) {
            console.log(`response`, response)
    
            var pembayaran = response.pembayaran

            getDetailBulanPembayaran(pembayaran.user_id)

            $('#nis').val(pembayaran.nis);
            $('#nama').val(pembayaran.nama_user);
            $('#tanggal_bayar').val(pembayaran.tanggal_bayar);
            $('#no_ref').val(pembayaran.no_ref);
            $('#nama_bank').val(pembayaran.nama_bank);
            $('#jumlah_pembayaran').val(pembayaran.jumlah_pembayaran).trigger('input');
            $('#bulan_pembayaran').val(pembayaran.bulan_pembayaran);
            
            $(".select2").select2();

            var fotoSlip = '';
            fotoSlip += '<a href="'+ BaseUrl + '/assets/data/images/' + pembayaran.bukti_pembayaran +'" data-lightbox="image-1" data-title="'+pembayaran.bukti_pembayaran+'">';
            fotoSlip += '<img width="300" height="150" class="imageFcp" src="'+ BaseUrl + '/assets/data/images/' + pembayaran.bukti_pembayaran +'"/>';
            fotoSlip += '</a>';
            $('#bukti_pembayaran').html(fotoSlip)

            var statusPembayaran = '';
            if (pembayaran.status == 'Diproses') {
                statusPembayaran += '<option value="Diproses" selected>Diproses</option>';
                $('.select2-container--default .select2-selection--single').attr('style', 'background-color: #6580f8');
            } else {
                statusPembayaran += '<option value="Diproses">Diproses</option>';
            }

            if (pembayaran.status == 'Diverifikasi') {
                statusPembayaran += '<option value="Diverifikasi" selected>Diverifikasi</option>';
                $('.select2-container--default .select2-selection--single').attr('style', 'background-color: #1CBB8C');
            } else {
                statusPembayaran += '<option value="Diverifikasi">Diverifikasi</option>';
            }

            if (pembayaran.status == 'Ditolak') {
                statusPembayaran += '<option value="Ditolak" selected>Ditolak</option>';
                $('.select2-container--default .select2-selection--single').attr('style', 'background-color: #f86565');
            } else {
                statusPembayaran += '<option value="Ditolak">Ditolak</option>';
            }

            $('#ed-statuspembayaran').html(statusPembayaran)

            lightbox.option({
                'resizeDuration': 200,
                'wrapAround': true,
                'alwaysShowNavOnTouchDevices': true,
                'fadeDuration':300,
                'imageFadeDuration': 300
            })

        },error: function(xhr) {
            stopLoading();
            toastr["error"](xhr.responseJSON.message)
        },
    })
}
getDetailPembayaran()

function handleSelect() {
    var statusText = $('#ed-statuspembayaran').val();

    switch (statusText) {
        case 'Diproses':
            $('.select2-container--default .select2-selection--single').attr('style', 'background-color: #6580f8');
            break;
        case 'Diverifikasi':
            $('.select2-container--default .select2-selection--single').attr('style', 'background-color: #1CBB8C');
            break;
        case 'Ditolak':
            $('.select2-container--default .select2-selection--single').attr('style', 'background-color: #f86565');
            break;
    
        default:
            break;
    }
}

function updatePembayaran() {

    var request = new FormData();
    request.append("status", $('#ed-statuspembayaran').val() );
    request.append("id", id_pembayaran );
    request.append("id", id_pembayaran );
    request.append("_token", csrf_token );
    $.ajax({
        data: request,
        url: ServeUrl + "/admin/pembayaran/update_pembayaran",
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        type: 'POST',
        dataType: 'json',
        beforeSend: function() {
            startLoading();
        },
        complete: function() {
            stopLoading();
        },
        success: function(response) {
            Swal.fire({
                title: '<div style="color: white;">Berhasil!</div>',
                html: '<p class="card-text"><small class="text-muted">Perintah ini berhasil diproses. <br> Status pembayaran telah berhasil diperbarui.</small></p>',
                type: 'success',
                icon: 'success',
                confirmButtonColor: '#3085d6',
                confirmButtonText: '<i class="fas fa-life-ring fa-sm rotating mr-5"></i>Ya, Baiklah!<i class="fas fa-sm fa-life-ring rotating ml-5"></i>',
                background: '#14274E',
                confirmButtonClass: 'btn btn-success',
                buttonsStyling: false,
                onClose: function() {
                    window.location.href = BaseUrl + '/admin/data-pembayaran'
                }
            })
        },error: function(xhr) {
            stopLoading();
            toastr["error"](xhr.responseJSON.message)
        },
    })
}

function getDetailBulanPembayaran(user_id) {
    $.ajax({
        url: ServeUrl + '/admin/pembayaran/bulan_status/' + user_id,
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        type: 'GET',
        dataType: 'json',
        beforeSend: function() {
            startLoading();
        },
        complete: function() {
            stopLoading();
        },
        success: function(response) {
            var data = response.pembayaran;

            var tempBulan = '';
            var statusPembayaran = response.status_pembayaran
            semesterAktif = response.semester.semester;
            $('#semester').html(response.semester.semester)
            if (response.semester.tipe == 'Ganjil') {
                bulan = [
                    "Januari",
                    "Februari",
                    "Maret",
                    "April",
                    "Mei",
                    "Juni",
                ];
            } else {
                bulan = [
                    "Juli",
                    "Agustus",
                    "September",
                    "Oktober",
                    "November",
                    "Desember",
                ];
            }
            templateBulan()

            for (let i = 0; i < bulan.length; i++) {
                for (let j = 0; j < statusPembayaran.length; j++) {
                    if (statusPembayaran[j].bulan == bulan[i] && statusPembayaran[j].status == 'Lunas') {
                        bulanLunas[i] = bulan[i]
                        tempBulan += '<div class="row">';
                        tempBulan += '<div class="mb-2 col-md-12">';
                        tempBulan += '<span class="roboto-font">'+formatRupiah(String(statusPembayaran[j].jumlah), 'Rp. ')+'</span>';
                        tempBulan += '</div>';
                        tempBulan += '<div class="col-md-12">';
                        tempBulan += '<span class="badge bg-success text-white roboto-font bulan-status-info"><i class="fas fa-check-circle fa-sm mr-2"></i>Lunas</span>';
                        tempBulan += '</div>';
                        tempBulan += '</div>';
                        $('#' + bulan[i]).html(tempBulan)
                        tempBulan = '';
                    }
                }
            }

            for (let i = 0; i < bulan.length; i++) {
                for (let j = 0; j < data.length; j++) {
                    if (data[j].bulan == bulan[i] && data[j].status == 'Lunas') {
                        $('#' + bulan[i]).html('<span class="badge bg-success text-white roboto-font bulan-status-info"><i class="fas fa-check-circle fa-sm mr-2"></i>Lunas</span>')
                    }
                }
            }
            
        },error: function(xhr) {
            stopLoading();
            toastr["error"](xhr.responseJSON.message)
        },
    })
}

function templateBulan() {
    var html = '';
    for (let i = 0; i < bulan.length; i++) {
        html += '<div class="col-md-2 text-center border p-2">';
        html += '<div class="bulan-name">';
        html += '<span class="roboto-font bold">'+bulan[i]+'</span>';
        html += '</div>';
        html += '<div class="bulan-status mt-2" id="'+bulan[i]+'">';

        html += '<div class="row">';
        html += '<div class="mb-2 col-md-12">';
        html += '<span class="roboto-font">'+formatRupiah('0', 'Rp. ')+'</span>';
        html += '</div>';
        html += '<div class="col-md-12">';
        html += '<span class="badge bg-danger text-white roboto-font bulan-status-info"><i class="far fa-window-close fa-sm mr-2"></i>Belum Lunas</span>'
        html += '</div>';
        html += '</div>';

        
        html += '</div>';
        html += '</div>';
    }
    $('#generateTemplate').html(html);
}