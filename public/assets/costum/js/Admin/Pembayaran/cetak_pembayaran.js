
function getData() {

    var request = new FormData();

    request.append("kelas", datas.kelas );
    request.append("semester", datas.semester );

    request.append("_token", csrf_token );

    $.ajax({
        data: request,
        url: ServeUrl + '/admin/pembayaran/cetak_pembayaran',
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        type: 'POST',
        dataType: 'json',
        beforeSend: function() {
            startLoading();
        },
        complete: function() {
            stopLoading();
        },
        success: function(response) {
            console.log(`response`, response)
    
            var dataSemester = response.semester;
            var user = response.user;
            var numberIncrement = 1;

            $('#kelas').html('<span>Kelas: '+response.kelas+'</span>')
            $('#semester').html('<span>'+response.semester.semester+'</span>')
            $('#tittle_semester').html('Data Pembayaran ' + response.semester.semester)
            var tableHead = '';
            tableHead += '<tr>';
            tableHead += '<th class="text-center">No</th>';
            tableHead += '<th>Nama Siswa</th>';
            tableHead += '<th class="text-center">Kelas</th>';
            if (dataSemester.tipe == 'Ganjil') {
                tableHead += '<th class="text-center">Januari</th>';
                tableHead += '<th class="text-center">Februari</th>';
                tableHead += '<th class="text-center">Maret</th>';
                tableHead += '<th class="text-center">April</th>';
                tableHead += '<th class="text-center">Mei</th>';
                tableHead += '<th class="text-center">Juni</th>';
            } else {
                tableHead += '<th class="text-center">Juli</th>';
                tableHead += '<th class="text-center">Agustus</th>';
                tableHead += '<th class="text-center">September</th>';
                tableHead += '<th class="text-center">Oktober</th>';
                tableHead += '<th class="text-center">November</th>';
                tableHead += '<th class="text-center">Desember</th>';
            }
            tableHead += '<th class="text-center">Total</th>';
            tableHead += '</tr>';
            $('#generateHead').html(tableHead);

            var total = 0;
            var html = '';
            for (let i = 0; i < user.length; i++) {
                var praTotal = 0;
                html += '<tr>';
                html += '<td class="align-middle text-center">'+numberIncrement+'</td>';
                html += '<td class="align-middle">'+user[i].nama+'</td>';
                html += '<td class="align-middle text-center">'+user[i].rombel+'</td>';

                if (dataSemester.tipe == 'Ganjil') {
                    praTotal = user[i].Januari + user[i].Februari + user[i].Maret + user[i].April + user[i].Mei + user[i].Juni;
                    html += '<td class="align-middle text-center">'+refractionFormat(user[i].Januari)+'</td>';
                    html += '<td class="align-middle text-center">'+refractionFormat(user[i].Februari)+'</td>';
                    html += '<td class="align-middle text-center">'+refractionFormat(user[i].Maret)+'</td>';
                    html += '<td class="align-middle text-center">'+refractionFormat(user[i].April)+'</td>';
                    html += '<td class="align-middle text-center">'+refractionFormat(user[i].Mei)+'</td>';
                    html += '<td class="align-middle text-center">'+refractionFormat(user[i].Juni)+'</td>';

                    total = total + praTotal;
                } else {
                    praTotal = user[i].Juli + user[i].Agustus + user[i].September + user[i].Oktober + user[i].November + user[i].Desember;
                    html += '<td class="align-middle text-center">'+refractionFormat(user[i].Juli)+'</td>';
                    html += '<td class="align-middle text-center">'+refractionFormat(user[i].Agustus)+'</td>';
                    html += '<td class="align-middle text-center">'+refractionFormat(user[i].September)+'</td>';
                    html += '<td class="align-middle text-center">'+refractionFormat(user[i].Oktober)+'</td>';
                    html += '<td class="align-middle text-center">'+refractionFormat(user[i].November)+'</td>';
                    html += '<td class="align-middle text-center">'+refractionFormat(user[i].Desember)+'</td>';

                    total = total + praTotal;
                }
                html += '<td class="align-middle text-center">'+refractionFormat(praTotal)+'</td>';
                html += '</tr>';
                numberIncrement++;
            }
            
            $('#generateData').html(html);
            $('#totalPembayaran').html('<b>Total Keseluruhan : </b>' + refractionFormat(total))
            setTimeout(() => {
                window.print();
            }, 1000);
        },error: function(xhr) {
            stopLoading();
            toastr["error"](xhr.responseJSON.message)
        },
    })
}
getData()

function refractionFormat(value) {
    return new Intl.NumberFormat('id-ID', { style: 'currency', currency: 'IDR', minimumFractionDigits: 0, maximumFractionDigits: 6 }).format(value)
}