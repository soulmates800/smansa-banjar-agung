
function getProfile() {
    $.ajax({
        url: ServeUrl + '/admin/profile/get_profile',
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        type: 'GET',
        dataType: 'json',
        beforeSend: function() {
            startLoading();
        },
        complete: function() {
            stopLoading();
        },
        success: function(response) {
            var data = response;
            $('#nis').val(data.nis)
            $('#nama').val(data.nama)
            $('#jenis_kelamin').val(data.jenis_kelamin)
            $('#tanggal_lahir').val(data.tanggal_lahir)
            $('#email').val(data.email)
            $('#no_telepon').val(data.no_telepon)
            $('#password').val(data.password)
            $('#password').trigger('input')
            var html = ''
            html += '<a href="'+ BaseUrl + '/assets/data/images/' + data.foto_profile +'" data-lightbox="image-1" data-title="'+data.foto_profile+'">';
            html += '<img width="150" height="150" src="'+BaseUrl + '/assets/data/images/' + data.foto_profile +'"/>';
            html += '</a>';

            $('#imageFile').html(html)
            $('#file-name').html(data.foto_profile);

            lightbox.option({
                'resizeDuration': 200,
                'wrapAround': true,
                'alwaysShowNavOnTouchDevices': true,
                'fadeDuration':300,
                'imageFadeDuration': 300
            })

        },error: function(xhr) {
            stopLoading();
            toastr["error"](xhr.responseJSON.message)
        },
    })
}
getProfile()

function validateFileType(input){
    readURL(input)
    var fileName = $('#customFile').val();
    var idxDot = fileName.lastIndexOf(".") + 1;
    var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();
    if (extFile=="jpg" || extFile=="jpeg" || extFile=="png"){
        $('#file-name').html(fileName);
    }else{
        alert("Hanya gambar berformat .jpg .jpeg .png yang diizinkan");
    }
}

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {

                var html = ''
                html += '<a href="'+ e.target.result +'" data-lightbox="image-1">';
                html += '<img width="150" height="150" src="'+ e.target.result +'"/>';
                html += '</a>';

                $('#imageFile').html(html)
        };
        reader.readAsDataURL(input.files[0]);
    }
}

function updateProfile() {
    var form = $('#update-profile')[0];
    var request = new FormData(form);
    request.append("_token", csrf_token );

    $.ajax({
        data: request,
        url: ServeUrl + '/admin/profile/update_profile',
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        type: 'POST',
        dataType: 'json',
        beforeSend: function() {
            startLoading();
        },
        complete: function() {
            stopLoading();
        },
        success: function(response) {
            
            Swal.fire({
                title: '<div style="color: white;">Berhasil!</div>',
                html: '<p class="card-text"><small class="text-muted">Perintah ini berhasil diproses. <br> Lakukan pengecekan ulang pada aksi ini.</small></p>',
                type: 'success',
                icon: 'success',
                confirmButtonColor: '#3085d6',
                confirmButtonText: '<i class="fas fa-life-ring fa-sm rotating mr-5"></i>Ya, Baiklah!<i class="fas fa-sm fa-life-ring rotating ml-5"></i>',
                background: '#14274E',
                confirmButtonClass: 'btn btn-success',
                buttonsStyling: false,
                onClose: function() {
                    window.location.reload();
                }
            })
    
        },error: function(xhr) {
            stopLoading();
            toastr["error"](xhr.responseJSON.message)
        },
    })
}

function showPass() {
    var checkTipe = $('#password').get(0).type;
    if (checkTipe == 'password') {
        $('#password').get(0).type = 'text';
        $("#panel").html('<i class="ri-eye-line fa-lg"></i>');
    } else {
        $('#password').get(0).type = 'password';
        $("#panel").html('<i class="ri-eye-off-line fa-lg"></i>');
    }
}

