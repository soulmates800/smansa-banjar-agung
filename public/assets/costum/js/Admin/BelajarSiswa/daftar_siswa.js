
function getSiswa() {

    $('#datatable-buttons').DataTable().clear();
    $('#datatable-buttons').DataTable().destroy();

    var kelas = $('#generateKelas').val();
    var semester = $('#generateSemester').val();

    if (semester == null || semester == 'all') {
        semester = 'all';
    }

    if (kelas == null || kelas == 'all') {
        kelas = 'all'
    }

    $.ajax({
        url: ServeUrl + '/admin/belajar_siswa/get_siswa?semester=' + semester + '&kelas=' + kelas,
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        type: 'GET',
        dataType: 'json',
        beforeSend: function() {
            startLoading();
        },
        complete: function() {
            stopLoading();
        },
        success: function(response) {
            var data = response;
            var html = '';
            var numberIncrement = 1;
            for (let i = 0; i < data.length; i++) {
                html += '<tr>';
                html += '<td class="text-center align-middle">'+numberIncrement+'</td>'
                html += '<td class="text-center align-middle">'+data[i].nis+'</td>'
                html += '<td class="align-middle">'+data[i].nama+'</td>'
                html += '<td class="text-center align-middle">'+data[i].rombel+'</td>'
                html += '<td class="text-center"><a href="'+BaseUrl + '/admin/belajar-siswa/detail/' + data[i].id +'" class="btn btn-primary btn-sm waves-effect waves-light"><i class="fas fa-edit fa-sm mr-2"></i>Edit</a></td>'
                html += '</tr>';
                numberIncrement++;
            }

            $('#generateData').html(html);
            reinitDatatable()
        },error: function(xhr) {
            stopLoading();
            toastr["error"](xhr.responseJSON.message)
        },
    })
}
getSiswa();

function reinitDatatable() {
    var a = $("#datatable-buttons").DataTable({
        lengthChange: !1,
        language: {
            paginate: {
                previous: "<i class='mdi mdi-chevron-left'>",
                next: "<i class='mdi mdi-chevron-right'>",
            },
        },
        drawCallback: function () {
            $(".dataTables_paginate > .pagination").addClass(
                "pagination-rounded"
            );
        },
        buttons: ["copy", "excel", "pdf"],
    });
    a
    .buttons()
    .container()
    .appendTo("#datatable-buttons_wrapper .col-md-6:eq(0)"),
    $("#selection-datatable").DataTable({
        select: { style: "multi" },
        language: {
            paginate: {
                previous: "<i class='mdi mdi-chevron-left'>",
                next: "<i class='mdi mdi-chevron-right'>",
            },
        },
        drawCallback: function () {
            $(".dataTables_paginate > .pagination").addClass(
                "pagination-rounded"
            );
        },
    })
}

function getKelas() {
    $.ajax({
        url: ServeUrl + '/admin/master_data/get_kelas',
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        type: 'GET',
        dataType: 'json',
        beforeSend: function() {
            startLoading();
        },
        complete: function() {
            stopLoading();
        },
        success: function(response) {
            var data = response.kelas;
            var html = '';
            html += '<option selected value="all">Semua kelas ...</option>';
            for (let i = 0; i < data.length; i++) {
                html += '<option value="'+data[i].kelas+'">'+data[i].kelas+'</option>';
            }
            $('#generateKelas').html(html);

        },error: function(xhr) {
            stopLoading();
            toastr["error"](xhr.responseJSON.message)
        },
    })
}
getKelas()

function getSemester() {
    $.ajax({
        url: ServeUrl + '/admin/master_data/get_semester',
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        type: 'GET',
        dataType: 'json',
        beforeSend: function() {
            startLoading();
        },
        complete: function() {
            stopLoading();
        },
        success: function(response) {
            var data = response.semester;
            var html = '';
            html += '<option selected value="all">Semua semester ...</option>';
            for (let i = 0; i < data.length; i++) {
                html += '<option value="'+data[i].semester+'">'+data[i].semester+'</option>';
            }
            $('#generateSemester').html(html);

        },error: function(xhr) {
            stopLoading();
            toastr["error"](xhr.responseJSON.message)
        },
    })
}
getSemester()

$(".select2").select2();