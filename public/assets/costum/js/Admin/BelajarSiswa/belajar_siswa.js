var user_id = window.location.pathname.split('/').pop();
function reinitDatatable() {
    var a = $("#datatable-buttons").DataTable({
        lengthChange: !1,
        language: {
            paginate: {
                previous: "<i class='mdi mdi-chevron-left'>",
                next: "<i class='mdi mdi-chevron-right'>",
            },
        },
        drawCallback: function () {
            $(".dataTables_paginate > .pagination").addClass(
                "pagination-rounded"
            );
        },
        buttons: ["copy", "excel", "pdf"],
    });
    a
    .buttons()
    .container()
    .appendTo("#datatable-buttons_wrapper .col-md-6:eq(0)"),
    $("#selection-datatable").DataTable({
        select: { style: "multi" },
        language: {
            paginate: {
                previous: "<i class='mdi mdi-chevron-left'>",
                next: "<i class='mdi mdi-chevron-right'>",
            },
        },
        drawCallback: function () {
            $(".dataTables_paginate > .pagination").addClass(
                "pagination-rounded"
            );
        },
    })
}

var dataLaporan = [];
function getLaporan() {

    $('#datatable-buttons').DataTable().clear();
    $('#datatable-buttons').DataTable().destroy();

    $.ajax({
        url: ServeUrl + '/admin/belajar_siswa/detail_belajar/' + user_id,
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        type: 'GET',
        dataType: 'json',
        beforeSend: function() {
            startLoading();
        },
        complete: function() {
            stopLoading();
        },
        success: function(response) {
            var user = response.user;
            $('#dd-nis').html(': ' + user.nis)
            $('#dd-nama').html(': ' + user.nama)
            $('#dd-kelas').html(': ' + user.rombel)
            dataLaporan = response.laporan;
            var html = '';
            var number = 1;
            for (let i = 0; i < dataLaporan.length; i++) {
                html += '<tr>';
                html += '<td class="text-center align-middle">'+number+'</td>';
                html += '<td class="align-middle">'+dataLaporan[i].semester+'</td>';
                html += '<td class="text-center align-middle">'+dataLaporan[i].file+'</td>';
                html += '<td class="text-center">';
                html += '<button type="button" class="btn btn-sm btn-success waves-effect waves-light mr-2" onclick="modalUpdate('+i+')"><i class="fas fa-save fa-sm mr-2"></i>Update</button>';
                html += '<button type="button" class="btn btn-sm btn-danger waves-effect waves-light mr-2" onclick="hapusLaporan('+dataLaporan[i].id+')"><i class="fas fa-trash fa-sm mr-2"></i>Hapus</button>';
                html += '</td>';
                html += '</tr>';
                number++;
            }
            $('#generateData').html(html);
            
            reinitDatatable()
        },error: function(xhr) {
            stopLoading();
            toastr["error"](xhr.responseJSON.message)
        },
    })
}
getLaporan()

function modalUpdate(index) {
    $('.update-semester').modal('show');
    $('#id-update').val(dataLaporan[index].id);
    $('#nama-semester').val(dataLaporan[index].semester);
    $('#update-label-file-laporan').html(dataLaporan[index].file);
}

function tambahSemester() {

    var form = $('#tambah-semester')[0];
    var request = new FormData(form);
    request.append("user_id", user_id );
    request.append("_token", csrf_token );
    
    $.ajax({
        data: request,
        url: ServeUrl + '/admin/belajar_siswa/tambah_semester',
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        type: 'POST',
        dataType: 'json',
        beforeSend: function() {
            startLoading();
        },
        complete: function() {
            stopLoading();
        },
        success: function() {

            Swal.fire({
                title: '<div style="color: white;">Berhasil!</div>',
                html: '<p class="card-text"><small class="text-muted">Perintah ini berhasil diproses. <br> Lakukan pengecekan ulang pada aksi ini.</small></p>',
                type: 'success',
                icon: 'success',
                confirmButtonColor: '#3085d6',
                confirmButtonText: '<i class="fas fa-life-ring fa-sm rotating mr-5"></i>Ya, Baiklah!<i class="fas fa-sm fa-life-ring rotating ml-5"></i>',
                confirmButtonText: 'Pergi ke Status Pembayaran!',
                background: '#14274E',
                confirmButtonClass: 'btn btn-success',
                buttonsStyling: false,
                onClose: function() {
                    getLaporan();
                    $('#tambah-semester').trigger("reset");
                    $('.tambah-semester').modal('hide');
                }
            })
    
        },error: function(xhr) {
            stopLoading();
            toastr["error"](xhr.responseJSON.message)
        },
    })
}

function updateSemester() {

    var form = $('#update-semester')[0];
    var request = new FormData(form);
    request.append("_token", csrf_token );
    
    $.ajax({
        data: request,
        url: ServeUrl + '/admin/belajar_siswa/update_semester',
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        type: 'POST',
        dataType: 'json',
        beforeSend: function() {
            startLoading();
        },
        complete: function() {
            stopLoading();
        },
        success: function(response) {

            Swal.fire({
                title: '<div style="color: white;">Berhasil!</div>',
                html: '<p class="card-text"><small class="text-muted">Perintah ini berhasil diproses. <br> Lakukan pengecekan ulang pada aksi ini.</small></p>',
                type: 'success',
                icon: 'success',
                confirmButtonColor: '#3085d6',
                confirmButtonText: '<i class="fas fa-life-ring fa-sm rotating mr-5"></i>Ya, Baiklah!<i class="fas fa-sm fa-life-ring rotating ml-5"></i>',
                confirmButtonText: 'Pergi ke Status Pembayaran!',
                background: '#14274E',
                confirmButtonClass: 'btn btn-success',
                buttonsStyling: false,
                onClose: function() {
                    getLaporan();
                    $('#update-semester').trigger("reset");
                    $('.update-semester').modal('hide');
                }
            })
    
        },error: function(xhr) {
            stopLoading();
            toastr["error"](xhr.responseJSON.message)
        },
    })
}

function hapusLaporan(id) {
    Swal.fire({
        title: '<div style="color: white;">Wait !</div>',
        html: '<p class="card-text"><small class="text-muted">Apa kamu benar benar sudah yakin ?<br> Perintah ini akan menghapus untuk <b>selamanya</b></small></p>',
        type: 'warning',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya, Lakukan!',
        cancelButtonText: 'Tidak, Batalkan!',
        confirmButtonClass: 'btn btn-primary',
        background: '#20232A',
        cancelButtonClass: 'btn btn-danger ml-1',
        buttonsStyling: false,
    }).then(function (result) {
        if (result.value) {
            $.ajax({
                url: ServeUrl + '/admin/belajar_siswa/hapus_semester/' + id,
                processData: false,
                contentType: false,
                cache: false,
                timeout: 600000,
                type: 'GET',
                dataType: 'json',
                beforeSend: function() {
                    startLoading();
                },
                complete: function() {
                    stopLoading();
                },
                success: function(response) {
                    Swal.fire({
                        title: '<div style="color: white;">Berhasil!</div>',
                        html: '<p class="card-text"><small class="text-muted">Perintah ini berhasil diproses. <br> Lakukan pengecekan ulang pada aksi ini.</small></p>',
                        type: 'success',
                        icon: 'success',
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: '<i class="fas fa-life-ring fa-sm rotating mr-5"></i>Ya, Baiklah!<i class="fas fa-sm fa-life-ring rotating ml-5"></i>',
                        confirmButtonText: 'Pergi ke Status Pembayaran!',
                        background: '#14274E',
                        confirmButtonClass: 'btn btn-success',
                        buttonsStyling: false,
                        onClose: function() {
                            getLaporan();
                        }
                    })
                },error: function(xhr) {
                    stopLoading();
                    toastr["error"](xhr.responseJSON.message)
                },
            })
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                Swal.fire({
                title: 'Dibatalkan !',
                icon: 'error',
                html: '<p class="card-text"><small class="text-muted">Data ini tetap akan di simpan</small></p>',
                type: 'error',
                confirmButtonText: 'Ya, Baiklah !',
                confirmButtonClass: 'btn btn-success',
                })
            }
        })
}

bsCustomFileInput.init();

function getSemester() {
    $.ajax({
        url: ServeUrl + '/admin/master_data/get_semester',
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        type: 'GET',
        dataType: 'json',
        beforeSend: function() {
            startLoading();
        },
        complete: function() {
            stopLoading();
        },
        success: function(response) {
            var data = response.semester;
            var html = '';
            html += '<option selected disabled>Pilih semester ...</option>'
    
            for (let i = 0; i < data.length; i++) {
                html += '<option>'+data[i].semester+'</option>'
            }
            $('.nama-semester').html(html);
            
        },error: function(xhr) {
    
        },
    })
}
getSemester();
$(".select2").select2();