
function getPengumuman() {
    $.ajax({
        url: ServeUrl + '/admin/dashboard/get_pengumuman',
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        type: 'GET',
        dataType: 'json',
        beforeSend: function() {
            startLoading();
        },
        complete: function() {
            stopLoading();
        },
        success: function(response) {
            var data = response;
            $('#semester-dashboard').html(data.aktif_semester.semester)
            $('#pengumuman').html(data.pengumuman.pengumuman);
            $('#jumlahPeserta').html(data.total_user);
            var d = new Date();  
            $('#totalPengeluaran').html(formatRupiah(String(data.total_pembayaran), 'Rp. ') + ' <br>(Tahun '+d.getFullYear()+')');
    
        },error: function(xhr) {
            stopLoading();
            toastr["error"](xhr.responseJSON.message)
        },
    })
}
getPengumuman()

function kirimPengumuman() {

    var request = new FormData();
    request.append("pengumuman", $('#textpengumuman').val() );
    request.append("_token", csrf_token );
    
    $.ajax({
        data: request,
        url: ServeUrl + '/admin/dashboard/kirim_pengumuman',
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        type: 'POST',
        dataType: 'json',
        beforeSend: function() {
            startLoading();
        },
        complete: function() {
            stopLoading();
        },
        success: function(response) {
            Swal.fire({
                title: '<div style="color: white;">Berhasil!</div>',
                html: '<p class="card-text"><small class="text-muted">Berhasil memperbarui pengumuman.</small></p>',
                type: 'success',
                icon: 'success',
                confirmButtonColor: '#3085d6',
                confirmButtonText: 'Ya Baiklah!',
                background: '#14274E',
                buttonsStyling: true,
                onClose: function() {
                    getPengumuman()
                    $('#textpengumuman').val('')
                }
            })
        },error: function(xhr) {
            stopLoading();
            toastr["error"](xhr.responseJSON.message)
        },
    })
}

setInterval(() => {
    liveTime();
}, 1000);
function liveTime() {
    $('#timeLive').html(moment().format('h:mm:ss a'));
}
liveTime();

$('#currentDate').html(moment().format('dddd, DD MMMM YYYY'));