var dataKegiatan;
function getKegiatan() {

    $('#datatable-buttons').DataTable().clear();
    $('#datatable-buttons').DataTable().destroy();

    $.ajax({
        url: ServeUrl + '/admin/master_data/get_kegiatan',
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        type: 'GET',
        dataType: 'json',
        beforeSend: function() {
            startLoading();
        },
        complete: function() {
            stopLoading();
        },
        success: function(response) {
            
            var data = response;
            dataKegiatan = data;
            var html = '';
            var numberIncrement = 1;
            for (let i = 0; i < data.length; i++) {
                html += '<tr>';
                html += '<td class="align-mmiddle text-center">'+numberIncrement+'</td>';
                html += '<td class="align-mmiddle">'+data[i].kegiatan+'</td>';
                html += '<td class="align-mmiddle">'+formatMomentTime(data[i].created_at)+'</td>';
                html += '<td class="align-mmiddle text-center"><button type="button" class="btn btn-danger btn-sm waves-effect waves-light mr-2" onclick="hapusKegiatan('+data[i].id+')"><i class="fas fa-plus fa-sm mr-2"></i>Hapus</button></td>';
                numberIncrement++;
            }
            $('#generateData').html(html);
            reinitDatatable()
        },error: function(xhr) {
            stopLoading();
            toastr["error"](xhr.responseJSON.message)
        },
    })
}
getKegiatan()

function formatMomentTime(value) {
    return moment(value).format('DD MMMM YYYY - h:mm:ss a');
}

function hapusKegiatan(id) {
    Swal.fire({
        title: '<div style="color: white;">Wait !</div>',
        html: '<p class="card-text"><small class="text-muted">Apa kamu benar benar sudah yakin ?<br> Perintah ini akan menghapus untuk <b>selamanya</b></small></p>',
        type: 'warning',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya, Lakukan!',
        cancelButtonText: 'Tidak, Batalkan!',
        confirmButtonClass: 'btn btn-primary',
        background: '#20232A',
        cancelButtonClass: 'btn btn-danger ml-1',
        buttonsStyling: false,
    }).then(function (result) {
        if (result.value) {
            $.ajax({
                url: ServeUrl + '/admin/master_data/hapus_kegiatan/' + id ,
                processData: false,
                contentType: false,
                cache: false,
                timeout: 600000,
                type: 'GET',
                dataType: 'json',
                beforeSend: function() {
                    startLoading();
                },
                complete: function() {
                    stopLoading();
                },
                success: function(response) {
                    Swal.fire({
                        title: '<div style="color: white;">Berhasil!</div>',
                        html: '<p class="card-text"><small class="text-muted">Kegiatan berhasil dihapus.</small></p>',
                        type: 'success',
                        icon: 'success',
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: '<i class="fas fa-life-ring fa-sm rotating mr-5"></i>Ya, Baiklah!<i class="fas fa-sm fa-life-ring rotating ml-5"></i>',
                        confirmButtonText: 'Ya, Baiklah!',
                        background: '#14274E',
                        confirmButtonClass: 'btn btn-success',
                        buttonsStyling: false,
                        onClose: function() {
                            getKegiatan()
                        }
                    })
                },error: function(xhr) {
                    stopLoading();
                    toastr["error"](xhr.responseJSON.message)
                },
            })
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                Swal.fire({
                title: 'Dibatalkan !',
                icon: 'error',
                html: '<p class="card-text"><small class="text-muted">Data ini tetap akan di simpan</small></p>',
                type: 'error',
                confirmButtonText: 'Ya, Baiklah !',
                confirmButtonClass: 'btn btn-success',
                })
            }
        })
}

function reinitDatatable() {
    var a = $("#datatable-buttons").DataTable({
        lengthChange: !1,
        language: {
            paginate: {
                previous: "<i class='mdi mdi-chevron-left'>",
                next: "<i class='mdi mdi-chevron-right'>",
            },
        },
        drawCallback: function () {
            $(".dataTables_paginate > .pagination").addClass(
                "pagination-rounded"
            );
        },
        buttons: ["copy", "excel", "pdf"],
    });
    a
    .buttons()
    .container()
    .appendTo("#datatable-buttons_wrapper .col-md-6:eq(0)"),
    $("#selection-datatable").DataTable({
        select: { style: "multi" },
        language: {
            paginate: {
                previous: "<i class='mdi mdi-chevron-left'>",
                next: "<i class='mdi mdi-chevron-right'>",
            },
        },
        drawCallback: function () {
            $(".dataTables_paginate > .pagination").addClass(
                "pagination-rounded"
            );
        },
    })
}

function tambahKegiatan() {
    var form = $('#tambah-kegiatan')[0];
    var request = new FormData(form);
    request.append("_token", csrf_token );
    
    $.ajax({
        data: request,
        url: ServeUrl + '/admin/master_data/tambah_kegiatan',
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        type: 'POST',
        dataType: 'json',
        beforeSend: function() {
            startLoading();
        },
        complete: function() {
            stopLoading();
        },
        success: function(response) {
            Swal.fire({
                title: '<div style="color: white;">Berhasil!</div>',
                html: '<p class="card-text"><small class="text-muted">Siswa berhasil dihapus.</small></p>',
                type: 'success',
                icon: 'success',
                confirmButtonColor: '#3085d6',
                confirmButtonText: '<i class="fas fa-life-ring fa-sm rotating mr-5"></i>Ya, Baiklah!<i class="fas fa-sm fa-life-ring rotating ml-5"></i>',
                confirmButtonText: 'Ya, Baiklah!',
                background: '#14274E',
                confirmButtonClass: 'btn btn-success',
                buttonsStyling: false,
                onClose: function() {
                    getKegiatan()
                    $('#tambah-kegiatan').trigger("reset");
                    $('.tambah-kegiatan').modal('hide');
                }
            })
        },error: function(xhr) {
            stopLoading();
            toastr["error"](xhr.responseJSON.message)
        },
    })
}

var toogle = false;
function checkDataSemester() {
    var kegiatan = $('#formkegiatan').val()
    for (let i = 0; i < dataKegiatan.length; i++) {
        if (dataKegiatan[i].kegiatan.trim() != kegiatan.trim()) {
            toogle = false;
        } else {
            $('#dataAda').show();
            toogle = true
            return;
        }
    }

    if (toogle == false) {
        $('#dataAda').hide();
        tambahKegiatan();
    }
}
$('#dataAda').hide();
