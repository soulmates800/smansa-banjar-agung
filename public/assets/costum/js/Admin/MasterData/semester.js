
var dataSemester = [];
function getSemester() {

    $('#datatable-buttons').DataTable().clear();
    $('#datatable-buttons').DataTable().destroy();

    $.ajax({
        url: ServeUrl + '/admin/master_data/get_semester',
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        type: 'GET',
        dataType: 'json',
        beforeSend: function() {
            startLoading();
        },
        complete: function() {
            stopLoading();
        },
        success: function(response) {
            
            var data = response.semester;
            dataSemester = data;
            var akrifSemester = response.aktif_semester

            $('#aktivated_semester').html('Semester Yang Saat Ini Diaktifkan :' + akrifSemester.semester)

            var html = '';
            var numberIncrement = 1;
            for (let i = 0; i < data.length; i++) {
                html += '<tr>';
                html += '<td class="align-mmiddle text-center">'+numberIncrement+'</td>';
                html += '<td class="align-mmiddle">'+data[i].semester+'</td>';
                html += '<td class="align-mmiddle">'+formatMomentTime(data[i].created_at)+'</td>';
                html += '<td class="align-mmiddle text-center">';
                if (akrifSemester.semester != data[i].semester) {
                    html += '<button type="button" class="btn btn-info btn-sm waves-effect waves-light mr-2" onclick="aktifkanSemester('+data[i].id+')"><i class="fas fa-plus fa-sm mr-2"></i>Aktifkan</button>';
                } else {
                    html += '<button disabled type="button" class="btn disabled btn-light btn-sm waves-effect  waves-light mr-2" onclick="aktifkanSemester('+data[i].id+')"><i class="fas fa-plus fa-sm mr-2"></i>Aktifkan</button>';
                }
                
                html += '<button type="button" class="btn btn-primary btn-sm waves-effect waves-light mr-2" onclick="openModalUpdate('+i+')"><i class="fas fa-edit fa-sm mr-2"></i>Edit</button>';
                html += '<button type="button" class="btn btn-danger btn-sm waves-effect waves-light mr-2" onclick="hapusSemester('+data[i].id+')"><i class="fa fa-trash fa-sm mr-2"></i>Hapus</button>';
                html += '</td>'
                numberIncrement++;
            }
            $('#generateData').html(html);
            reinitDatatable()
        },error: function(xhr) {
            stopLoading();
            toastr["error"](xhr.responseJSON.message)
        },
    })
}
getSemester()

function formatMomentTime(value) {
    return moment(value).format('DD MMMM YYYY - h:mm:ss a');
}

function hapusSemester(id) {
    Swal.fire({
        title: '<div style="color: white;">Wait !</div>',
        html: '<p class="card-text"><small class="text-muted">Apa kamu benar benar sudah yakin ?<br> Perintah ini akan menghapus untuk <b>selamanya</b></small></p>',
        type: 'warning',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya, Lakukan!',
        cancelButtonText: 'Tidak, Batalkan!',
        confirmButtonClass: 'btn btn-primary',
        background: '#20232A',
        cancelButtonClass: 'btn btn-danger ml-1',
        buttonsStyling: false,
    }).then(function (result) {
        if (result.value) {
            $.ajax({
                url: ServeUrl + '/admin/master_data/hapus_semester/' + id ,
                processData: false,
                contentType: false,
                cache: false,
                timeout: 600000,
                type: 'GET',
                dataType: 'json',
                beforeSend: function() {
                    startLoading();
                },
                complete: function() {
                    stopLoading();
                },
                success: function(response) {
                    Swal.fire({
                        title: '<div style="color: white;">Berhasil!</div>',
                        html: '<p class="card-text"><small class="text-muted">Semester berhasil dihapus.</small></p>',
                        type: 'success',
                        icon: 'success',
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: '<i class="fas fa-life-ring fa-sm rotating mr-5"></i>Ya, Baiklah!<i class="fas fa-sm fa-life-ring rotating ml-5"></i>',
                        confirmButtonText: 'Ya, Baiklah!',
                        background: '#14274E',
                        confirmButtonClass: 'btn btn-success',
                        buttonsStyling: false,
                        onClose: function() {
                            getSemester()
                        }
                    })
                },error: function(xhr) {
                    stopLoading();
                    toastr["error"](xhr.responseJSON.message)
                },
            })
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                Swal.fire({
                title: 'Dibatalkan !',
                icon: 'error',
                html: '<p class="card-text"><small class="text-muted">Data ini tetap akan di simpan</small></p>',
                type: 'error',
                confirmButtonText: 'Ya, Baiklah !',
                confirmButtonClass: 'btn btn-success',
                })
            }
        })
}

function reinitDatatable() {
    var a = $("#datatable-buttons").DataTable({
        lengthChange: !1,
        language: {
            paginate: {
                previous: "<i class='mdi mdi-chevron-left'>",
                next: "<i class='mdi mdi-chevron-right'>",
            },
        },
        drawCallback: function () {
            $(".dataTables_paginate > .pagination").addClass(
                "pagination-rounded"
            );
        },
        // buttons: ["copy", "excel", "pdf"],
    });
    a
    .buttons()
    .container()
    .appendTo("#datatable-buttons_wrapper .col-md-6:eq(0)"),
    $("#selection-datatable").DataTable({
        select: { style: "multi" },
        language: {
            paginate: {
                previous: "<i class='mdi mdi-chevron-left'>",
                next: "<i class='mdi mdi-chevron-right'>",
            },
        },
        drawCallback: function () {
            $(".dataTables_paginate > .pagination").addClass(
                "pagination-rounded"
            );
        },
    })
}

function tambahSemester() {
    var request = new FormData();

    var semester = 'Semester '+$('#tipe_ed').val()+' TA. '+$('#awal_ed').val()+'/'+$('#akhir_ed').val()
    request.append("semester", semester );
    request.append("tipe", $('#tipe_ed').val() );
    request.append("_token", csrf_token );
    
    $.ajax({
        data: request,
        url: ServeUrl + '/admin/master_data/tambah_semester',
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        type: 'POST',
        dataType: 'json',
        beforeSend: function() {
            startLoading();
        },
        complete: function() {
            stopLoading();
        },
        success: function(response) {
            Swal.fire({
                title: '<div style="color: white;">Berhasil!</div>',
                html: '<p class="card-text"><small class="text-muted">Semester berhasil ditambah.</small></p>',
                type: 'success',
                icon: 'success',
                confirmButtonColor: '#3085d6',
                confirmButtonText: '<i class="fas fa-life-ring fa-sm rotating mr-5"></i>Ya, Baiklah!<i class="fas fa-sm fa-life-ring rotating ml-5"></i>',
                confirmButtonText: 'Ya, Baiklah!',
                background: '#14274E',
                confirmButtonClass: 'btn btn-success',
                buttonsStyling: false,
                onClose: function() {
                    getSemester()
                    $('#tambah-semester').trigger("reset");
                    $('.tambah-semester').modal('hide');
                }
            })
        },error: function(xhr) {
            stopLoading();
            toastr["error"](xhr.responseJSON.message)
        },
    })
}

function aktifkanSemester(id) {
    Swal.fire({
        title: '<div>Wait !</div>',
        html: '<p class="card-text"><small class="text-muted">Apa anda benar benar sudah yakin ?</small></p>',
        type: 'warning',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya, Lakukan!',
        cancelButtonText: 'Tidak, Batalkan!',
        confirmButtonClass: 'btn btn-primary',
        background: '#20232A',
        cancelButtonClass: 'btn btn-danger ml-1',
    }).then(function (result) {
        if (result.value) {
            $.ajax({
                url: ServeUrl + '/admin/master_data/aktifkan_semester/' + id,
                processData: false,
                contentType: false,
                cache: false,
                timeout: 600000,
                type: 'GET',
                dataType: 'json',
                beforeSend: function() {
                    startLoading();
                },
                complete: function() {
                    stopLoading();
                },
                success: function(response) {
                    Swal.fire({
                        title: '<div style="color: white;">Berhasil!</div>',
                        html: '<p class="card-text"><small class="text-muted">Semester berhasil diaktifkan.</small></p>',
                        type: 'success',
                        icon: 'success',
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: '<i class="fas fa-life-ring fa-sm rotating mr-5"></i>Ya, Baiklah!<i class="fas fa-sm fa-life-ring rotating ml-5"></i>',
                        confirmButtonText: 'Ya, Baiklah!',
                        background: '#14274E',
                        confirmButtonClass: 'btn btn-success',
                        buttonsStyling: false,
                        onClose: function() {
                            getSemester()
                        }
                    })
                },error: function(xhr) {
            
                },
            })
        }
    })
}

function txt_tipe(attr) {
    $('#txt-tipe').html(attr.value);
}

function txt_awal(attr) {
    $('#txt-awal').html(attr.value);
}

function txt_akhir(attr) {
    $('#txt-akhir').html(attr.value);
}

function openModalUpdate(index) {
    $('.update-semester').modal('show')
    
    var selectHtmlUpdate = '';
    if (dataSemester[index].tipe == 'Ganjil') {
        selectHtmlUpdate += '<option selected>Ganjil</option>';
        selectHtmlUpdate += '<option>Genap</option>';
        $('#txt-tipe_updt').html('Ganjil');
    } else {
        selectHtmlUpdate += '<option>Ganjil</option>';
        selectHtmlUpdate += '<option selected>Genap</option>';
        $('#txt-tipe_updt').html('Genap');
    }

    // var tahunAwal = dataSemester[index].semester.substring(-6);
    var getTahun = dataSemester[index].semester.substring(dataSemester[index].semester.length - 9, dataSemester[index].semester.length);
    var tahunAwal = getTahun.substring(0, 4);
    var tahunAkhir = getTahun.substring(5, 9);
    
    $('#update_tipe_ed').html(selectHtmlUpdate)

    $('#update_awal_ed').val(tahunAwal)
    $('#update_akhir_ed').val(tahunAkhir)

    
    $('#txt-awal_updt').html(tahunAwal);
    $('#txt-akhir_updt').html(tahunAkhir);
}

function txt_tipe_updt(attr) {
    $('#txt-tipe_updt').html(attr.value);
}

function txt_awal_updt(attr) {
    $('#txt-awal_updt').html(attr.value);
}

function txt_akhir_updt(attr) {
    $('#txt-akhir_updt').html(attr.value);
}

var toogle = false;
function checkDataSemester() {
    var semester = 'Semester '+$('#tipe_ed').val()+' TA. '+$('#awal_ed').val()+'/'+$('#akhir_ed').val()
    for (let i = 0; i < dataSemester.length; i++) {
        if (dataSemester[i].semester != semester) {
            toogle = false;
        } else {
            console.log('data ada');
            $('#dataAda').show();
            toogle = true
            return;
        }
    }

    if (toogle == false) {
        $('#dataAda').hide();
        tambahSemester();
    }
}
$('#dataAda').hide();
