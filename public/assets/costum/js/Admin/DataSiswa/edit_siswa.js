var user_id = window.location.pathname.split('/').pop();

$(".select2").select2();

var arrKelas = [];
function getKelas() {
    $.ajax({
        url: ServeUrl + '/admin/master_data/get_kelas',
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        type: 'GET',
        dataType: 'json',
        beforeSend: function() {
            startLoading();
        },
        complete: function() {
            stopLoading();
        },
        success: function(response) {
            
            arrKelas = response.kelas;
    
        },error: function(xhr) {
    
        },
    })
}
getKelas()

var arrKegiatan = [];
function getKegiatan() {
    $.ajax({
        url: ServeUrl + '/admin/master_data/get_kegiatan',
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        type: 'GET',
        dataType: 'json',
        beforeSend: function() {
            startLoading();
        },
        complete: function() {
            stopLoading();
        },
        success: function(response) {
            arrKegiatan = response;
        },error: function(xhr) {
            stopLoading();
            toastr["error"](xhr.responseJSON.message)
        },
    })
}
getKegiatan()

var kegiatanIncrement = 0;
function generateKegiatan() {
    
    var html = '';

    html += '<div class="col-md-12 kegiatan'+kegiatanIncrement+'"">';
    html += '<div class="alert alert-info" role="alert">';
    html += '<div class="form-group row">';
    html += '<label for="example-text-input" class="col-md-2 col-form-label">Kegiatan</label>';
    html += '<div class="col-md-10">';
    html += '<select class="form-control select2 kegiatan'+kegiatanIncrement+'"" name="kegiatan[]">';
    html += '<option>Pilih kegiatan ...</option>';

    for (let i = 0; i < arrKegiatan.length; i++) {
        html += '<option value="'+arrKegiatan[i].kegiatan+'">'+arrKegiatan[i].kegiatan+'</option>';
        
    }

    html += '</select>';
    html += '</div>';
    html += '</div>';
    html += '<div class="form-group row">';
    html += '<label for="example-text-input" class="col-md-2 col-form-label">Prestasi</label>';
    html += '<div class="col-md-10">';
    html += '<textarea required="" class="form-control kegiatan'+kegiatanIncrement+'"" name="prestasi[]" rows="5"></textarea>';
    html += '</div>';
    html += '</div>';

    html += '<div class="form-group row">';
    html += '<label for="example-text-input" class="col-md-2 col-form-label">Diraih Pada</label>';
    html += '<div class="col-md-10">';
    html += '<input class="form-control required" type="date" name="diraih_pada[]" required>';
    html += '</div>';
    html += '</div>';

    html += '<div class="form-group row">';
    html += '<label for="example-text-input" class="col-md-2 col-form-label">Status</label>';
    html += '<div class="col-md-10">';
    html += '<select class="form-control select2 kegiatan'+kegiatanIncrement+'"" name="status[]">';
    html += '<option>Pilih status ...</option>';
    html += '<option value="Aktif">Aktif</option>';
    html += '<option value="Tidak Aktif">Tidak Aktif</option>';
    html += '</select>';
    html += '</div>';
    html += '</div>';

    html += '<div class="form-group row">';
    html += '<div class="col-md-12">';
    html += '<button type="button" class="btn btn-danger waves-effect waves-light mr-2" onclick="deleteKegiatan('+kegiatanIncrement+')"><i class="fas fa-trash"></i></button>';
    html += '</div>';
    html += '</div>';

    html += '</div>';
    html += '</div>';
    kegiatanIncrement++;
    $('#generateKegiatan').append(html);
    $(".select2").select2();
}

function deleteKegiatan(index) {
    $('.kegiatan' + index).hide();
    $('.kegiatan' + index).attr("disabled", "disabled");
}

function validateFileType(input){
    readURL(input)
    var fileName = $('#customFile').val();
    var idxDot = fileName.lastIndexOf(".") + 1;
    var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();
    if (extFile=="jpg" || extFile=="jpeg" || extFile=="png"){
        $('#file-name').html(fileName);
    }else{
        alert("Hanya gambar berformat .jpg .jpeg .png yang diizinkan");
    }
}

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#imageFile')
                .attr('src', e.target.result)
                .width(150)
                .height(150);
        };
        reader.readAsDataURL(input.files[0]);
    }
}

var hashsingPassword = '';
function getDetailSiswa() {
    $.ajax({
        url: ServeUrl + '/admin/data_siswa/detail_siswa/' + user_id,
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        type: 'GET',
        dataType: 'json',
        beforeSend: function() {
            startLoading();
        },
        complete: function() {
            stopLoading();
        },
        success: function(response) {
            
            var data = response.user;
            var kegiatan = response.kegiatan;
            $('#ed-id').val(data.id);
            $('#ed-nis').val(data.nis);
            $('#ed-nama').val(data.nama);
            $('#ed-jeniskelamin').val(data.jenis_kelamin);
            $('#ed-email').val(data.email);
            // $('#ed-password').val(data.password);
            hashsingPassword = data.password;
            
            var htmlKelas = '';
            htmlKelas += '<option>Pilih rombel ...</option>';

            for (let i = 0; i < arrKelas.length; i++) {
                if (data.rombel == arrKelas[i].kelas) {
                    htmlKelas += '<option value="'+arrKelas[i].kelas+'" selected>'+arrKelas[i].kelas+'</option>';
                } else {
                    htmlKelas += '<option value="'+arrKelas[i].kelas+'">'+arrKelas[i].kelas+'</option>';
                }
            }

            htmlKelas += '</select>';

            $('#ed-rombel').html(htmlKelas);

            $('#ed-tempatlahir').val(data.tempat_lahir);
            $('#ed-tanggallahir').val(data.tanggal_lahir);
            $('#ed-namawali').val(data.nama_wali);
            $('#ed-nikwali').val(data.nik_wali);
            $('#ed-asalsd').val(data.asal_sd);
            $('#ed-asalsmp').val(data.asal_sd);
            $('#ed-password').val(data.password);

            var html = ''
            html += '<a href="'+ BaseUrl + '/assets/data/images/' + data.foto_profile +'" data-lightbox="image-1" data-title="'+data.foto_profile+'">';
            html += '<img width="150" height="150" src="'+BaseUrl + '/assets/data/images/' + data.foto_profile +'"/>';
            html += '</a>';

            $('#imageFile').html(html)
            $('#file-name').html(data.foto_profile);

            lightbox.option({
                'resizeDuration': 200,
                'wrapAround': true,
                'alwaysShowNavOnTouchDevices': true,
                'fadeDuration':300,
                'imageFadeDuration': 300
            })

            var html = '';
            for (let j = 0; j < kegiatan.length; j++) {
                
                html += '<div class="col-md-12 kegiatan'+kegiatanIncrement+'"">';
                html += '<div class="alert alert-info" role="alert">';
                html += '<div class="form-group row">';
                html += '<label for="example-text-input" class="col-md-2 col-form-label">Kegiatan</label>';
                html += '<div class="col-md-10">';
                html += '<select class="form-control select2 kegiatan'+kegiatanIncrement+'"" name="kegiatan[]">';
                html += '<option disabled>Pilih kegiatan ...</option>';

                for (let i = 0; i < arrKegiatan.length; i++) {
                    if (kegiatan[j].kegiatan == arrKegiatan[i].kegiatan) {
                        html += '<option value="'+arrKegiatan[i].kegiatan+'" selected>'+arrKegiatan[i].kegiatan+'</option>';
                        
                    } else {
                        html += '<option value="'+arrKegiatan[i].kegiatan+'">'+arrKegiatan[i].kegiatan+'</option>';
                    }
                }

                html += '</select>';
                html += '</div>';
                html += '</div>';
                html += '<div class="form-group row">';
                html += '<label for="example-text-input" class="col-md-2 col-form-label">Prestasi</label>';
                html += '<div class="col-md-10">';
                html += '<textarea required="" class="form-control kegiatan'+kegiatanIncrement+'"" name="prestasi[]" rows="5">'+kegiatan[j].prestasi+'</textarea>';
                html += '</div>';
                html += '</div>';

                html += '<div class="form-group row">';
                html += '<label for="example-text-input" class="col-md-2 col-form-label">Diraih Pada</label>';
                html += '<div class="col-md-10">';
                html += '<input class="form-control required" type="date" name="diraih_pada[]" value="'+kegiatan[j].diraih_pada+'" required>';
                html += '</div>';
                html += '</div>';

                html += '<div class="form-group row">';
                html += '<label for="example-text-input" class="col-md-2 col-form-label">Status</label>';
                html += '<div class="col-md-10">';
                html += '<select class="form-control select2 kegiatan'+kegiatanIncrement+'"" name="status[]">';
                html += '<option disabled>Pilih status ...</option>';

                if (kegiatan[j].status == 'Aktif') {
                    html += '<option selected value="Aktif">Aktif</option>';
                } else {
                    html += '<option value="Aktif">Aktif</option>';
                }

                if (kegiatan[j].status == 'Tidak Aktif') {
                    html += '<option selected value="Tidak Aktif">Tidak Aktif</option>';
                } else {
                    html += '<option value="Tidak Aktif">Tidak Aktif</option>';
                }
                
                html += '</select>';
                html += '</div>';
                html += '</div>';

                html += '<div class="form-group row">';
                html += '<div class="col-md-12">';
                html += '<button type="button" class="btn btn-danger waves-effect waves-light mr-2" onclick="deleteKegiatan('+kegiatanIncrement+')"><i class="fas fa-trash"></i></button>';
                html += '</div>';
                html += '</div>';

                html += '</div>';
                html += '</div>';
                kegiatanIncrement++;
            
            }
            $('#generateKegiatan').html(html);
            $(".select2").select2();
    
        },error: function(xhr) {
            stopLoading();
            toastr["error"](xhr.responseJSON.message)
        },
    })
}
getDetailSiswa()

function updateSiswa() {
    var form = $('#create-siswa')[0];
    var request = new FormData(form);
    request.append("_token", csrf_token );
    
    $.ajax({
        data: request,
        url: ServeUrl + '/admin/data_siswa/update_siswa',
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        type: 'POST',
        dataType: 'json',
        beforeSend: function() {
            startLoading();
        },
        complete: function() {
            stopLoading();
        },
        success: function(response) {
            Swal.fire({
                title: '<div style="color: white;">Berhasil!</div>',
                html: '<p class="card-text"><small class="text-muted">Perintah ini berhasil diproses. <br> Lakukan pengecekan ulang pada aksi ini.</small></p>',
                type: 'success',
                icon: 'success',
                confirmButtonColor: '#3085d6',
                confirmButtonText: '<i class="fas fa-life-ring fa-sm rotating mr-5"></i>Ya, Baiklah!<i class="fas fa-sm fa-life-ring rotating ml-5"></i>',
                confirmButtonText: 'Pergi ke Status Pembayaran!',
                background: '#14274E',
                confirmButtonClass: 'btn btn-success',
                buttonsStyling: false,
                onClose: function() {
                    window.location.href = BaseUrl + '/admin/data-siswa'
                }
            })
    
        },error: function(xhr) {
            stopLoading();
            toastr["error"](xhr.responseJSON.message)
        },
    })
}

function showPass() {
    var checkTipe = $('#ed-password').get(0).type;
    if (checkTipe == 'password') {
        $('#ed-password').get(0).type = 'text';
        $("#panel").html('<i class="ri-eye-line fa-lg"></i>');
    } else {
        $('#ed-password').get(0).type = 'password';
        $("#panel").html('<i class="ri-eye-off-line fa-lg"></i>');
    }
}