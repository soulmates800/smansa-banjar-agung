
$('.text-error').hide();
$(".select2").select2();
function tambahSiswa() {

    var formStats = false;
    $.each($('.required'), function(index, value){
        if( $(this).val() == "" ){
            formStats = true;
            $(this).addClass('is-invalid');
        } else {
            $(this).removeClass('is-invalid');
        }
    });

    if (formStats) {
        errorSwal();
        return;
    }

    if ($('#jenis-kelamin').val() == 'default') {
        $('#jenis').addClass('is-invalid');
        $('#jenis-kelamin-error').show()
        $('#jenis-kelamin-error').html('Pilih salah satu jenis kelamin')
        errorSwal();
        return;
    } else {
        $('#jenis-kelamin-error').hide()
    }

    var form = $('#create-siswa')[0];
    var request = new FormData(form);
    request.append("_token", csrf_token );
    
    $.ajax({
        data: request,
        url: ServeUrl + '/admin/data_siswa/tambah_siswa',
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        type: 'POST',
        dataType: 'json',
        beforeSend: function() {
            startLoading();
        },
        complete: function() {
            stopLoading();
        },
        success: function() {
            Swal.fire({
                title: '<div style="color: white;">Berhasil!</div>',
                html: '<p class="card-text"><small class="text-muted">Perintah ini berhasil diproses. <br> Lakukan pengecekan ulang pada aksi ini.</small></p>',
                type: 'success',
                icon: 'success',
                confirmButtonColor: '#3085d6',
                confirmButtonText: '<i class="fas fa-life-ring fa-sm rotating mr-5"></i>Ya, Baiklah!<i class="fas fa-sm fa-life-ring rotating ml-5"></i>',
                confirmButtonText: 'Ya, Baiklah!',
                background: '#14274E',
                confirmButtonClass: 'btn btn-success',
                buttonsStyling: false,
                onClose: function() {
                    window.location.href = BaseUrl + '/admin/data-siswa'
                }
            })
    
        },error: function(xhr) {
            stopLoading();
            
            Swal.fire({
                title: '<div style="color: white;">Gagal!</div>',
                html: '<p class="card-text"><small class="text-muted">'+ xhr.responseText +'</small></p>',
                type: 'error',
                icon: 'error',
                confirmButtonColor: '#3085d6',
                confirmButtonText: '<i class="fas fa-life-ring fa-sm rotating mr-5"></i>Ya, Baiklah!<i class="fas fa-sm fa-life-ring rotating ml-5"></i>',
                confirmButtonText: 'Ya, Baiklah!',
                background: '#14274E',
                confirmButtonClass: 'btn btn-success',
                buttonsStyling: false,
                onClose: function() {
                    // window.location.href = BaseUrl + '/admin/data-siswa'
                }
            })
        },
    })
}

function errorSwal() {
    Swal.fire({
        title: '<div style="color: white;">Gagal!</div>',
        html: '<p class="card-text"><small class="text-muted">Mohon untuk melengkapi semua form</small></p>',
        type: 'error',
        icon: 'error',
        confirmButtonColor: '#3085d6',
        confirmButtonText: '<i class="fas fa-life-ring fa-sm rotating mr-5"></i>Ya, Baiklah!<i class="fas fa-sm fa-life-ring rotating ml-5"></i>',
        confirmButtonText: 'Ya, Baiklah!',
        background: '#14274E',
        confirmButtonClass: 'btn btn-success',
        buttonsStyling: false,
        onClose: function() {
            // window.location.href = BaseUrl + '/admin/data-siswa'
        }
    })
}

function check_required_inputs() {
    $('.required').each(function(){
        if( $(this).val() == "" ){
          alert('Please fill all the fields');
          return;
        } else {
            return;
        }
    });
    tambahSiswa();
}

var arrKegiatan = [];
function getKegiatan() {
    $.ajax({
        url: ServeUrl + '/admin/master_data/get_kegiatan',
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        type: 'GET',
        dataType: 'json',
        beforeSend: function() {
            startLoading();
        },
        complete: function() {
            stopLoading();
        },
        success: function(response) {
            arrKegiatan = response;
        },error: function(xhr) {
            stopLoading();
            toastr["error"](xhr.responseJSON.message)
        },
    })
}
getKegiatan()

function getKelas() {
    $.ajax({
        url: ServeUrl + '/admin/master_data/get_kelas',
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        type: 'GET',
        dataType: 'json',
        beforeSend: function() {
            startLoading();
        },
        complete: function() {
            stopLoading();
        },
        success: function(response) {
            
            var data = response.kelas;
            var html = '';
            html += '<option selected disabled>Pilih rombel ...</option>'
            for (let i = 0; i < data.length; i++) {
                html += '<option>'+data[i].kelas+'</option>'
            }
            $('#rombel').html(html);

    
        },error: function(xhr) {
    
        },
    })
}
getKelas()

var kegiatanIncrement = 0;
function generateKegiatan() {
    
    var html = '';

    html += '<div class="col-md-12 kegiatan'+kegiatanIncrement+'"">';
    html += '<div class="alert alert-info" role="alert">';
    html += '<div class="form-group row">';
    html += '<label for="example-text-input" class="col-md-2 col-form-label">Kegiatan</label>';
    html += '<div class="col-md-10">';
    html += '<select class="form-control select2 kegiatan'+kegiatanIncrement+'"" name="kegiatan[]" required>';
    html += '<option>Pilih kegiatan ...</option>';

    for (let i = 0; i < arrKegiatan.length; i++) {
        html += '<option value="'+arrKegiatan[i].kegiatan+'">'+arrKegiatan[i].kegiatan+'</option>';
    }

    html += '</select>';
    html += '</div>';
    html += '</div>';
    html += '<div class="form-group row">';
    html += '<label for="example-text-input" class="col-md-2 col-form-label">Prestasi</label>';
    html += '<div class="col-md-10">';
    html += '<textarea required="" class="form-control kegiatan'+kegiatanIncrement+'"" name="prestasi[]" rows="5"></textarea>';
    html += '</div>';
    html += '</div>';

    html += '<div class="form-group row">';
    html += '<label for="example-text-input" class="col-md-2 col-form-label">Diraih Pada</label>';
    html += '<div class="col-md-10">';
    html += '<input class="form-control required" type="date" name="diraih_pada[]" required>';
    html += '</div>';
    html += '</div>';

    html += '<div class="form-group row">';
    html += '<label for="example-text-input" class="col-md-2 col-form-label">Status</label>';
    html += '<div class="col-md-10">';
    html += '<select class="form-control select2 kegiatan'+kegiatanIncrement+'"" name="status[]" required>';
    html += '<option>Pilih status ...</option>';
    html += '<option value="Aktif">Aktif</option>';
    html += '<option value="Tidak Aktif">Tidak Aktif</option>';
    html += '</select>';
    html += '</div>';
    html += '</div>';

    html += '<div class="form-group row">';
    html += '<div class="col-md-12">';
    html += '<button type="button" class="btn btn-danger waves-effect waves-light mr-2" onclick="deleteKegiatan('+kegiatanIncrement+')"><i class="fas fa-trash"></i></button>';
    html += '</div>';
    html += '</div>';

    html += '</div>';
    html += '</div>';
    kegiatanIncrement++;
    $('#generateKegiatan').append(html);
    $(".select2").select2();
}

function deleteKegiatan(index) {
    $('.kegiatan' + index).hide();
}

function validateFileType(input){
    readURL(input)
    var fileName = $('#customFile').val();
    var idxDot = fileName.lastIndexOf(".") + 1;
    var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();
    if (extFile=="jpg" || extFile=="jpeg" || extFile=="png"){
        $('#file-name').html(fileName);
    }else{
        alert("Hanya gambar berformat .jpg .jpeg .png yang diizinkan");
    }
}

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#imageFile')
                .attr('src', e.target.result)
                .width(150)
                .height(150);
        };
        reader.readAsDataURL(input.files[0]);
    }
}

