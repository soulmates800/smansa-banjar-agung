
var semesterAktif;
var selectFilterHelperBug = 0;
function getSiswa(kelas) {

    $('#datatable-buttons').DataTable().clear();
    $('#datatable-buttons').DataTable().destroy();

    $.ajax({
        url: ServeUrl + '/admin/data_siswa/get_siswa/' + kelas,
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        type: 'GET',
        dataType: 'json',
        beforeSend: function() {
            startLoading();
        },
        complete: function() {
            stopLoading();
        },
        success: function(response) {
            var data = response.siswa;
            semesterAktif = response.aktif_semester.semester;
            var html = '';
            var numberIncrement = 1;
            for (let i = 0; i < data.length; i++) {
                html += '<tr>';
                html += '<td class="text-center align-middle">'+numberIncrement+'</td>'
                html += '<td class="text-center align-middle">'+data[i].nis+'</td>'
                html += '<td class="align-middle">'+data[i].nama+'</td>'
                html += '<td class="align-middle">'+data[i].rombel+'</td>'
                html += '<td class="text-center align-middle">'+data[i].jenis_kelamin+'</td>'
                html += '<td class="align-middle">'+CapitalEachWord(data[i].nama_wali)+'</td>'
                html += '<td class="text-center align-middle">'+data[i].tanggal_lahir+'</td>'
                html += '<td class="text-center align-middle">'+data[i].tempat_lahir+'</td>'
                html += '<td class="text-center">';
                html += '<button class="btn btn-sm btn-primary waves-effect waves-light mr-2" onclick="cetakPerSiswa('+data[i].id+')"><i class="fas fas fa-print fa-sm mr-2"></i>Print</button>';
                html += '<a class="btn btn-sm btn-info waves-effect waves-light mr-2" href="'+BaseUrl + '/admin/data-siswa/edit_siswa/' + data[i].id +'"><i class="fas fa-edit fa-sm mr-2"></i>Edit</a>';
                html += '<button type="button" onclick="hapusSiswa('+data[i].id+')" class="btn btn-sm btn-danger waves-effect waves-light mr-2"><i class="fas fa-edit fa-sm mr-2"></i>Hapus</button>';
                html += '</td>';
                html += '</tr>';
                numberIncrement++;
            }

            $('#generateData').html(html);
            reinitDatatable();
        },error: function(xhr) {
            stopLoading();
            toastr["error"](xhr.responseJSON.message)
        },
    })
}
getSiswa('semua');

function reinitDatatable() {
    var a = $("#datatable-buttons").DataTable({
        lengthChange: !1,
        language: {
            paginate: {
                previous: "<i class='mdi mdi-chevron-left'>",
                next: "<i class='mdi mdi-chevron-right'>",
            },
        },
        drawCallback: function () {
            $(".dataTables_paginate > .pagination").addClass(
                "pagination-rounded"
            );
        },
        // buttons: ["copy", "excel", "pdf"],
    });
    a
    .buttons()
    .container()
    .appendTo("#datatable-buttons_wrapper .col-md-6:eq(0)"),
    $("#selection-datatable").DataTable({
        select: { style: "multi" },
        language: {
            paginate: {
                previous: "<i class='mdi mdi-chevron-left'>",
                next: "<i class='mdi mdi-chevron-right'>",
            },
        },
        drawCallback: function () {
            $(".dataTables_paginate > .pagination").addClass(
                "pagination-rounded"
            );
        },
    })
}

function hapusSiswa(id) {
    Swal.fire({
        title: '<div style="color: white;">Wait !</div>',
        html: '<p class="card-text"><small class="text-muted">Apa kamu benar benar sudah yakin ?<br> Perintah ini akan menghapus untuk <b>selamanya</b></small></p>',
        type: 'warning',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya, Lakukan!',
        cancelButtonText: 'Tidak, Batalkan!',
        confirmButtonClass: 'btn btn-primary',
        background: '#20232A',
        cancelButtonClass: 'btn btn-danger ml-1',
        buttonsStyling: false,
    }).then(function (result) {
        if (result.value) {
            $.ajax({
                url: ServeUrl + '/admin/data_siswa/hapus_siswa/' + id ,
                processData: false,
                contentType: false,
                cache: false,
                timeout: 600000,
                type: 'GET',
                dataType: 'json',
                beforeSend: function() {
                    startLoading();
                },
                complete: function() {
                    stopLoading();
                },
                success: function(response) {
                    Swal.fire({
                        title: '<div style="color: white;">Berhasil!</div>',
                        html: '<p class="card-text"><small class="text-muted">Siswa berhasil dihapus.</small></p>',
                        type: 'success',
                        icon: 'success',
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: '<i class="fas fa-life-ring fa-sm rotating mr-5"></i>Ya, Baiklah!<i class="fas fa-sm fa-life-ring rotating ml-5"></i>',
                        confirmButtonText: 'Ya, Baiklah!',
                        background: '#14274E',
                        confirmButtonClass: 'btn btn-success',
                        buttonsStyling: false,
                        onClose: function() {
                            getSiswa()
                        }
                    })
                },error: function(xhr) {
                    stopLoading();
                    toastr["error"](xhr.responseJSON.message)
                },
            })
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                Swal.fire({
                title: 'Dibatalkan !',
                icon: 'error',
                html: '<p class="card-text"><small class="text-muted">Data ini tetap akan di simpan</small></p>',
                type: 'error',
                confirmButtonText: 'Ya, Baiklah !',
                confirmButtonClass: 'btn btn-success',
                })
            }
        })
}

function getSemuaSemester() {
    $.ajax({
        url: ServeUrl + '/wali_murid/pembayaran/get_semua_semester',
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        type: 'GET',
        dataType: 'json',
        beforeSend: function() {
            startLoading();
        },
        complete: function() {
            stopLoading();
        },
        success: function(response) {
            var dataSelectFilter = response.semua_semester
            var selectFilter = '';
            for (let i = 0; i < dataSelectFilter.length; i++) {
                if (dataSelectFilter[i].semester == semesterAktif && selectFilterHelperBug == 0) {
                    selectFilterHelperBug = 1;
                    selectFilter += '<option selected value="'+dataSelectFilter[i].id+'">'+dataSelectFilter[i].semester+'</option>'
                } else {
                    selectFilter += '<option value="'+dataSelectFilter[i].id+'">'+dataSelectFilter[i].semester+'</option>'
                }
            }
            $('#selectFilter').html(selectFilter);
        },error: function(xhr) {
    
        },
    })
}
getSemuaSemester()

function handleKelasFilter(attr) {
    getSiswa(attr.value);
}

function getKelas() {
    $.ajax({
        url: ServeUrl + '/admin/master_data/get_kelas',
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        type: 'GET',
        dataType: 'json',
        beforeSend: function() {
            startLoading();
        },
        complete: function() {
            stopLoading();
        },
        success: function(response) {
            var data = response.kelas
            var selectFilter = '';
            selectFilter += '<option selected value="semua">Seluruh Kelas</option>'
            for (let i = 0; i < data.length; i++) {
                selectFilter += '<option value="'+data[i].id+'">'+data[i].kelas+'</option>'
            }
            $('#kelasFilter').html(selectFilter);
        },error: function(xhr) {
    
        },
    })
}
getKelas()

function cetakPerSiswa(id_user) {
    var currentSemester = $('#selectFilter').val();
    window.location.href = BaseUrl + '/admin/data-siswa/cetak_per_siswa?id_user=' + id_user + '&semester=' + currentSemester;
}

function cetakSemuaPerKelas() {
    var currentKelas = $('#kelasFilter').val();
    window.location.href = BaseUrl + '/admin/data-siswa/cetak_siswa/' + currentKelas
}

$(".select2").select2();