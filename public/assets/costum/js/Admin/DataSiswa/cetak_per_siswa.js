var user_id = window.location.pathname.split('/').pop();

console.log(`datas`, datas)
var csrf_token = $('meta[name="csrf-token"]').attr('content');
function getData() {
    $.ajax({
        url: ServeUrl + '/admin/data_siswa/cetak_per_siswa/' + datas.id_user,
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        type: 'GET',
        dataType: 'json',
        beforeSend: function() {
            startLoading();
        },
        complete: function() {
            stopLoading();
        },
        success: function(response) {
            getDetailBulanPembayaran();
            var data = response;
            
            $('#nis').html(data.nis);
            $('#nama').html(data.nama);
            $('#rombel').html(data.rombel);
            $('#jenis_kelamin').html(data.jenis_kelamin);
            $('#tempat_lahir').html(data.tempat_lahir);
            $('#tanggal_lahir').html(data.tanggal_lahir);
            $('#nama_wali').html(data.nama_wali);
            
            
        },error: function(xhr) {
            stopLoading();
            toastr["error"](xhr.responseJSON.message)
        },
    })
}
getData()
var bulanLunas = new Array(12);
function getDetailBulanPembayaran() {

    var request = new FormData();

    request.append("user_id", datas.id_user);
    request.append("semester_id", datas.semester);

    request.append("_token", csrf_token );

    $.ajax({
        data: request,
        url: ServeUrl + '/admin/data_siswa/status_pembayaran_siswa',
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        type: 'POST',
        dataType: 'json',
        beforeSend: function() {
            startLoading();
        },
        complete: function() {
            stopLoading();
        },
        success: function(response) {
            var data = response.status_pembayaran;
            var html = '';
            var total = 0;
            var tempBulan = '';
            $('#semester-aktif').html(response.semester.semester)
            if (response.semester.tipe == 'Ganjil') {
                bulan = [
                    "Januari",
                    "Februari",
                    "Maret",
                    "April",
                    "Mei",
                    "Juni",
                ];
            } else {
                bulan = [
                    "Juli",
                    "Agustus",
                    "September",
                    "Oktober",
                    "November",
                    "Desember",
                ];
            }
            templateBulan()

            for (let i = 0; i < bulan.length; i++) {
                for (let j = 0; j < data.length; j++) {
                    if (data[j].bulan == bulan[i] && data[j].status == 'Lunas') {
                        bulanLunas[i] = bulan[i]
                        tempBulan += '<div class="row">';
                        tempBulan += '<div class="mb-2 col-md-12">';
                        tempBulan += '<span class="roboto-font">'+formatRupiah(String(data[j].jumlah), 'Rp. ')+'</span>';
                        total = total + data[j].jumlah;
                        tempBulan += '</div>';
                        tempBulan += '<div class="col-md-12">';
                        tempBulan += '<span class="badge bg-success text-white roboto-font bulan-status-info"><i class="fas fa-check-circle fa-sm mr-2"></i>Lunas</span>';
                        tempBulan += '</div>';
                        tempBulan += '</div>';
                        $('#' + bulan[i]).html(tempBulan)
                        tempBulan = '';
                    }
                }
            }
            $('#generateBulanStatus').html(html);
            $('#totalPembayaran').html('<b>Total :</b> ' + formatRupiah(String(total), 'Rp. '));
            setTimeout(() => {
                window.print();
            }, 1000);
        },error: function(xhr) {
            stopLoading();
            toastr["error"](xhr.responseJSON.message)
        },
    })
}

function templateBulan() {
    var html = '';
    for (let i = 0; i < bulan.length; i++) {
        html += '<div class="col-md-2 text-center border p-2">';
        html += '<div class="bulan-name">';
        html += '<span class="roboto-font bold">'+bulan[i]+'</span>';
        html += '</div>';
        html += '<div class="bulan-status mt-2" id="'+bulan[i]+'">';

        html += '<div class="row">';
        html += '<div class="mb-2 col-md-12">';
        html += '<span class="roboto-font">'+formatRupiah('0', 'Rp. ')+'</span>';
        html += '</div>';
        // html += '<div class="col-md-12">';
        // html += '<span class="badge bg-danger text-white roboto-font bulan-status-info"><i class="far fa-window-close fa-sm mr-2"></i>Belum Lunas</span>'
        // html += '</div>';
        html += '</div>';

        
        html += '</div>';
        html += '</div>';
    }
    $('#generateTemplate').html(html);
}
templateBulan()
