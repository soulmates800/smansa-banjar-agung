
var kelas = window.location.pathname.split('/').pop();

function getData() {
    $.ajax({
        url: ServeUrl + '/admin/data_siswa/cetak_siswa/' + kelas,
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        type: 'GET',
        dataType: 'json',
        beforeSend: function() {
            startLoading();
        },
        complete: function() {
            stopLoading();
        },
        success: function(response) {
            console.log(`response`, response)
            var data = response.siswa;
            var html = '';
            var currentKelas = '';
            var numberIncrement = 1;
            for (let i = 0; i < data.length; i++) {
                html += '<tr>';
                html += '<td class="align-middle text-center">'+numberIncrement+'</td>';
                html += '<td class="align-middle text-center">'+data[i].nis+'</td>';
                html += '<td class="align-middle">'+data[i].nama+'</td>';
                html += '<td class="align-middle text-center">'+data[i].jenis_kelamin+'</td>';
                html += '<td class="align-middle text-center">'+data[i].nama_wali+'</td>';
                html += '<td class="align-middle text-center">'+data[i].tanggal_lahir+'</td>';
                html += '<td class="align-middle text-center">'+data[i].tempat_lahir+'</td>';
                html += '<td class="align-middle text-center">'+data[i].rombel+'</td>';
                html += '</tr>'
                currentKelas = data[i].rombel
                numberIncrement++;
            }
            $('#generateData').html(html);

            if (kelas == 'semua') {
                $('#namaKelas').html('<b>Semua Kelas</b>')
            } else {
                $('#namaKelas').html('<b>Kelas : </b>' + currentKelas);
            }
            
            setTimeout(() => {
                window.print();
            }, 1000);
        },error: function(xhr) {
            stopLoading();
            toastr["error"](xhr.responseJSON.message)
        },
    })
}
getData()